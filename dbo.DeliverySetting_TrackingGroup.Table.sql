USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DeliverySetting_TrackingGroup](
	[DeliverySettings_TrackingGroupID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[DeliverySettingsID] [int] NULL,
	[TrackingGroup] [int] NULL,
	[Value] [varchar](20) NULL,
 CONSTRAINT [PK_DeliverySetting_TrackingGroup] PRIMARY KEY CLUSTERED 
(
	[DeliverySettings_TrackingGroupID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
