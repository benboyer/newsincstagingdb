USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ps_SingleContentByStory]
	@PartnerId int,
	@StoryId varchar(64),
	@TrackingGroup int

AS
BEGIN
	set FMTONLY OFF
	set NOCOUNT ON
	exec SprocTrackingUpdate 'ps_SingleContentByStory'
	if @PartnerId = 0 set @PartnerId = null
	if not exists (select 1 from mTrackingGroup where TrackingGroup = @TrackingGroup) set @TrackingGroup = 10557
	declare @ContentID bigint
	set		@ContentID = (select top 1 ContentID from ContentLink (nolock) where (@PartnerId is null or PartnerID = @PartnerId) and (VideoGuid = @StoryId or StoryGUID = @StoryId) order by ContentID desc)
		select c.ContentID, c.Name, c.Description, pt.TrackingGroup ContentPartnerID, c.PartnerID AS CPID,
		null as VideoGroupName, fa.Duration, convert(date, c.EffectiveDate) PubDate, c.Keyword, c.antikw AS AKW, c.UpdatedDate, convert(varchar(120), null) Timeline,
		case	when fa.Name like 'Video%' then 'src'
				else fa.Name
			end as AssetType,
		fa.AssetLocation,
		fa.AssetMimeType,
		fa.AssetSortOrder,
		fa.AssetLocation2,
		fa.AssetMimeType2,
		fa.AssetSortOrder2,
		dbo.GetPathToServeAsset(fa.assetid) AssetLocation3,
		pt.name as ProducerName, pt.shortname ProducerNameAlt, isnull(pt.logourl,'http://assets.newsinc.com/newsinconebyone.png') ProducerLogo, convert(int, 0) as ContentOrder,
		isnull(replace(nc.NDNCategory, 'NDN Viral Network', 'News Distribution Network, Inc.'), 'news') ProducerCategory,
		REPLACE(c.Name, ' ', '-') + + '-' +CONVERT(varchar(20), c.ContentID) as SEOFriendlyTitle,
		case	when dsm.partnerid is null then 1
				when dsm.partnerid is not null then 0
			end as embedCodeAccessible
	from	(select top 1 * from Content (nolock) where StoryID = @StoryId or ContentID = @ContentID order by ContentID desc) c
--    left join	(select top 1 * from ContentLink (nolock) where VideoGuid = @StoryId or StoryGUID = @StoryId order by ContentID desc) cl
  --  on		c.ContentID = cl.contentid
	join	partner (nolock) pt
	on		pt.PartnerID = c.PartnerID
	left join Playlist_Content_PartnerMap pm (nolock)
	on		pt.PartnerID = pm.PartnerID and c.FeedID = pm.FeedID
	left join NDNCategory nc (nolock)
	on		pm.NDNCategoryID = nc.NDNCategoryID
	left	join vw_partnerDefaultSettings (nolock) dsm
	on		pt.PartnerID = dsm.PartnerID and Entity = 'BlockShareForProviderID'
	cross apply dbo.ps_fn_getAssetDataMultiAsset(c.PartnerID, c.contentid) fa
	where	---(c.StoryID = @StoryId or c.ContentID = @ContentID) --(c.PartnerID = @PartnerId or c.PartnerID = cl.PartnerID)
	--AND		(cl.VideoGuid = @StoryId or cl.StoryGUID = @StoryId)-- or c.StoryID = @StoryId) -- c.StoryID = @StoryId AND C.Active = 1
	-- and
			dbo.AllowedContentID(c.contentid, @trackinggroup, 1) = 1
	order by replace(fa.Name, 'Video', 'src'), fa.Assetsortorder, fa.AssetMimeType

	set NOCOUNT OFF
end
GO
