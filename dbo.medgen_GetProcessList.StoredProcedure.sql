USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[medgen_GetProcessList]
	--declare
	@StartDTM datetime = null,
	@limittime bit = 1
as
BEGIN
	set fmtonly off
	set nocount on

	exec SprocTrackingUpdate 'medgen_GetProcessList'
	--declare @StartDTM datetime = '2012-09-02'
	declare @now datetime = getdate()
	if @StartDTM is null
	begin
		set @StartDTM = (select value from ConfigLookup where [KEY] = 'LastMedGenTime')
	end

	-- below, we default to 2 days ago as the max we will go back.  Anything more will require manual intervention
	if @limittime = 1 and  @StartDTM is null or @StartDTM < DATEADD(dd, -2, getdate())
	begin
		set @StartDTM = (select DATEADD(dd, -2, getdate()))
	end

		select  @now ProcessRunTime, c.PartnerID, ol.LogoUrl, /*p.LogoURL, */p.Name, v.VideoID,v.ContentItemID,v.UserID,v.ClipURL,v.Title,v.Description,v.EventDate,v.Height,v.Width, c.aDuration as Duration,
				v.CreatedOn,v.ClipPopularity,v.CandidateOfInterest,v.ThumbnailURL,v.PublishDate,v.Keywords,v.NumberOfPlays,
				v.SentToFreeWheel,v.IsEnabled,v.IsDeleted,v.ExpiryDate,v.OriginalName,v.StillFrameURL,v.FileExtension,
				v.Category,v.EnterDTM,v.LastChgDTM,v.IngestionStatusMessage,v.IsAdvertisement,v.UserType,v.NumberOfReviews,v.IsFileArchived, 
				p.ShortName ProducerNameAlt,  isnull(replace(nc.NDNCategory, 'NDN Viral Network', 'News Distribution Network, Inc.'), 'News Distribution Network, Inc.') ProducerCategory

	-- select @StartDTM ProcessRunTime, v.*, nu.*, o.*
	from (select con.*, a.Duration aDuration from Content (nolock) con join Content_Asset ca  (nolock) on con.contentid = ca.ContentID join asset a (nolock) on ca.AssetID = a.AssetID where con.UpdatedDate >= @StartDTM and ca.TargetPlatformID = 1 and ca.AssetTypeID = 1) C
	join	Partner p (nolock)
	on	c.PartnerID = p.PartnerID
	left join (select * from Playlist_Content_PartnerMap (nolock) where NDNCategoryID is not null) pcpm
	on		p.PartnerID = pcpm.PartnerID
	left join NDNCategory nc
	on		pcpm.NDNCategoryID = nc.NDNCategoryID
	join OrganizationsLEGACY ol (nolock)
	on	p.PartnerID = ol.PartnerID
	join	Videos (nolock) v
	on		c.ContentID = v.videoid
	join NDNUsers nu (nolock)
	on	v.UserID = nu.UserID
	join Organizations o (nolock)
	on	nu.OrganizationID = o.OrganizationId
	set nocount off

END
GO
