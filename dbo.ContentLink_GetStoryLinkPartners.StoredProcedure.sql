USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ContentLink_GetStoryLinkPartners]
	--declare
	@GetMatched bit = 0
as
BEGIN
/*
	 APPLICABLE TICKETS:
		DB-13
*/
	set		fmtonly off
	set		nocount on
	exec	SprocTrackingUpdate 'ContentLink_GetStoryLinkPartners'
	select	a.PartnerID, p.Name
	from	(select	distinct PartnerID
			-- select *
			from	contentlink (nolock)
			where	((@GetMatched = 0 and ContentID is null)
					or
					@GetMatched = 1)) a
	join	Partner (nolock) p
	on		a.partnerid = p.PartnerID
	order by a.PartnerID
	set nocount off
END
GO
