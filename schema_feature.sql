USE [NewsincStaging]
GO

/****** Object:  Table [deploy].[schema_feature]    Script Date: 04/11/2014 11:33:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [deploy].[schema_feature](
	[feature_order] [int] NOT NULL,
	[feature_type] [varchar](20) NOT NULL,
	[feature_name] [varchar](60) NOT NULL,
	[defined_on] [datetime] NULL,
	[defined_by] [varchar](20) NULL,
	[depends_on] [int] NULL,
	[installation_code] [varchar](max) NULL,
	[for_schema] [varchar](20) NOT NULL,
	[tickets_applicable] [varchar](120) NULL,
	[completed_on] [datetime] NULL,
	[description] [varchar](100) NULL,
 CONSTRAINT [PK_schema_feature] PRIMARY KEY CLUSTERED 
(
	[feature_order] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [deploy].[schema_feature] ADD  CONSTRAINT [DF__schema_fe__defin__6B4FC9D9]  DEFAULT (getdate()) FOR [defined_on]
GO

