USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[dme_PartnerLogo]
as
select pl.partnerid, l.logourl, lt.name, pl.LauncherID, case lt.description when '740' then '4x3' when '600' then '16x9' else lt.description end as LogoDescription, l.Active, 0 as [Default]
from partner_logo pl
join	logo l
on		pl.logoid = l.logoid
join	logotype lt
on		l.logotypeid = lt.logotypeid
GO
