USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_GetBestFeedContentAsset]
	(
--		-- declare
	@feedcontentid bigint = null,
	@assettyperid int = null
	)
as
BEGIN
	set fmtonly OFF
	set Nocount ON
	exec SprocTrackingUpdate 'usp_GetBestFeedContentAsset'

	--	declare @feedcontentid bigint = 324955 , @assettyperid int = 9 -- , @feedcontentassetid int, @width int, @bitrate int, @encode bit = 0, @fileextension varchar(120)
	--	declare @feedcontentid bigint = 338651, @assettyperid int = 3 -- , @feedcontentassetid int, @width int, @bitrate int, @encode bit = 0, @fileextension varchar(120)

	-- set up variables to return if we got nuthin
	declare @feedcontentassetid bigint = 0, @width int = 0, @bitrate int = 0, @encode bit = 0, @fileextension varchar(120) = '', @CreateFile bit = 0

	-- Create a temp table for our potential asset and it's platform-video
	select a.*, at.AssetTypeID VideoAssetTypeID
	into #tester
	from (select	fc.PartnerID, fc.FeedContentID, ptpp.TargetPlatformID, ptpp.isImportEnabled, at.AssetTypeID, mt.MimeTypeID, mt.MediaType
		from	FeedContent fc
		join	Partner_TargetPlatformProfile ptpp
		on		fc.PartnerID = ptpp.PartnerID
		join	TargetPlatform_AssetType tpat
		on		ptpp.TargetPlatformID = tpat.TargetPlatformID
		join	AssetType at
		on		tpat.AssetTypeID = at.AssetTypeID
		join	MimeType mt
		on		at.MimeTypeID = mt.MimeTypeID
		where   fc.FeedContentID = @feedcontentid
		and		ptpp.isImportEnabled = 1
		and		tpat.AssetTypeID = @assettyperid) a
	join	Partner_TargetPlatformProfile tp
	on		a.PartnerID = tp.PartnerID
	and		a.TargetPlatformID = tp.TargetPlatformID
	join	TargetPlatform_AssetType tpat
	on		tp.TargetPlatformID = tpat.TargetPlatformID
	join	AssetType at
	on		tpat.AssetTypeID = at.AssetTypeID
	join	MimeType mt
	on		at.MimeTypeID = mt.MimeTypeID
	where  mt.MediaType in ('video')

	create table #assetcheck(FeedContentAssetID bigint, Width int, bitrate int, fileextension varchar(10), Encode bit, createfile bit)

	if (select COUNT(*) from #tester) > 0 -- do we have a valid platform to work with? If so, it will have the assettype for the video in the temp table.
	begin
		declare @fccid bigint = (select FeedContentID from #tester), @vatid bigint = (select VideoAssetTypeID from #tester)

		-- declare @fccid int = 338651, @vatid int = 1
		create table #videocheck(FeedContentAssetID bigint, Width int, bitrate int, fileextension varchar(10), Encode bit, createfile bit)
		insert into #videocheck(FeedContentAssetID, Width, bitrate, fileextension, Encode, createfile)
		exec usp_GetBestFeedContentAsset_code @fccid,@vatid

		if (select FeedContentAssetID from #videocheck) <> 0 -- do we have a video available for the platform?  If not we won't do an image either!
		begin
			-- declare @fccid int = 338651, @vatid int = 1
			set @vatid = (select AssetTypeID from #tester)
			insert into #assetcheck(FeedContentAssetID, Width, bitrate, fileextension, Encode, createfile)
			exec usp_GetBestFeedContentAsset_code @fccid, @vatid
			if	(select FeedContentAssetID from #assetcheck) > 0
			begin
				select FeedContentAssetID, Width, bitrate, fileextension, Encode, createfile from #assetcheck
				if OBJECT_ID('tempdb.dbo.#tester') is not null drop table #tester
				if OBJECT_ID('tempdb.dbo.#videocheck') is not null drop table #videocheck
				if OBJECT_ID('tempdb.dbo.#assetcheck') is not null drop table #assetcheck
				return
			end
			if	(select FeedContentAssetID from #assetcheck) = 0
				and	(select width from #assetcheck) = 0
				and (select bitrate from #assetcheck) = 0
				and (select fileextension from #assetcheck) = ''
				and (select encode from #assetcheck) = 0
				and	( -- declare @fccid int = 338651, @vatid int = 2
					select MediaType
					from AssetType at
					join MimeType mt
					on	at.MimeTypeID = mt.MimeTypeID
					where at.AssetTypeID = @vatid)
				= 'image'
			begin
					select @feedcontentassetid = feedcontentassetid , @bitrate = 0, @encode = 1, @CreateFile = 0 -- select *
					from #videocheck
					select @width = (select idealwidth from AssetType where AssetTypeID = @vatid)
					select @fileextension = (select mt.fileExtension from assettype at join mimetype mt on at.MimeTypeID = mt.MimeTypeID where at.AssetTypeID = @vatid)
					SELECT @FeedContentAssetID FeedContentAssetID, @width Width, @bitrate Bitrate, @fileextension FileExtension, @encode Encode, @CreateFile CreateFile
			end
		end
	end
	if OBJECT_ID('tempdb.dbo.#tester') is not null drop table #tester
	if OBJECT_ID('tempdb.dbo.#videocheck') is not null drop table #videocheck
	if OBJECT_ID('tempdb.dbo.#assetcheck') is not null drop table #assetcheck

/*
-- The rollback stuff for 20120726
	declare @feedcontentassetid bigint = 0, @width int = 0, @bitrate int = 0, @encode bit = 0, @fileextension varchar(120) = '', @CreateFile bit = 0

	-- Create a temp table for our potential asset and it's platform-video
	select fc.PartnerID, fc.FeedContentID, ptpp.TargetPlatformID, ptpp.isImportEnabled, at.AssetTypeID, mt.MimeTypeID, mt.MediaType
	into #tester
	from (select * from FeedContent (nolock) where FeedContentID = @feedcontentid) fc
	join Partner_TargetPlatformProfile ptpp
	on	fc.PartnerID = ptpp.PartnerID
	join (select * from TargetPlatform_AssetType (nolock) where AssetTypeID = @assettyperid) tpata
	on	ptpp.TargetPlatformID = tpata.TargetPlatformID
	join TargetPlatform_AssetType tpat
	on	tpata.TargetPlatformID = tpat.TargetPlatformID
	join AssetType at
	on	tpat.AssetTypeID = at.AssetTypeID
	--and	@assettyperid = at.AssetTypeID
	join MimeType mt
	on	at.MimeTypeID = mt.MimeTypeID
	where fc.FeedContentID = @feedcontentid
	--and	at.AssetTypeID = @assettyperid
	and	ptpp.isImportEnabled = 1
	and mt.MediaType in ('video','externalid')

	if (select COUNT(*) from #tester) > 0 -- do we have a valid platform to work with? If so, it will have the assettype for the video in the temp table.
	begin
		declare @fccid bigint = (select distinct FeedContentID from #tester where mediatype = 'video'), @vatid bigint = (select AssetTypeID from #tester where mediatype = 'video')

		create table #videocheck(FeedContentAssetID bigint, Width int, bitrate int, fileextension varchar(10), Encode bit, createfile bit)
		insert into #videocheck(FeedContentAssetID, Width, bitrate, fileextension, Encode, createfile)
		exec usp_GetBestFeedContentAsset_code @fccid,@vatid
	--------------------
		if (select COUNT(*) from #videocheck) = 0 or (select feedcontentassetid from #videocheck) = 0
		begin
			truncate table #videocheck		
			-- declare @fccid bigint,  @vatid bigint
			set @fccid = (select distinct FeedContentID from #tester where mediatype = 'ExternalID')
			set @vatid = (select AssetTypeID from #tester where mediatype = 'ExternalID')
			insert into #videocheck(FeedContentAssetID, Width, bitrate, fileextension, Encode, createfile)
			exec usp_GetBestFeedContentAsset_code @fccid,@vatid
		end
	--------------------
		if (select distinct FeedContentAssetID from #videocheck) <> 0 -- do we have a video available for the platform?  If not we won't do an image either!
		begin
			---set @vatid = (select AssetTypeID from #tester)
			exec usp_GetBestFeedContentAsset_code @fccid, @assettyperid -- @vatid
		end
		else
		begin
			SELECT @FeedContentAssetID FeedContentAssetID, @width Width, @bitrate Bitrate, @fileextension FileExtension, @encode Encode, @CreateFile CreateFile
		end
	end

	if OBJECT_ID('tempdb.dbo.#tester') is not null drop table #tester
	if OBJECT_ID('tempdb.dbo.#videocheck') is not null drop table #videocheck


*/
END
GO
