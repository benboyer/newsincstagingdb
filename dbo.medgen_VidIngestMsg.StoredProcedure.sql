USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[medgen_VidIngestMsg]
	--declare
	@ContentID bigint
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'medgen_VidIngestMsg'
	select	*
	from	Videos (nolock)
	where	VideoID = @ContentID
	and		IngestionStatusMessage like '%<mediaID>' + CONVERT(varchar(20), VideoID) + '</mediaID>%'
	set nocount off
END
GO
