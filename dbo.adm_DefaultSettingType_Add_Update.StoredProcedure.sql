USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[adm_DefaultSettingType_Add_Update]
	@iName varchar(36),
	@iActive bit = True,
	@deleteit bit = 0,
	@icomment varchar(120) = null
as
BEGIN
	set FMTONLY OFF
	set NOCOUNT ON
	exec	SprocTrackingUpdate 'adm_DefaultSettingType_Add_Update'
	--	select * from defaultsettingtype
	if @iName is null
	begin
		select 'You must supply the Entity name.  Please check and try again' as MessageBack
		return
	end
	if @deleteit = 1
	and	exists
		(select 1 -- select *
		from	defaultsettingtype
		where	name = @iName
)
	begin
		delete
		from	DefaultSettingtype
		where	Name = @iName
	end
	else
	BEGIN
		if exists
			(select 1 -- select *
			from	DefaultSettingType
			where	Name = @iName)
		begin
			update	DefaultSettingType
			set		Comment = @icomment,
					Active = @iActive
			where	Name = @iName
		end
		else
		BEGIN
			insert into DefaultSettingType(Name, Comment, Active)
			select @iName, @icomment, @iActive
		END
	END
		set NOCOUNT OFF
END
GO
