USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[medgen_VidStagByGuidUsrID]
(
	@guid varchar(250),
	@usierid int
)
as
BEGIN
	set fmtonly off
	set nocount on

	exec SprocTrackingUpdate 'medgen_VidStagByGuidUsrID'

	select *
	from VideoStaging (nolock)
	where GUID = @guid and UserID = @usierid
	set nocount off
end
GO
