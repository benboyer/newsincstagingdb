USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FeedContent](
	[FeedContentID] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PartnerID] [int] NOT NULL,
	[GUID] [varchar](250) NULL,
	[ContentID] [int] NULL,
	[PublishDate] [datetime] NULL,
	[ExpirationDate] [datetime] NULL,
	[Name] [varchar](500) NULL,
	[Description] [varchar](1000) NULL,
	[Keywords] [varchar](1000) NULL,
	[Categories] [varchar](250) NULL,
	[Version] [int] NOT NULL,
	[ImportComplete] [bit] NOT NULL,
	[Retries] [tinyint] NOT NULL,
	[PendingPublishDate] [bit] NOT NULL,
	[network] [varchar](120) NULL,
	[FeedID] [int] NOT NULL,
	[RecordAddedDTM] [datetime] NOT NULL,
	[OnlyOwnerView] [bit] NOT NULL,
	[StoryID] [varchar](100) NULL,
 CONSTRAINT [PK_FeedContent_1] PRIMARY KEY CLUSTERED 
(
	[FeedContentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [NDX_FC_guidICaddeddtm] ON [dbo].[FeedContent] 
(
	[GUID] ASC,
	[ImportComplete] ASC,
	[RecordAddedDTM] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NDX_FeedCID] ON [dbo].[FeedContent] 
(
	[ContentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NDX_FeedContent_CID] ON [dbo].[FeedContent] 
(
	[ContentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FeedContent]  WITH CHECK ADD  CONSTRAINT [FK_FeedContent_Content] FOREIGN KEY([ContentID])
REFERENCES [dbo].[Content] ([ContentID])
GO
ALTER TABLE [dbo].[FeedContent] CHECK CONSTRAINT [FK_FeedContent_Content]
GO
ALTER TABLE [dbo].[FeedContent]  WITH CHECK ADD  CONSTRAINT [FK_FeedContent_Partner] FOREIGN KEY([PartnerID])
REFERENCES [dbo].[Partner] ([PartnerID])
GO
ALTER TABLE [dbo].[FeedContent] CHECK CONSTRAINT [FK_FeedContent_Partner]
GO
ALTER TABLE [dbo].[FeedContent] ADD  CONSTRAINT [DF_FeedContent_RecordAddedDTM]  DEFAULT (getdate()) FOR [RecordAddedDTM]
GO
ALTER TABLE [dbo].[FeedContent] ADD  CONSTRAINT [DF_FeedContent_OnlyOwnerView]  DEFAULT ((0)) FOR [OnlyOwnerView]
GO
