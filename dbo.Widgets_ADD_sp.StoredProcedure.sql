USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Widgets_ADD_sp]
      @ContentItemID int = null,
      @UserId int,
      @DCId int = null,
      @VanityName varchar(32) = null,
      @Name nvarchar(50),
      @WidgetType int = null,
      @WidgetIdentificationName nvarchar(100) = null,
      @IsDefault bit = null,
      @DefaultTab int = null,
      @IsDeleted bit,
      @LastUpdated datetime = null,
      @GenerateNow bit = null,
      @TemplateZip varbinary(max) = null,
      @Height int = null,
      @Width int = null,
      @SiteSectionId int = null,
      @EnterDTM smalldatetime,
      @LastChgDTM datetime = null
AS
BEGIN
	set nocount on
	exec SprocTrackingUpdate 'Widgets_ADD_sp'

	Declare @NewID int;
	Declare @PartnerID int;

	BEGIN TRANSACTION

	Select @PartnerID = PartnerID from [User] where UserID = @UserId;
	IF @@ERROR = 0

	BEGIN

	Insert Launcher (PartnerID, Name, LauncherTypeID, PlayerTypeID, SectionID, Active, CreatedDate, CreatedUserID, UpdatedDate, UpdatedUserID)
	VALUES (@PartnerID, @Name, @WidgetType, 0, @SiteSectionId, (Case When @IsDeleted = 1 Then 0 Else 1 End), @EnterDTM, @UserId, @EnterDTM, @UserId)

	END

	IF @@ERROR = 0
	BEGIN
		SELECT @NewID = SCOPE_IDENTITY();

		Insert WidgetsLegacy(LauncherID, ContentItemID, DCId, VanityName, WidgetIdentificationName, IsDefault, DefaultTab, GenerateNow,
		TemplateZip, Height, Width)
		Values(@NewID, @ContentItemID, @DCId, @VanityName, @WidgetIdentificationName, @IsDefault, @DefaultTab, @GenerateNow,
		@TemplateZip, @Height, @Width)
	END
	IF @@ERROR = 0
		COMMIT TRANSACTION
	ELSE
		ROLLBACK TRANSACTION

	Select @NewID as ID;
	set nocount off

END
GO
