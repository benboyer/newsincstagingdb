USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[WidgetPlayer_UPDATE_sp]
	 @WidgetId int,
      @DisableSocialNetworking bit = null,
      @DisableAds bit = null,
      @DisableVideoPlayOnLoad bit = null,
      @DisableSingleEmbed bit= null,
      @PartnerLogo varbinary(max) = null,
      @StyleSheet varbinary(max) = null,
      @ContinuousPlay int = null,
      @PlayerID int = null,
      @Layout int = null,
      @DisableKeywords bit = null
AS
BEGIN
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'WidgetPlayer_UPDATE_sp'

	BEGIN TRANSACTION

	UPDATE Launcher set PlayerTypeID = @PlayerID
	WHERE LauncherID = @WidgetId

	IF @@ERROR = 0
	BEGIN
		UPDATE WidgetPlayerLegacy SET DisableSocialNetworking=@DisableSocialNetworking, DisableAds=@DisableAds, DisableVideoPlayOnLoad=@DisableVideoPlayOnLoad,
		DisableSingleEmbed=@DisableSingleEmbed, PartnerLogo=@PartnerLogo, StyleSheet=@StyleSheet, DisableKeywords=@DisableKeywords, ContinuousPlay=@ContinuousPlay
		WHERE LauncherID = @WidgetId
	END
	IF @@ERROR = 0
		COMMIT TRANSACTION
	ELSE
		ROLLBACK TRANSACTION

   Return @@ERROR;

END
GO
