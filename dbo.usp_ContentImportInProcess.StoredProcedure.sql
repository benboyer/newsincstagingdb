USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_ContentImportInProcess]
as
BEGIN
	set fmtonly off
	SET NOCOUNT ON
	exec SprocTrackingUpdate 'usp_ContentImportInProcess'

	declare @now datetime = GETDATE()

	select f.ContentImportSystemID ImportSystem, f.FeedPriorityID Priority, c.ContentID, c.CreatedDate, c.EffectiveDate PublishDate, c.UpdatedDate, p.name Partner, c.Name VideoName, /*c.PartnerID, */i.name ContentImportStatus, s.Name AssetImportStatus,
			tp.Name TargetPlatform, mt.MediaType, isnull(fca.duration, a.Duration) 'Duration (sec)',  COUNT(*) Assets,
			case
				when isnull(c.UpdatedDate, c.CreatedDate) < DATEADD(dd, -7, GETDATE()) then 'One week or more'
				when isnull(c.UpdatedDate, c.CreatedDate) < DATEADD(hh, -24, GETDATE()) then 'One day or more'
				when isnull(c.UpdatedDate, c.CreatedDate) < DATEADD(hh, -12, GETDATE()) then 'Half day'
				when isnull(c.UpdatedDate, c.CreatedDate) < DATEADD(hh, -6, GETDATE()) then 'Quarter day'
				when isnull(c.UpdatedDate, c.CreatedDate) < DATEADD(hh, -1, GETDATE()) then 'Over an hour'
				else convert( varchar(4), DATEDIFF(MINUTE, c.createddate, GETDATE())) + ' minutes' -- 'Under one hour'
				end as ProcessingTime
	from Content (nolock) c
	join Partner (nolock) p
	on		c.PartnerID = p.PartnerID
	join Content_Asset (nolock) ca
	on	 c.ContentID = ca.ContentID
	join importstatus (nolock) i
	on	c.ContentImportStatus = i.importstatusid
	join AssetType (nolock) at
	on ca.AssetTypeID = at.AssetTypeID
	join MimeType (nolock) mt
	on	at.MimeTypeID = mt.MimeTypeID
	join Asset (nolock) a
	on	ca.AssetID = a.AssetID
	left join FeedContentAsset (nolock) fca
	on		a.FeedContentAssetID = fca.FeedContentAssetID
	join ImportStatus (nolock) s
	on	a.ImportStatusID = s.ImportStatusID
	join	TargetPlatform (nolock) tp
	on		ca.TargetPlatformID = tp.TargetPlatformID
	left join Feed (nolock) f
	on		c.FeedID = f.FeedId
	where	c.ContentImportStatus not in (5, 9) --<> 5 -- (5, 9)
	and		a.ImportStatusID not in (5, 9)-- <> 5 --(5, 9)
	and		c.EffectiveDate <= @now
	and	not exists (
		select 1
		from	content (nolock) c2
		join	Content_Asset (nolock) ca2
		on		c2.ContentID = ca2.ContentID
		join	Asset (nolock) a2
		on		ca2.AssetID = a2.AssetID
		where	ca2.ContentID = c.ContentID
		and		c2.Active = 1
		and		ca2.Active = 1
		and		a2.Active = 1		)
	group by f.ContentImportSystemID, f.FeedPriorityID, c.ContentID, c.createddate, c.EffectiveDate, c.updateddate, p.name, c.Name, /*c.PartnerID, */i.name, tp.Name, s.Name, mt.MediaType, isnull(fca.duration, a.Duration)
	--	order by c.ContentID, c.createddate, c.updateddate, mt.MediaType, s.Name
	order by isnull(c.UpdatedDate, c.CreatedDate) desc,
		c.ContentID,
		tp.Name,
		mt.MediaType
	set nocount off
END
GO
