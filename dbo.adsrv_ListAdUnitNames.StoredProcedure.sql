USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[adsrv_ListAdUnitNames]
	--declare 
	@Ownershipstatus int = null, -- 0 = unassociated
							-- 1 = associated
	@PartnerStatus int = null,	-- 1=pending, 2=open or pending, 3=closed, 4=deleted
	@detailed bit = 0			-- 0=no, 1=yes
as
BEGIN
	set fmtonly OFF
	set nocount ON
	exec SprocTrackingUpdate 'adsrv_ListAdUnitNames'
	
	select @Ownershipstatus = isnull(@Ownershipstatus,1)
	select @partnerstatus = isnull(@partnerstatus, 2)
	if @detailed = 0
	begin
		select --og.OwnershipGroupID, p.PartnerID, p.TrackingGroup, 
				case when og.ownershipgroupid IS null then 'unassigned' else og.OwnershipGroup end as Organization, p.Name PartnerName
				, ogp.OwnershipAdUnitName /* lower(dbo.GetNOspecialChars(og.OwnershipGroup)) */ OrganizationAdUnitName
				, ogp.TrackingGroupAdUnitName /* lower(dbo.GetNOspecialChars(p.name) + '_' + CONVERT(varchar(10), p.trackinggroup)) */ TrackingGroupAdUnitName
		from (select a.PartnerID, a.Name, a.TrackingGroup, a.ContactID 
			from Partner (nolock) a
			join mTrackingGroup (nolock) mt
			on	a.PartnerID = mt.partnerid
			and a.TrackingGroup = mt.TrackingGroup
			where a.TrackingGroup <> 0 and a.TrackingGroup is not null) p
			
		join [User] (nolock) u
		on	p.ContactID = u.UserID
		join NDNUsersLEGACY (nolock) ul
		on	p.ContactID = ul.UserID
		left join ownershipgroup_partner (nolock) ogp
		on	p.PartnerID = ogp.partnerid
		left join ownershipgroup (nolock) og
		on ogp.ownershipgroupid = og.ownershipgroupid
		where u.StatusID = case when @PartnerStatus > 2 then @PartnerStatus else 2 end
		and ul.AccountStatus < case when @PartnerStatus > 2 then 2 else 3 end
		and ((og.ownershipgroupid is null and @Ownershipstatus = 0) or (og.ownershipgroupid is not null and @Ownershipstatus = 1))
		order by og.OwnershipGroup, p.Name
	END
	if @detailed = 1
	begin
		select og.OwnershipGroupID, p.PartnerID, p.TrackingGroup, 
				case when og.ownershipgroupid IS null then 'unassigned' else og.OwnershipGroup end as Organization, p.Name PartnerName
				, ogp.OwnershipAdUnitName /* lower(dbo.GetNOspecialChars(og.OwnershipGroup)) */ OrganizationAdUnitName
				, ogp.TrackingGroupAdUnitName /* lower(dbo.GetNOspecialChars(p.name) + '_' + CONVERT(varchar(10), p.trackinggroup)) */ TrackingGroupAdUnitName
		from (select a.PartnerID, a.Name, a.TrackingGroup, a.ContactID 
			from Partner (nolock) a
			join mTrackingGroup (nolock) mt
			on	a.PartnerID = mt.partnerid
			and a.TrackingGroup = mt.TrackingGroup
			where a.TrackingGroup <> 0 and a.TrackingGroup is not null) p
		join [User] (nolock) u
		on	p.ContactID = u.UserID
		join NDNUsersLEGACY (nolock) ul
		on	p.ContactID = ul.UserID
		left join ownershipgroup_partner (nolock) ogp
		on	p.PartnerID = ogp.partnerid
		left join ownershipgroup (nolock) og
		on ogp.ownershipgroupid = og.ownershipgroupid
		where u.StatusID = case when @PartnerStatus > 2 then @PartnerStatus else 2 end
		and ul.AccountStatus < case when @PartnerStatus > 2 then 2 else 3 end
		and ((og.ownershipgroupid is null and @Ownershipstatus = 0) or (og.ownershipgroupid is not null and @Ownershipstatus = 1))
		order by og.OwnershipGroup, p.Name
	END

	set nocount OFF
END
GO
