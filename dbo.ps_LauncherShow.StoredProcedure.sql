USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ps_LauncherShow]
	@widgetID int = null,
	@trackingGroup int = null,
	@deviceTypeID int = 1
AS
BEGIN
	set fmtonly off
	set nocount on
/*
	applicable tickets
		DB-27
*/
	exec SprocTrackingUpdate 'ps_LauncherShow'

	declare @partnerid int,
			@distPartnerid int,
			@AdServer varchar(26)

	if isnull(@deviceTypeID, 0) = 0 --is null
		set @deviceTypeID = 1
	if @widgetID is null
		set @widgetID = 1
	if isnull(@trackingGroup, 0) = 0-- is null
		set @trackingGroup = (select trackinggroup from Partner where PartnerID = (select PartnerID from launcher where LauncherID = @widgetID))
		--set @trackingGroup = 10557
	if @trackingGroup = 0
		set  @trackingGroup = 10557
	if not exists (select 1 from partner where TrackingGroup = @trackingGroup)
		set @trackingGroup = 10557

	declare @iwidgetID int,
			@itrackinggroup int,
			@iDeviceTypeID int,
			@iPTRenable int,
			@iPlayOnMouseover int

	select @iwidgetID = @widgetID, @itrackinggroup = @trackingGroup, @iDeviceTypeID = @deviceTypeID
	set @partnerid = (select partnerid from Launcher where LauncherID = @iwidgetID)
	set @distPartnerid = (select partnerid from mTrackingGroup (nolock) where TrackingGroup = @trackinggroup)
	set @AdServer = isnull((select value from vw_partnerDefaultSettings where PartnerID = @distPartnerid and Entity = 'AdServer'), (select value from vw_sysDefaultSettings where Entity = 'AdServer'))
	set @iPTRenable = isnull((select value from vw_partnerDefaultSettings where PartnerID = @distPartnerid and Entity = 'PTREnabled'), (select value from vw_sysDefaultSettings where Entity = 'PTREnabled'))
	set @iPlayOnMouseover = isnull((select value from vw_partnerDefaultSettings where PartnerID = @distPartnerid and Entity = 'playOnMouseover'), (select value from vw_sysDefaultSettings where Entity = '@PlayOnMouseover'))

	select top 1 *
	from
		(SELECT   l.LauncherID, isnull(isnull(l.SectionName, s2.name), 'ndn') SiteSectionName, convert(smallint, 1) AllowFullScreen, @itrackinggroup TrackingGroup,
				l.DisableEmailButton  as DisableSocialNetworking,
				l.DisableAdsOnPlayer DisableAds, l.DisableAutoPlay DisableVideoPlayOnLoad, l.DisableShareButton DisableSingleEmbed, l.ContinuousPlay,
				coalesce(l.Width, ltem.Width, pt.Width) Width, coalesce(l.Height, ltem.height, pt.height) Height ,
				case when isnull(p.ZoneID, 0) = 0 then 50974 else p.ZoneID end as ZoneID
				 , isnull(lu2.LandingURL, 'http://www.newsinc.com') LandingUrl_Hulu , isnull(lu.LandingURL,
				 isnull(p.LandingURL, 'http://landing.newsinc.com/shared/video.html')) LandingURL,
				 isnull(isnull(palo43.logourl, pld43.LogoURL), 'http://assets.newsinc.com/partnerlogo.jpg') LogoURL43,
				 isnull(isnull(palo169.logourl, pld169.logourl), 'http://assets.newsinc.com/partnerlogo169.jpg') LogoURL169,
				 ISNULL(isnull(paloD.LogoURL, plddisplay.logourl), 'no logo') LogoURLDisplay,
				 m.Name DistributorName,  isnull(replace(nc.NDNCategory, 'NDN Viral Network', 'News Distribution Network, Inc.'), 'News Distribution Network, Inc.') ProducerCategory, l.LauncherID WidgetID, m.ShortName DistributorNameAlt,
				 l.LaunchInLandingPage LandingPageAllowed, l.TransitionSeconds as VideoInterval, @AdServer AdServer, @iPTRenable PTREnabled, @iPlayOnMouseover PlayOnMouseover
		FROM		(select la.*, sa.name SectionName from Launcher (nolock) la left join Section  (nolock) sa on la.SectionID = sa.SectionID where la.LauncherID = @iwidgetID) l
		left join	PlayerType pt (nolock)
		on			l.PlayerTypeID = pt.PlayerTypeID
		left join	(select pl.partnerid, pl.launcherid, l.LogoURL
					from	partner_logo pl (nolock)
					join	Logo l (nolock)
					on		pl.logoid = l.LogoID
					where	pl.PartnerID = @partnerid
					and		l.LogoTypeID = 1
					and		l.Active = 1) palo43
		on			@distPartnerid /*l.PartnerID */ = palo43.partnerid
		and			l.LauncherID = palo43.LauncherID
		left join	(select top 1 pl.PartnerID, logourl
					from	Partner_Logo pl  (nolock)
					join	Logo lo  (nolock)
					on		pl.LogoID = lo.LogoID
					where	pl.PartnerID = @partnerid
					and		lo.LogoTypeID = 1
					and		lo.PartnerDefault = 1
					and		lo.Active = 1) pld43
		on			@distPartnerid /*l.PartnerID */ = pld43.partnerid
		left join	(select pl.partnerid, pl.LauncherID, l.LogoURL
					from	partner_logo pl (nolock)
					join	Logo l (nolock)
					on		pl.logoid = l.LogoID
					where	pl.PartnerID = @partnerid
					and		l.LogoTypeID = 2
					and		l.Active = 1) palo169

	on			@distPartnerid /* l.PartnerID */ = palo169.partnerid
		and		l.LauncherID = palo169.LauncherID
		left join	(select top 1 pl.PartnerID, logourl
					from	Partner_Logo pl  (nolock)
					join	Logo lo  (nolock)
					on		pl.LogoID = lo.LogoID
					where	pl.PartnerID = @partnerid
					and		lo.LogoTypeID = 2
					and		lo.PartnerDefault = 1
					and		lo.Active = 1) pld169
		on			@distPartnerid /*l.PartnerID */ = pld169.partnerid
		left join	(select pl.partnerid, pl.LauncherID, l.LogoURL
					from	partner_logo pl (nolock)
					join	Logo l (nolock)
					on		pl.logoid = l.LogoID
					where	l.LogoTypeID = 6
					and		l.Active = 1) paloD
		on			@distPartnerid /*l.PartnerID */ = paloD.partnerid
		and			l.LauncherID = paloD.LauncherID
		left join	(select top 1 pl.PartnerID, logourl
					from	Partner_Logo pl  (nolock)
					join	Logo lo  (nolock)
					on		pl.LogoID = lo.LogoID
					where	pl.PartnerID = @partnerid
					and		lo.LogoTypeID = 6
					and		lo.PartnerDefault = 1
					and		lo.Active = 1) plddisplay
		on			@distPartnerid /* l.PartnerID */ = plddisplay.partnerid
		left join LauncherTemplate ltem (nolock)
		on		l.LauncherTemplateID = ltem.LauncherTemplateID
		left join LauncherTypes lt (nolock)
		on		ltem.Launchertypesid = lt.LauncherTypesID
		join	mTrackingGroup mt (nolock)
		on		@itrackinggroup = mt.TrackingGroup
		join	Partner m (nolock)
		on		mt.partnerid = m.PartnerID
		left join	(select m.partnerid, lu.LandingURL
					from	(select partnerid from mTrackingGroup where TrackingGroup = @itrackinggroup) m
					join	Partner_LandingURL lu2 (nolock)
					on		m.partnerid = lu2.PartnerID
					join	LandingURL lu (nolock)
					on		lu2.LandingURLID = lu.LandingURLID
					join	LandingURLType lt  (nolock)-- select * from LandingURLType
					on		lu.LandingURLTypeID = lt.LandingURLTypeID
					where	lt.Name = 'Partner Default' and lu.active = 1) lu
		on	@distPartnerid /* l.PartnerID */ = lu.partnerid
		left join	(select m.partnerid, lu.LandingURL
					from	(select partnerid from mTrackingGroup (nolock) where TrackingGroup = @itrackinggroup) m
					join	Partner_LandingURL lu2 (nolock)
					on		m.partnerid = lu2.PartnerID
					join	LandingURL lu (nolock)
					on		lu2.LandingURLID = lu.LandingURLID
					join	LandingURLType lt (nolock)
					on		lu.LandingURLTypeID = lt.LandingURLTypeID
					where	lt.Name = 'hulu' and lu.active = 1) lu2
		on	m.PartnerID = lu2.partnerid
		left join  Playlist_Content_PartnerMap (nolock) pm --	(select max(NDNCategoryID) NDNCategoryID, PartnerID from Playlist_Content_PartnerMap (nolock) group by PartnerID) pm  -- select * from playlist_content_partnermap where feedid is not null
		on			l.PartnerID = pm.PartnerID
		and			pm.NDNCategoryID is not null
		left join	NDNCategory (nolock) nc
		on			pm.NDNCategoryID = nc.NDNCategoryID
		left join	 Partner p  (nolock)
		on			@itrackinggroup = p.TrackingGroup
		left join (select * from section (nolock) where sectionid = (select max(sectionid)  from Section (nolock) where PartnerID = @partnerid and [default] = 1)) s2
		on			m.partnerid = s2.PartnerID
		) z
	order by LauncherID desc

	set nocount off
END
GO
