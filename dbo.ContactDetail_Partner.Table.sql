USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContactDetail_Partner](
	[ContactDetailID] [int] NOT NULL,
	[PartnerID] [int] NOT NULL,
 CONSTRAINT [PK_ContactDetail_Partner] PRIMARY KEY CLUSTERED 
(
	[ContactDetailID] ASC,
	[PartnerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContactDetail_Partner]  WITH CHECK ADD  CONSTRAINT [FK_ContactDetail_Partner_ContactDetail] FOREIGN KEY([ContactDetailID])
REFERENCES [dbo].[ContactDetail] ([ContactDetailID])
GO
ALTER TABLE [dbo].[ContactDetail_Partner] CHECK CONSTRAINT [FK_ContactDetail_Partner_ContactDetail]
GO
ALTER TABLE [dbo].[ContactDetail_Partner]  WITH CHECK ADD  CONSTRAINT [FK_ContactDetail_Partner_Partner] FOREIGN KEY([PartnerID])
REFERENCES [dbo].[Partner] ([PartnerID])
GO
ALTER TABLE [dbo].[ContactDetail_Partner] CHECK CONSTRAINT [FK_ContactDetail_Partner_Partner]
GO
