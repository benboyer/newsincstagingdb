USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AssetType](
	[AssetTypeID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](40) NOT NULL,
	[MimeTypeID] [int] NULL,
	[MinHeight] [int] NULL,
	[MaxHeight] [int] NULL,
	[MinWidth] [int] NULL,
	[MaxWidth] [int] NULL,
	[MinBitrate] [int] NULL,
	[MaxBitrate] [int] NULL,
	[IdealWidth] [int] NULL,
	[IdealBitrate] [int] NULL,
	[PlayerAssetType] [varchar](20) NULL,
	[Comment] [varchar](max) NULL,
	[Process] [bit] NULL,
	[Transcode] [bit] NULL,
	[CreateFile] [bit] NULL,
	[CreateFileExtension] [varchar](10) NULL,
	[CustomProcessingID] [int] NULL,
 CONSTRAINT [PK_AssetType] PRIMARY KEY CLUSTERED 
(
	[AssetTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_AssetType_Mimetype] ON [dbo].[AssetType] 
(
	[MimeTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AssetType]  WITH CHECK ADD  CONSTRAINT [FK_AssetType_MimeType] FOREIGN KEY([MimeTypeID])
REFERENCES [dbo].[MimeType] ([MimeTypeID])
GO
ALTER TABLE [dbo].[AssetType] CHECK CONSTRAINT [FK_AssetType_MimeType]
GO
