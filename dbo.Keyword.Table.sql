USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Keyword](
	[KeywordID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[KeywordTypeID] [tinyint] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IncludeKeywords] [nvarchar](1000) NULL,
	[ExcludeKeywords] [nvarchar](1000) NULL,
	[ExpirationUTCDate] [smalldatetime] NULL,
	[CreatedUTCDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Keyword] PRIMARY KEY CLUSTERED 
(
	[KeywordID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IE_Keyword_ExpirationUTCDate] ON [dbo].[Keyword] 
(
	[ExpirationUTCDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Keyword]  WITH CHECK ADD  CONSTRAINT [FK_Keyword_KeywordType] FOREIGN KEY([KeywordTypeID])
REFERENCES [dbo].[KeywordType] ([KeywordTypeID])
GO
ALTER TABLE [dbo].[Keyword] CHECK CONSTRAINT [FK_Keyword_KeywordType]
GO
ALTER TABLE [dbo].[Keyword] ADD  CONSTRAINT [DF__Keyword__IsActiv__4028AB40]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Keyword] ADD  CONSTRAINT [DF__Keyword__Created__411CCF79]  DEFAULT (getutcdate()) FOR [CreatedUTCDate]
GO
