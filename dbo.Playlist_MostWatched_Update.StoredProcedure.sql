USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Playlist_MostWatched_Update]
as
BEGIN
	/*
		Applicable tickets:
			[a whole host of others over the past three years, then:]
			DB-33
	*/

	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'Playlist_MostWatched_Update'
	
	declare @PartnerID bigint, @PlaylistID bigint, @StartDTM datetime, @EndDTM datetime, @MaxRows int, @utcoffset int, @ProviderOrDistributor char(1)
	set @utcoffset = 0 -- (select DATEDIFF(hh, getutcdate(), getdate()))
	declare @tmpResults table (id int, ContentPlays bigint, ContentID bigint)

	-- Get list of playlists/parameters to be updated
	select PartnerID, PlaylistID, dateadd(hh, -LookbackHours - @utcoffset, getdate()) StartDTM, dateadd(hh, LookbackDuration, dateadd(hh, -LookbackHours - @utcoffset, getdate())) EndDTM, MaxRows, ProviderOrDistributor
	into	#mostwatchedupdates -- select * -- select * from #mostwatchedupdates -- select min(playlistid) from #mostwatchedupdates -- delete from #mostwatchedupdates where playlistid <> 16625
	from	playlist_MostWatchedParams
	where	isEnabled = 1
	and		(lastrun is null 
				or 
			round(datediff(n, lastrun, getdate())/convert(float, 60), 0) >= RefreshHours) 
	if (select count(*) from #mostwatchedupdates) > 0
	begin
		delete from [EC2-54-227-117-100.COMPUTE-1.AMAZONAWS.COM].lookups.dbo.playlist_MostWatchedParams
		insert into [EC2-54-227-117-100.COMPUTE-1.AMAZONAWS.COM].lookups.dbo.playlist_MostWatchedParams(PartnerID,PlaylistID,LookbackHours,LookbackDuration,RefreshHours,MaxRows,lastrun,ProviderOrDistributor,isEnabled)
		select PartnerID,PlaylistID,LookbackHours,LookbackDuration,RefreshHours,MaxRows,lastrun,ProviderOrDistributor,isEnabled
		from playlist_MostWatchedParams
		where isenabled = 1
	end

	while (select count(*) from #mostwatchedupdates) > 0
		Begin
			delete from @tmpResults
			--	declare @providerOrDistributor char(1), @PartnerID bigint, @PlaylistID bigint, @StartDTM datetime, @EndDTM datetime, @MaxRows int, @utcoffset int set @utcoffset = 0 declare @tmpResults table (id int, ContentPlays bigint, ContentID bigint)
			set @PlaylistID = (select min(PlaylistID) from #mostwatchedupdates)
			set @PartnerID = (select min(PartnerID) from #mostwatchedupdates where Playlistid = @PlaylistID)
			set @StartDTM = (select distinct StartDTM from #mostwatchedupdates where PlaylistID = @PlaylistID)
			set @EndDTM = (select distinct EndDTM from #mostwatchedupdates where PlaylistID = @PlaylistID)
			set @MaxRows = (select distinct MaxRows from #mostwatchedupdates where PlaylistID = @PlaylistID)
			set @ProviderOrDistributor = (select distinct ProviderOrDistributor from #mostwatchedupdates where PlaylistID = @PlaylistID)
			select @partnerid, @playlistid, @startDTM, @endDTM, @ProviderOrDistributor, @maxrows
			-- Don't go back more than 24 hours, don't pull when the end time < start time
			IF	@StartDTM < dateadd(hh, -25 - @utcoffset, getdate())
				or
				@EndDTM <= @StartDTM
			Begin
				print 'Parameters are FUBAR!'
				Return 
			End

			exec [EC2-54-227-117-100.COMPUTE-1.AMAZONAWS.COM].newsincrpt.dbo.Playlist_MostWatched_pull @PartnerID, @playlistid, @StartDTM, @EndDTM, @MaxRows, @utcoffset, @ProviderOrDistributor

			insert into @tmpResults(id, ContentPlays, ContentID) -- select * from #tmpResults
			select PlaylistMWid, videoviews, videoid from [EC2-54-227-117-100.COMPUTE-1.AMAZONAWS.COM].Lookups.dbo.PlaylistMW
			select * from @tmpResults

			if (select COUNT(*) from @tmpResults) > 0
			begin
				--select @playlistid, * from @tmpResults
				-- Remove contents of the playlist that we are updating
				delete from Playlist_Content where playlistid = @PlaylistID
				-- Insert the new contents of the playlist we are updating
				insert into Playlist_Content (PlaylistID, ContentID, [Order]) -- declare @Playlistid int set @playlistid = 10218
				select distinct @PlaylistID, ContentID, id as [Order] from @tmpResults
				update Playlist set ContentUpdatedDate = GETDATE() where PlaylistID = @PlaylistID
				-- Remove contents from legacy table
				delete from PlaylistVideosLEGACY where	 PlaylistID = @PlaylistID
				-- Insert the new contents into the legacy table so the GUI code will work right
				insert into PlaylistVideosLEGACY(playlistid, contentid)
				select distinct playlistid, contentid from Playlist_Content pc
				where	pc.PlaylistID = @PlaylistID
			end

			--	Update the parameters table to show this playlist has been updated
			update playlist_MostWatchedParams set lastrun = getdate() where PlaylistID = @PlaylistID

			--	Remove the row for this playlist from our temp table of playlists that need to be updated		
			delete from #mostwatchedupdates
			where	PlaylistID = @PlaylistID
		End
	drop table #mostwatchedupdates
	set nocount on
END
GO
