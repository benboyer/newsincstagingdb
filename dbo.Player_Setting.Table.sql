USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Player_Setting](
	[PlayerID] [int] NOT NULL,
	[SettingID] [int] NOT NULL,
	[Value] [nvarchar](50) NOT NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[UpdatedDate] [smalldatetime] NULL,
 CONSTRAINT [PK_PlayerSettings] PRIMARY KEY CLUSTERED 
(
	[PlayerID] ASC,
	[SettingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Player_Setting]  WITH CHECK ADD  CONSTRAINT [FK_PlayerSettings_Setting] FOREIGN KEY([SettingID])
REFERENCES [dbo].[Setting] ([SettingID])
GO
ALTER TABLE [dbo].[Player_Setting] CHECK CONSTRAINT [FK_PlayerSettings_Setting]
GO
ALTER TABLE [dbo].[Player_Setting] ADD  CONSTRAINT [DF__Player_Se__Creat__5EE9FC26]  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
