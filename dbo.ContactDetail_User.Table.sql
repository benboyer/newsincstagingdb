USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContactDetail_User](
	[UserID] [int] NOT NULL,
	[ContactDetailID] [int] NOT NULL,
 CONSTRAINT [PK_Contact_User] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[ContactDetailID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContactDetail_User]  WITH CHECK ADD  CONSTRAINT [FK_ContactDetail_User_ContactDetail] FOREIGN KEY([ContactDetailID])
REFERENCES [dbo].[ContactDetail] ([ContactDetailID])
GO
ALTER TABLE [dbo].[ContactDetail_User] CHECK CONSTRAINT [FK_ContactDetail_User_ContactDetail]
GO
ALTER TABLE [dbo].[ContactDetail_User]  WITH CHECK ADD  CONSTRAINT [FK_ContactDetail_User_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[ContactDetail_User] CHECK CONSTRAINT [FK_ContactDetail_User_User]
GO
