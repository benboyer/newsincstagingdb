USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[adm_ExceptionalProcess]
as
BEGIN
	set FMTONLY OFF
	set NOCOUNT ON
	exec SprocTrackingUpdate 'adm_ExceptionalProcess'
	
	declare @partnerid int, @procedurename varchar(200), @lastContentID bigint, @user varchar(20), @sql varchar(5000), @Update bit = 1
	select	@partnerid = PartnerID, @procedurename = ProcedureName, @lastContentID = LastContentID, @user = 'admin'
		-- select *
	from	admExceptionalProcessing
	where	Active = 1
	and		(LastRunDTM is null
			or DATEADD(MINUTE, RunintervalMinutes, LastRunDTM) <= GETDATE() )
	select	@partnerid, @procedurename, @lastContentID
	set		@sql = 'exec ' + @procedurename + ' ' + CONVERT(varchar(20), @partnerID) + ', ' + convert(varchar(20), @lastContentID) + ', ' +  @user + ',' + CONVERT(char(1), @update)
	select	@sql
	if @sql is not null
	begin
		exec	(@sql)
	end

	set NOCOUNT OFF
END
GO
