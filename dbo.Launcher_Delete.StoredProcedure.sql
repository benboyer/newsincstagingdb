USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Launcher_Delete]
		@LauncherID int = null
AS
BEGIN
	UPDATE Launcher SET
		Active = 0
	WHERE
		LauncherID = @LauncherID
END
GO
