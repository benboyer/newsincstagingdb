USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_DeleteAssetsOlderThanUpdates]
	--	declare 
	@contentid bigint
as 
BEGIN
	set FMTONLY OFF
	set NOCOUNT ON
	exec SprocTrackingUpdate 'usp_DeleteAssetsOlderThanUpdates'
	declare @tpcount int = 3 * (select count(distinct TargetPlatformID) TargetPlatformIDs
	from	Content_Asset 
	where	ContentID = @contentid)

	if	(select distinct ImportStatusID	
		from	(select top (@tpcount) *
			from	Content_Asset 
			where	ContentID = @contentid
			order by AssetID desc) a)
		in (5,7)
	and		exists (select 1
			from	Content_Asset
			where	ContentID = @contentid
			and		ImportStatusID = 6)
	BEGIN 
			-- declare @contentid bigint = 23558140 
			select contentid, AssetID
			into #oldassets
			from Content_Asset (nolock)
			where	contentid = @contentid
			and		ImportStatusID = 6

			delete b
			-- select count(*)
			from	#oldassets a
			join	Content_Asset b
			on		a.ContentID = b.ContentID
			and		a.AssetID = b.AssetID
			
			delete b
			from	#oldassets a
			join	Asset b
			on		a.AssetID = b.AssetID

			drop table #oldassets			
	END
	if @@ERROR = 0
	select 'Process Successful' as MessageBack
	set NOCOUNT OFF
END
GO
