USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[adm_ContentKeywordsToCategory]
	(
	--declare 
	@partnerid int, @lastContentID bigint, @user varchar(20) = 'admin', @Exproc bit = 1
	)
as
BEGIN

	set FMTONLY OFF
	set NOCOUNT ON
	-- Brought to you by the fine folks at Ticket DBRequest-1345

	exec SprocTrackingUpdate 'adm_ContentKeywordsToCategory'
	
	declare @workme bigint,
			@workcat varchar(max),
			@longstring varchar(max),
			@shortstring varchar(50)

	declare @CIDs table (ContentID bigint)
	insert into @CIDs(ContentID)

	select top 100 Contentid 
	from	Content 
	where	PartnerID = @partnerid
	and		ContentID > isnull(@lastContentID, 0)
	order by ContentID

	while (select COUNT(*) from @CIDs ) > 0
	begin
		set @shortstring = null
		set @longstring = null
		set @workcat = null
		set @workme = (select MIN(contentid) from @CIDs)
		set @workcat = (select keyword from Content where ContentID = @workme)

		if	(select COUNT(*)
			from	dbo.fn_Rows_fmCommaSeparated(@workcat)) > 0
		begin
			set @shortstring = 'womenshealth'
			if exists (select 1 from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)
			set @longstring = (select case when @longstring IS not null then @longstring + ','+ Value else Value end
			from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)

			set @shortstring = 'cancer'
			if exists (select 1 from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)
			set @longstring = (select case when @longstring IS not null then @longstring + ','+ Value else Value end
			from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)

			set @shortstring = 'breastcancer'
			if exists (select 1 from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)
			set @longstring = (select case when @longstring IS not null then @longstring + ','+ Value else Value end
			from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)

			set @shortstring = 'coldandflu'
			if exists (select 1 from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)
			set @longstring = (select case when @longstring IS not null then @longstring + ','+ Value else Value end
			from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)

			set @shortstring = 'diabetes'
			if exists (select 1 from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)
			set @longstring = (select case when @longstring IS not null then @longstring + ','+ Value else Value end
			from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)

			set @shortstring = 'dieting'
			if exists (select 1 from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)
			set @longstring = (select case when @longstring IS not null then @longstring + ','+ Value else Value end
			from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)

			set @shortstring = 'sportsmed'
			if exists (select 1 from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)
			set @longstring = (select case when @longstring IS not null then @longstring + ','+ Value else Value end
			from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)

			set @shortstring = 'hearthealth'
			if exists (select 1 from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)
			set @longstring = (select case when @longstring IS not null then @longstring + ','+ Value else Value end
			from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)

			set @shortstring = 'kidsminute'
			if exists (select 1 from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)
			set @longstring = (select case when @longstring IS not null then @longstring + ','+ Value else Value end
			from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)

			set @shortstring = 'menshealth'
			if exists (select 1 from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)
			set @longstring = (select case when @longstring IS not null then @longstring + ','+ Value else Value end
			from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)

			set @shortstring = 'mentalhealth'
			if exists (select 1 from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)
			set @longstring = (select case when @longstring IS not null then @longstring + ','+ Value else Value end
			from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)

			set @shortstring = 'science'
			if exists (select 1 from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)
			set @longstring = (select case when @longstring IS not null then @longstring + ','+ Value else Value end
			from dbo.fn_Rows_fmCommaSeparated(@workcat) where Value = @shortstring)
		end
		if @longstring is not null 
		begin
			-- select @workme CID, @longstring NewCategory, 'KeywordsToCategory' KeywordsToCategory
			exec	RefTableInserts @workme, @longstring, 'KeywordsToCategory'
		end

		if @Exproc = 1
		begin
			update	admExceptionalProcessing
			set		LastContentID = @workme,
					UpdatedDTM = GETDATE(),
					UpdatedUser = @user 
			where	PartnerID = @partnerid
			and		ProcedureName = 'adm_ContentKeywordsToCategory'
		end

		delete from @CIDs where ContentID = @workme
	end

		update	admExceptionalProcessing
		set		LastRunDTM = GETDATE(),
				UpdatedUser = @user 
		where	PartnerID = @partnerid
		and		ProcedureName = 'adm_ContentKeywordsToCategory'

	set NOCOUNT OFF
END
GO
