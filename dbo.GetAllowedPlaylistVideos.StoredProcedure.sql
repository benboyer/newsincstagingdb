USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllowedPlaylistVideos]
	@playlistID int,
	@trackingGroup int,
	@devicetype int = 1
AS
BEGIN
	exec SprocTrackingUpdate 'GetAllowedPlaylistVideos'
	-- declare 	@playlistID int = 13866, @trackingGroup int = 90121, @devicetype int = 1 -- 90121
	declare
	@iplaylistID int,
	@itrackinggroup int,
	@iDeviceType int

	select @iplaylistID = @playlistID, @itrackinggroup = @trackingGroup, @iDeviceType = @devicetype
	SELECT
		c.ContentID as VideoID -- declare @iDeviceType int = 1 select top 10 *
	FROM Playlist_Content pc
	INNER JOIN Content c
	ON		c.ContentID = pc.ContentID
	join	Content_Asset ca
	on		c.ContentID = ca.ContentID
	and		@iDeviceType = ca.TargetPlatformID
	join	AssetType at
	on		ca.AssetTypeID = at.AssetTypeID
	join	MimeType mt
	on		at.MimeTypeid = mt.MimeTypeID
	and		(mt.MediaType ='Video' or mt.MediaType ='ExternalID')
	WHERE	pc.PlaylistID = @iplaylistID
	and		dbo.AllowedContentID(c.contentid, @itrackinggroup, @iDeviceType) = 1
	ORDER BY pc.[order]
end
GO
