USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetContentIDByNameDescriptionKeyword] @name varchar(100), @page int = 1, @size int = 100 as      
      
if LEN(@name) < 3      
 return      
      
-- select * into #temp from CONTAINSTABLE (content, (name, description, keyword), @name)      

select top 10000 ContentID as [key] into #temp from content where ( name like '%'+@name+'%' or description like '%'+@name+'%' or keyword like '%'+@name+'%' ) and Active = 1 
order by createddate desc

select *, @page as page, ( select COUNT(*) from #temp ) as cnt from       
(      
select contentid, name, [description], keyword, antikw, partnerid, ( select name from partner where partnerid = content.partnerid) as partnername, RANK ( ) OVER ( order by contentid desc ) as rnk from Content      
 inner join       
  (select * from #temp) sub      
 on content.contentid = sub.[key]      
) data      
where rnk between ( @page - 1 ) * @size + 1 and ( ( @page - 1 ) * @size ) + @size      
order by ContentID desc
GO
