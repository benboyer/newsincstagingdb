USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileContentVideoImportConfig](
	[FeedID] [int] NOT NULL,
	[PartnerID] [int] NOT NULL,
	[ContentFilePath] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_FileContentVideoImportConfig] PRIMARY KEY CLUSTERED 
(
	[FeedID] ASC,
	[PartnerID] ASC,
	[ContentFilePath] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
