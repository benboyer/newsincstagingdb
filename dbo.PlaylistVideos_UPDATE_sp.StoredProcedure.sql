USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PlaylistVideos_UPDATE_sp]  
   @VideoID int,  
      @SortOrder int,  
      @Sharing int,  
      @PlaylistID int,  
      @ContentItemID int  
AS  
BEGIN  
 SET NOCOUNT ON;  
 exec SprocTrackingUpdate 'PlaylistVideos_UPDATE_sp'  
  
 BEGIN TRANSACTION  
  
 UPDATE Playlist_Content set [Order] = @SortOrder  
 WHERE PlaylistID = @PlaylistID and ContentID = @VideoID  
 IF @@ERROR = 0  
 BEGIN  
  UPDATE PlaylistVideosLEGACY SET  Sharing =@Sharing, ContentItemID =@ContentItemID  
  WHERE PlaylistID = @PlaylistID and ContentID = @VideoID  
    
  UPDATE Playlist  
  SET ContentUpdatedDate = GETDATE()  
  where PlaylistID = @PlaylistID  
 END  
  
 IF @@ERROR = 0  
  COMMIT TRANSACTION  
 ELSE  
  ROLLBACK TRANSACTION  
  
 RETURN @@ERROR  
  
  
END
GO
