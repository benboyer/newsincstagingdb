USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[launchersPlayersCombined](
	[LauncherTypesID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](20) NULL,
	[Description] [nvarchar](60) NULL,
	[LauncherCompatible] [bit] NOT NULL,
	[PlayerCompatible] [bit] NOT NULL,
	[LegacyPlayerTypesName] [varchar](50) NULL,
	[LegacyPlayerTypesID] [int] NULL,
 CONSTRAINT [PK_launcherSPlayersCombined] PRIMARY KEY CLUSTERED 
(
	[LauncherTypesID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[launchersPlayersCombined] ADD  CONSTRAINT [DF_launcherSPlayersCombined_LauncherCompatible]  DEFAULT ((1)) FOR [LauncherCompatible]
GO
ALTER TABLE [dbo].[launchersPlayersCombined] ADD  CONSTRAINT [DF_launcherSPlayersCombined_PlayerCompatible]  DEFAULT ((1)) FOR [PlayerCompatible]
GO
