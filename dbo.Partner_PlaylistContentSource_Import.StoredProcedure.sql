USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Partner_PlaylistContentSource_Import]
as
BEGIN
	select ppm.PartnerID, ppm.PlaylistID, c.ContentID, ca.Content_AssetID, a.AssetID
	into #sourcecontent
	from	Partner_PlaylistContentSource_Map ppm
	join	Playlist_Content pc
	on		ppm.playlistid = pc.PlaylistID
	join	Content c
	on		pc.ContentID = c.ContentID
	join	Content_Asset ca
	on		c.ContentID = ca.ContentID
	join	Asset a
	on		ca.AssetID = a.AssetID
	where	c.active = 1
	and		pc.ContentID > isnull(ppm.LastContentID, 0)
	and	not exists
		(select 1
		from	Content c
		where	c.PartnerID = 193
		and		c.SourceGUID = convert(varchar(100), pc.ContentID))

	-- select * from #sourcecontent
	declare @partnerid int, @ContentID bigint, @ContentIDNew bigint, @AssetID bigint, @PlaylistID int
	while (select COUNT(*) from #sourcecontent) > 0
	BEGIN
		set @partnerid = (select MIN(partnerid) from #sourcecontent)
		select @partnerid PartnerID
		while exists (select 1 from #sourcecontent where partnerid = @partnerid)
			BEGIN
				set @ContentID = (select MIN(ContentID) from #sourcecontent where partnerid = @partnerid)
				select @ContentID ContentID
				insert into Content(ContentTypeID, PartnerID, Version, ContentSourceID, Name, Description, Category, Keyword, Duration, EffectiveDate, ExpirationDate, CreatedDate, CreatedUserID, UpdatedDate, UpdatedUserID, Active, IsFileArchived, isDeleted, FeedID, SourceGUID, ContentImportStatus, TranscriptPath, TimesUpdated)
				select ContentTypeID, @partnerid PartnerID, Version, 4 ContentSourceID, Name, Description, Category, Keyword, Duration, EffectiveDate, ExpirationDate, getdate() CreatedDate, CreatedUserID, getdate() UpdatedDate, -1 UpdatedUserID, 0 Active, IsFileArchived, isDeleted, FeedID, ContentID SourceGUID, ContentImportStatus, TranscriptPath, 0 TimesUpdated
				from Content where ContentID = @ContentID
				set @ContentIDNew = (select max(contentid) from Content where PartnerID = @partnerid and SourceGUID = convert(varchar(100), @ContentID))
				select @ContentIDNew NewContentID
				while exists (select 1 from #sourcecontent where partnerid = @partnerid and ContentID = @ContentID)
				BEGIN
					set @AssetID = (select MIN(assetid) from #sourcecontent where partnerid = @partnerid and ContentID = @ContentID)
					set @playlistid = (select PlaylistID from #sourcecontent where partnerid = @partnerid and ContentID = @ContentID and AssetID = @AssetID)
					select @AssetID, @playlistid PlaylistID
					insert into content_asset(ContentID, AssetID, AssetTypeID, TargetPlatformID, ImportStatusID, Active)
					select	@ContentIDNew ContentID, @Assetid AssetID, AssetTypeID, TargetPlatformID, ImportStatusid, Active
					from	Content_Asset where Content_AssetID = (select Content_AssetID from  #sourcecontent where partnerid = @partnerid and  ContentID = @ContentID and AssetID = @AssetID)
					if (select COUNT(*) from #sourcecontent where partnerid = @partnerid and ContentID = @ContentID) = 1
					begin

						insert into ContentVideo (ContentID, GUID, ContentVideoImportStatusID, FileName, ImportFilePath, Height, Width, Duration, ThumbnailImportFilePath, ThumbnailFileName, StillframeImportFilePath, StillframeFileName)
						-- declare @contentid int = 23757994, @contentIDnew bigint = 23777731
						select con.*, th.ImportFilePath, th.FileName, sf.ImportFilePath, sf.FileName
						from	(-- declare @contentid int = 4613
								select c.ContentID, c.SourceGUID,
									4 AS ContentImportStatus, --'http://' + a.FilePath  + '/' +  a.filename  as ImportFilePath, 'http://' + a.FilePath + '/' + a.filename  as FileName,
									'http://video.newsinc.com/' + convert(varchar(20), @ContentID) + '.flv' as FileName, 'http://Rawvideo.newsinc.com/' + convert(varchar(20), @ContentID) + '.flv' as ImportFilePath,
										a.Height, a.Width, a.Duration
								-- declare @contentid int = 23569498 select * from (select c.*
								from	(select * from Content (nolock) where contentid = @ContentIDNew) c

		join	Content_Asset ca (nolock)
								on		c.ContentID = ca.ContentID
								join	TargetPlatform_AssetType tpat
								on		ca.AssetTypeID = tpat.AssetTypeID
								join	Asset a (nolock)
								on		ca.AssetID = a.AssetID
								join	AssetType at (nolock)
								on		tpat.AssetTypeID = at.AssetTypeID
								join	MimeType mt (nolock)
								on		a.MimeTypeID = mt.MimeTypeID
								and		at.MimeTypeID = mt.MimeTypeID
								join	FeedContentAsset fca (nolock)
								on		a.FeedContentAssetID = fca.FeedContentAssetID
								left join ContentVideo cv (nolock)
								on		c.ContentID = cv.ContentID
								where	cv.ContentID is null
								and		tpat.TargetPlatformID = 1
								and		ca.TargetPlatformID = 1
								and		at.AssetTypeID = 1) con
						join	(select c.ContentID, 'http://rawvideo.newsinc.com/' + filename ImportFilePath, 'http://dev.thumbs.newsinc.com/' + convert(varchar(20), @ContentID) + '.jpg' as FileName
								from	(select * from Content (nolock) where contentid = @ContentIDNew) c
								join	Content_Asset ca (nolock)
								on		c.ContentID = ca.ContentID
								join	TargetPlatform_AssetType tpat (nolock)
								on		ca.AssetTypeID = tpat.AssetTypeID
								join	Asset a (nolock)
								on		ca.AssetID = a.AssetID
								join	AssetType at (nolock)
								on	tpat.AssetTypeID = at.AssetTypeID
								join	MimeType mt (nolock)
								on		a.MimeTypeID = mt.MimeTypeID
								and		at.MimeTypeID = mt.MimeTypeID
								where	tpat.TargetPlatformID = 1
								and		ca.TargetPlatformID = 1
								and		at.AssetTypeID = 2) th
						on		con.ContentID = th.ContentID
						join	(select c.ContentID, 'http://rawvideo.newsinc.com/' + filename ImportFilePath, 'http://dev.thumbs.newsinc.com/' + convert(varchar(20), @ContentID) + '.sf.jpg' as FileName
								-- 'http://' + a.FilePath  + '/' +  a.filename  as ImportFilePath, 'http://' + a.FilePath + '/' + a.filename  as FileName
								from	(select * from Content (nolock) where ContentID = @ContentIDNew) c
								join	Content_Asset ca (nolock)
								on		c.ContentID = ca.ContentID
								join	TargetPlatform_AssetType tpat (nolock)
								on		ca.AssetTypeID = tpat.AssetTypeID
								join	Asset a (nolock)
								on		ca.AssetID = a.AssetID
								join	AssetType at (nolock)
								on	tpat.AssetTypeID = at.AssetTypeID
								join	MimeType mt (nolock)
								on		a.MimeTypeID = mt.MimeTypeID
								and		at.MimeTypeID = mt.MimeTypeID
								where	tpat.TargetPlatformID = 1
								and		ca.TargetPlatformID = 1
								and		at.AssetTypeID = 3) sf
						on		con.ContentID = sf.ContentID


						insert into VideosLEGACY(ContentID, isAdvertisement, SentToFreeWheel, isDeleted)
						select	@ContentIDNew, 0, 0, 0
						where not exists(select contentid from videoslegacy (nolock) vl
								where	contentid = @ContentIDNew)

						insert into VideoStagingLEGACY(ContentID, fileExtension, NexidiaStatus, NiaStatus)
						-- declare @contentidnew int = 23777731
						select	c.ContentID, mt.FileExtension, 0, 0
						from	(select * from Content (nolock) where ContentID = @ContentIDNew) c
							join	Content_Asset ca (nolock)
							on		c.ContentID = ca.ContentID
							join	TargetPlatform_AssetType tpat
							on		ca.AssetTypeID = tpat.AssetTypeID
							join	Asset a (nolock)
							on		ca.AssetID = a.AssetID
							join	AssetType at (nolock)
							on	tpat.AssetTypeID = at.AssetTypeID
							join	MimeType mt (nolock)
							on		a.MimeTypeID = mt.MimeTypeID
							and		at.MimeTypeID = mt.MimeTypeID
							left join VideoStagingLEGACY cv (nolock)
							on		c.ContentID = cv.ContentID
							where	cv.ContentID is null
							and		ca.TargetPlatformID = 1
							and		tpat.TargetPlatformID = 1
							and		at.AssetTypeID = 1

						update Content set Active = (select Active from Content (nolock) where ContentID = @ContentID), UpdatedDate = GETDATE(), UpdatedUserID = -1
						where	ContentID = @ContentIDNew


						update Partner_PlaylistContentSource_Map set lastcontentid = @ContentID where partnerid = @partnerid and playlistid = @playlistid

					end
					delete from #sourcecontent where Partnerid = @partnerid and ContentID = @ContentID and AssetID = @AssetID
				END
			END
	END

	drop table #sourcecontent
END
GO
