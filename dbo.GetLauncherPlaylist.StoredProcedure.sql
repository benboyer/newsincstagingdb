USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetLauncherPlaylist]  
 @widgetID int,  
 @trackingGroup int = null,  
 @playlistid bigint = null,  
 @DeviceType int = 1  
AS  
BEGIN  
 set nocount on  
 set fmtonly off  
 exec SprocTrackingUpdate 'GetLauncherPlaylist'  
  
 if @DeviceType = 3  
 begin  
  exec ps_LauncherPlaylist @widgetID, @trackinggroup, @playlistid, @devicetype  
  return  
 end  
/*  
hotfix 20130530  
*/  
----------------------------------------------  
 declare @now datetime = getdate(),  
   @iwidgetID int,  
   @itrackingGroup int,  
   @iPlaylistID bigint,  
   @iDeviceType int  
 set  @now = GETDATE()  
 select @iwidgetID = @widgetID, @itrackingGroup = isnull(@trackingGroup, 10557), @iDeviceType = isnull(@DeviceType, 1), @iPlaylistID = @playlistid  
 ---: CID is passed in and is assigned to launcher, or CID is not passed in  
  -- if @iPlaylistID is null or exists (select 1 from Launcher_Playlist (nolock) lp join Playlist (nolock) p on lp.PlaylistID = p.PlaylistID where lp.LauncherID = @iwidgetID and lp.PlaylistID= @iPlaylistID and p.Active = 1)  
  if exists (select 1 from Launcher_Playlist lp join Playlist_Content pc on lp.PlaylistID = pc.PlaylistID where lp.LauncherID = @iwidgetID and lp.PlaylistID = isnull(@iPlaylistID, lp.PlaylistID) and dbo.AllowedContentID(pc.ContentID, @itrackinggroup, 1) =
 1)  
  begin  
   select lp.PlaylistID, pl.name Title, convert(int, isnull([Order], 0)) as PlaylistOrder  
   --into #tmptable  
   -- declare @iwidgetid int = 21649, @itrackingGroup int = 90017, @iplaylistid bigint = 994, @iDeviceType int = 1 select *  
   from (select Launcherid, PlaylistID, [Order]  
     from Launcher_Playlist (nolock)  
     where LauncherID = @iwidgetID -- and PlaylistID = ISNULL(@iplaylistid, Playlistid)  
     and exists (select 1  
        from Playlist_Content  (nolock)  
        where playlist_content.PlaylistID = Launcher_Playlist.PlaylistID  
        and  dbo.AllowedContentID(contentid, @itrackinggroup, @iDeviceType) = 1)  
     ) lp  
   join Playlist pl (nolock)  
   on  lp.PlaylistID = pl.PlaylistID  
   where dbo.AllowedLauncherContent(@iwidgetid, @itrackinggroup) = 1  
  end  
  
 ---: CID passed in but not assigned to launcher  
  else  
  begin  
   if exists (select 1 from Playlist_Content where PlaylistID = @iPlaylistID and dbo.AllowedContentID(contentid, @itrackinggroup, 1) = 1)  
   begin  
    select pc.PlaylistID, pl.name Title, min(convert(int, isnull(pc.[Order], 0))) as PlaylistOrder  
    -- select pc.PlaylistID, pl.name Title, min(pc.[Order]) PlaylistOrder  
    from Playlist_Content pc (nolock)  
    join Playlist pl (nolock)  
    on  pc.PlaylistID = pl.PlaylistID  
    where dbo.AllowedLauncherContent(@iwidgetid, @itrackinggroup) = 1  
    and  pc.PlaylistID = ISNULL(@iplaylistid, pc.playlistid)  
    and  dbo.AllowedContentID(pc.contentid, @itrackinggroup, @iDeviceType) = 1  
    group by pc.playlistid, pl.name  
   end  
  
   ---: no cid passed in, not cid attached to launcher, use min playlistid assigned  
   else  
   begin  
      select top 1 pc.PlaylistID, p.name Title, convert(int, isnull(pc.[Order], -1)) as PlaylistOrder  
      -- select top 1 pc.PlaylistID, p.Name Title, CONVERT(int, -1) as PlaylistOrder  
      from playlist_content  (nolock) pc  
      join Playlist (nolock) p  
      on  pc.PlaylistID = p.PlaylistID  
      where pc.PlaylistID = 507  
      and  dbo.AllowedLauncherContent(@iwidgetid, @itrackinggroup) = 1  
      and  dbo.AllowedContentID(pc.ContentID, @itrackingGroup, @iDeviceType)  = 1  
      order by [Order]  
   end  
  end  
 set nocount off  
  
END
GO
