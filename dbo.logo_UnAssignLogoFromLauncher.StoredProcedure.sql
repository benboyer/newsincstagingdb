USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[logo_UnAssignLogoFromLauncher]
	@PartnerID int,
	@LauncherID int,
	@LogoID int
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'logo_UnAssignLogoFromLauncher'

		update
				partner_Logo
		set		LauncherID = null
		where  	PartnerID = @partnerid
		and		LauncherID = @LauncherID
		and		LogoID = @LogoID
	set nocount off
END
GO
