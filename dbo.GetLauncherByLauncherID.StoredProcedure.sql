USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetLauncherByLauncherID]
	-- declare
	@launcherid int
as
BEGIN
	set nocount on
	set fmtonly off
	exec SprocTrackingUpdate 'GetLauncherByLauncherID'
	-- declare @launcherid int = 8719
	select	l.LauncherID, l.PartnerID, l.Name, lt.LauncherTypesID LauncherTypeID, isnull(l.PlayerTypeID, 0) PlayerTypeID, l.SectionID, l.Active, l.CreatedDate, l.CreatedUserID, l.UpdatedDate, l.UpdatedUserID, l.isDefault, l.GenerateNow, l.Height, l.Width, l.ContinuousPlay, l.DisableEmailButton, l.DisableAdsOnPlayer, l.DisableShareButton, l.DisableAutoPlay, l.DisableFullScreen, ISNULL(l.LauncherTemplateID, 0) LauncherTemplateID, s.Name SiteSection,
			p.TrackingGroup, convert(bit, case when lt.LauncherTemplateID is null then 1 else 0 end) as Deprecated, lower(lts.Name) as LauncherType, l.LaunchInLandingPage LandingPageAllowed
			,lp.PlaylistID
	from Launcher l (nolock)
	join Partner p (nolock)
	on	l.PartnerID = p.PartnerID
	left join Section s (nolock)
	on	l.SectionID = s.SectionID
	left join launchertemplate lt (nolock)
	on		l.LauncherTemplateID = lt.LauncherTemplateID
	left join LauncherTypes lts (nolock)
	on		lt.LauncherTypesID = lts.LauncherTypesID
	left join Launcher_Playlist lp (nolock)
	on		l.LauncherID = lp.LauncherID
	where l.LauncherID = @launcherid
	and	(lp.LauncherID is null or lp.[Order] = (select MIN([order]) from Launcher_Playlist where LauncherID = @launcherid))

	set nocount off
END
GO
