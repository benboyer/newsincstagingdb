USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[CopyMobileCVidtoPremobileUpdateTest]
	@ContentID bigint
as
begin
-- select top 100 * from videostaginglegacy order by contentid desc
	exec SprocTrackingUpdate 'CopyMobileCVidtoPremobile'
	
	Declare @count int;
	
	Select @count = COUNT(*) from ContentVideo where ContentID = @ContentID
	
	if @count > 0
		begin
		
			Update ContentVideo 
			set ContentVideoImportStatusID = 7 
			where ContentID = @ContentID		
		end
	else
	begin
		

	insert into VideosLEGACY(ContentID, isAdvertisement, SentToFreeWheel, isDeleted)
	select	c.ContentID, 0, 0, 0
	from	Content c
	left join	videoslegacy vl
	on		c.ContentID = vl.ContentID
	where	c.contentid = @ContentID
	and		vl.ContentID is null

	insert into ContentVideo (ContentID, GUID, ContentVideoImportStatusID, FileName, ImportFilePath, Height, Width, Duration, ThumbnailImportFilePath, ThumbnailFileName, StillframeImportFilePath, StillframeFileName)
	-- declare @contentid bigint = 4613 -- select * from content
	select con.*, th.ImportFilePath, th.FileName, sf.ImportFilePath, sf.FileName
	from	(-- declare @contentid int = 4613
			select c.ContentID, c.SourceGUID,
				7 /*case c.ContentImportStatus
					when 5 then 4
					when 4 then 2
					when 3 then 2
					when 2 then 1
					when 1 then 1
					when 7 then 2
					else null
				end */ AS ContentImportStatus, --'http://' + a.FilePath  + '/' +  a.filename  as ImportFilePath, 'http://' + a.FilePath + '/' + a.filename  as FileName,
				'http://video.newsinc.com/' + convert(varchar(20), c.contentid) + '.flv' as FileName, 'http://Rawvideo.newsinc.com/' + convert(varchar(20), c.contentid) + '.flv' as ImportFilePath,
					a.Height, a.Width, a.Duration
			from	Content c
			join	Content_Asset ca
			on		c.ContentID = ca.ContentID
			join	TargetPlatform_AssetType tpat
			on		ca.AssetTypeID = tpat.AssetTypeID
			join	Asset a
			on		ca.AssetID = a.AssetID
			join	AssetType at
			on	tpat.AssetTypeID = at.AssetTypeID
			join	MimeType mt
			on		a.MimeTypeID = mt.MimeTypeID
			and		at.MimeTypeID = mt.MimeTypeID
			join	FeedContentAsset fca
			on		a.FeedContentAssetID = fca.FeedContentAssetID
			left join ContentVideo cv
			on		c.ContentID = cv.ContentID
			where	c.ContentID = @ContentID
			and		cv.ContentID is null
			and		tpat.TargetPlatformID = 1
			and		at.AssetTypeID = 1) con
	join	(select c.ContentID, 'http://rawvideo.newsinc.com/' + filename ImportFilePath, 'http://thumbnail.newsinc.com/' + convert(varchar(20), c.contentid) + '.jpg' as FileName
				-- 'http://' + a.FilePath  + '/' +  a.filename  as ImportFilePath, 'http://' + a.FilePath + '/' + a.filename  as FileName
			-- select *
			from	Content c
			join	Content_Asset ca
			on		c.ContentID = ca.ContentID
			join	TargetPlatform_AssetType tpat
			on		ca.AssetTypeID = tpat.AssetTypeID
			join	Asset a
			on		ca.AssetID = a.AssetID
			join	AssetType at
			on	tpat.AssetTypeID = at.AssetTypeID
			join	MimeType mt
			on		a.MimeTypeID = mt.MimeTypeID
			and		at.MimeTypeID = mt.MimeTypeID
			where	tpat.TargetPlatformID = 1
			and		at.AssetTypeID = 2) th
	on		con.ContentID = th.ContentID
	join	(select c.ContentID, 'http://rawvideo.newsinc.com/' + filename ImportFilePath, 'http://thumbnail.newsinc.com/' + convert(varchar(20), c.contentid) + '.sf.jpg' as FileName
			-- 'http://' + a.FilePath  + '/' +  a.filename  as ImportFilePath, 'http://' + a.FilePath + '/' + a.filename  as FileName
			from	Content c
			join	Content_Asset ca
			on		c.ContentID = ca.ContentID
			join	TargetPlatform_AssetType tpat
			on		ca.AssetTypeID = tpat.AssetTypeID
			join	Asset a
			on		ca.AssetID = a.AssetID
			join	AssetType at
			on	tpat.AssetTypeID = at.AssetTypeID
			join	MimeType mt
			on		a.MimeTypeID = mt.MimeTypeID
			and		at.MimeTypeID = mt.MimeTypeID
			where	tpat.TargetPlatformID = 1
			and		at.AssetTypeID = 3) sf
	on		con.ContentID = sf.ContentID


	if @@ERROR = 0
	begin
		insert into VideosLEGACY(ContentID, IsAdvertisement, SentToFreeWheel, IsDeleted)
		-- declare @contentid bigint = 19
		Select @ContentID, 0, 0, 0
		where not exists (select 1 from VideosLEGACY where ContentID = @ContentID)

		insert into VideoStagingLEGACY(ContentID, fileExtension, NexidiaStatus, NiaStatus)
		select	c.ContentID, mt.FileExtension, 0, 0
		from	Content c
			join	Content_Asset ca
			on		c.ContentID = ca.ContentID
			join	TargetPlatform_AssetType tpat
			on		ca.AssetTypeID = tpat.AssetTypeID
			join	Asset a
			on		ca.AssetID = a.AssetID
			join	AssetType at
			on	tpat.AssetTypeID = at.AssetTypeID
			join	MimeType mt
			on		a.MimeTypeID = mt.MimeTypeID
			and		at.MimeTypeID = mt.MimeTypeID
			left join VideoStagingLEGACY cv
			on		c.ContentID = cv.ContentID
			where	c.ContentID = @ContentID
			and		cv.ContentID is null
			and		tpat.TargetPlatformID = 1
			and		at.AssetTypeID = 1

		update c set c.ContentImportStatus = 5 -- ca.importstatusid
		from	Content c
		join	Content_Asset ca
		on		c.ContentID = ca.ContentID
		join	AssetType at
		on		ca.AssetTypeID = at.AssetTypeID
		where	c.contentid = @ContentID
		and		at.Name like 'Video%'
		and		isnull(c.ContentImportStatus, 11) = 11
	end
	select @@ERROR as returnoutput;

	/*
		if @@ERROR = 0
		begin
			insert into VideosLEGACY(ContentID, IsAdvertisement, SentToFreeWheel, IsDeleted)
			-- declare @contentid bigint = 19
			Select @ContentID, 0, 0, 0
			where not exists (select 1 from VideosLEGACY where ContentID = @ContentID)
		end
	*/
	
	select @@ERROR as returnoutput;
end

end
GO
