USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UploadContent](
	[UploadContentID] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PartnerID] [int] NOT NULL,
	[GUID] [varchar](250) NOT NULL,
	[FeedContentID] [bigint] NULL,
	[PublishDate] [datetime] NULL,
	[ExpirationDate] [datetime] NULL,
	[Name] [varchar](500) NULL,
	[Description] [varchar](1000) NULL,
	[Keywords] [varchar](1000) NULL,
	[Categories] [varchar](250) NULL,
	[Version] [int] NOT NULL,
	[ImportComplete] [bit] NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[OnlyOwnerView] [bit] NOT NULL,
	[ContentSourceID] [int] NOT NULL,
 CONSTRAINT [PK_UploadContent] PRIMARY KEY CLUSTERED 
(
	[UploadContentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[UploadContent]  WITH CHECK ADD  CONSTRAINT [FK_UploadContent_Partner] FOREIGN KEY([PartnerID])
REFERENCES [dbo].[Partner] ([PartnerID])
GO
ALTER TABLE [dbo].[UploadContent] CHECK CONSTRAINT [FK_UploadContent_Partner]
GO
ALTER TABLE [dbo].[UploadContent] ADD  CONSTRAINT [DF_UploadContent_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[UploadContent] ADD  CONSTRAINT [DF_UploadContent_OnlyOwnerView]  DEFAULT ((0)) FOR [OnlyOwnerView]
GO
ALTER TABLE [dbo].[UploadContent] ADD  CONSTRAINT [DF_UploadContent_ContentSourceID]  DEFAULT ((0)) FOR [ContentSourceID]
GO
