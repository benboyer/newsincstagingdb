USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlaylistsLEGACY](
	[PlaylistID] [int] NOT NULL,
	[ContentItemID] [int] NULL,
	[SharingOption] [int] NULL,
	[SortOrder] [int] NOT NULL,
	[AverageRate] [float] NOT NULL,
	[IsDefault] [bit] NOT NULL,
	[RSSFilename] [nvarchar](255) NULL,
	[RSSEnabled] [bit] NULL,
 CONSTRAINT [PK_PlaylistsLEGACY] PRIMARY KEY CLUSTERED 
(
	[PlaylistID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
