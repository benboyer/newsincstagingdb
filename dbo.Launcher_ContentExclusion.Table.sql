USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Launcher_ContentExclusion](
	[LauncherID] [int] NOT NULL,
	[PartnerID] [int] NOT NULL,
 CONSTRAINT [PK_LauncherContentExclusion] PRIMARY KEY CLUSTERED 
(
	[LauncherID] ASC,
	[PartnerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Launcher_ContentExclusion]  WITH CHECK ADD  CONSTRAINT [FK_Launcher_ContentExclusion_Partner] FOREIGN KEY([PartnerID])
REFERENCES [dbo].[Partner] ([PartnerID])
GO
ALTER TABLE [dbo].[Launcher_ContentExclusion] CHECK CONSTRAINT [FK_Launcher_ContentExclusion_Partner]
GO
ALTER TABLE [dbo].[Launcher_ContentExclusion]  WITH CHECK ADD  CONSTRAINT [Launcher_Launcher_ContentExclusion] FOREIGN KEY([LauncherID])
REFERENCES [dbo].[Launcher] ([LauncherID])
GO
ALTER TABLE [dbo].[Launcher_ContentExclusion] CHECK CONSTRAINT [Launcher_Launcher_ContentExclusion]
GO
