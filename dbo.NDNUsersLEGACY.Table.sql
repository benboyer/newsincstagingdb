USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NDNUsersLEGACY](
	[UserID] [int] NOT NULL,
	[EmailID] [nvarchar](256) NOT NULL,
	[ReceiveDailyIQEmail] [bit] NULL,
	[MI] [nvarchar](60) NULL,
	[Gender] [char](1) NULL,
	[DOB] [datetime] NULL,
	[AddressID] [int] NULL,
	[ContentItemID] [int] NULL,
	[PartnerId] [nvarchar](255) NULL,
	[FreeWheelType] [nvarchar](20) NOT NULL,
	[IsAdmin] [bit] NOT NULL,
	[UserTypeID] [tinyint] NOT NULL,
	[IsFtpEnabled] [bit] NULL,
	[XSLT] [text] NULL,
	[AccountStatus] [int] NOT NULL,
	[AllowVideoUpload] [bit] NOT NULL,
	[ProposedOrganization] [nvarchar](500) NULL,
	[EnableRssFeed] [bit] NOT NULL,
	[LandingURL] [nvarchar](2083) NULL,
	[CustomRssFeedXSLT] [nvarchar](255) NULL,
 CONSTRAINT [PK_UsersLEGACY] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[NDNUsersLEGACY]  WITH CHECK ADD  CONSTRAINT [FK_NDNUsersLEGACY_UserTypesLegacy] FOREIGN KEY([UserTypeID])
REFERENCES [dbo].[UserTypesLegacy] ([UserTypeID])
GO
ALTER TABLE [dbo].[NDNUsersLEGACY] CHECK CONSTRAINT [FK_NDNUsersLEGACY_UserTypesLegacy]
GO
ALTER TABLE [dbo].[NDNUsersLEGACY] ADD  CONSTRAINT [DF_NDNUsersLEGACY_IsAdmin]  DEFAULT ((0)) FOR [IsAdmin]
GO
