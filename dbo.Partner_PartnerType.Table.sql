USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Partner_PartnerType](
	[PartnerID] [int] NOT NULL,
	[PartnerTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_Partner_Type_1] PRIMARY KEY CLUSTERED 
(
	[PartnerID] ASC,
	[PartnerTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Partner_PartnerType]  WITH CHECK ADD  CONSTRAINT [FK_Partner_Type_PartnerType] FOREIGN KEY([PartnerTypeID])
REFERENCES [dbo].[PartnerType] ([PartnerTypeID])
GO
ALTER TABLE [dbo].[Partner_PartnerType] CHECK CONSTRAINT [FK_Partner_Type_PartnerType]
GO
ALTER TABLE [dbo].[Partner_PartnerType]  WITH CHECK ADD  CONSTRAINT [Partner_Partner_PartnerType] FOREIGN KEY([PartnerID])
REFERENCES [dbo].[Partner] ([PartnerID])
GO
ALTER TABLE [dbo].[Partner_PartnerType] CHECK CONSTRAINT [Partner_Partner_PartnerType]
GO
