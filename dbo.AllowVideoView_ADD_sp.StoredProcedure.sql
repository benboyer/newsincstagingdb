USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AllowVideoView_ADD_sp]
	@ContentProviderOrgID int,
	@DistributorOrgID int
AS
BEGIN
	set fmtonly off
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'AllowVideoView_ADD_sp'

	Insert into AllowContent(ContentProviderPartnerID, DistributorPartnerID, CreatedDate)
	Values(@ContentProviderOrgID, @DistributorOrgID, GETUTCDATE())

	SET NOCOUNT OFF
END
GO
