USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[NDNUsers]
AS
SELECT     dbo.[User].UserID, dbo.NDNUsersLEGACY.UserTypeID, dbo.[User].Login AS UserName, dbo.NDNUsersLEGACY.EmailID, dbo.[User].PasswordFormat, 
                      CAST(dbo.[User].PasswordSalt AS varbinary(128)) AS PasswordSalt, CAST(dbo.[User].Password AS varbinary(128)) AS Password, 
                      ISNULL(dbo.NDNUsersLEGACY.ReceiveDailyIQEmail, 0) AS ReceiveDailyIQEmail, dbo.[User].FirstName, dbo.NDNUsersLEGACY.MI, dbo.[User].LastName, 
                      dbo.NDNUsersLEGACY.Gender, dbo.NDNUsersLEGACY.DOB, dbo.NDNUsersLEGACY.AddressID, ISNULL(dbo.[User].PartnerID, 0) AS OrganizationID, 
                      ISNULL(dbo.NDNUsersLEGACY.ContentItemID, 0) AS ContentItemID, dbo.NDNUsersLEGACY.PartnerId, dbo.NDNUsersLEGACY.FreeWheelType, dbo.[User].Active, 
                      ISNULL(dbo.NDNUsersLEGACY.IsFtpEnabled, 0) AS IsFtpEnabled, dbo.NDNUsersLEGACY.XSLT, ISNULL(dbo.NDNUsersLEGACY.AccountStatus, 1) AS AccountStatus, 
                      ISNULL(dbo.[User].UpdatedDate, dbo.[User].CreatedDate) AS LastModified, ISNULL(dbo.NDNUsersLEGACY.IsAdmin, 0) AS IsAdmin, dbo.[User].PasswordToken, 
                      dbo.NDNUsersLEGACY.AllowVideoUpload, dbo.NDNUsersLEGACY.ProposedOrganization, ISNULL(dbo.NDNUsersLEGACY.EnableRssFeed, 0) AS EnableRssFeed, 
                      dbo.NDNUsersLEGACY.LandingURL, dbo.NDNUsersLEGACY.CustomRssFeedXSLT
FROM         dbo.[User] INNER JOIN
                      dbo.NDNUsersLEGACY ON dbo.[User].UserID = dbo.NDNUsersLEGACY.UserID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[45] 4[36] 2[6] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "NDNUsersLEGACY"
            Begin Extent = 
               Top = 24
               Left = 725
               Bottom = 155
               Right = 920
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "User"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 207
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Address_User"
            Begin Extent = 
               Top = 290
               Left = 243
               Bottom = 379
               Right = 403
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "UserTypeToPartnerType"
            Begin Extent = 
               Top = 109
               Left = 446
               Bottom = 216
               Right = 715
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ContactDetail"
            Begin Extent = 
               Top = 252
               Left = 444
               Bottom = 369
               Right = 615
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "ContactType"
            Begin Extent = 
               Top = 158
               Left = 724
               Bottom = 270
               Right = 922
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ContactDetail_User"
            Begin Extent = 
               Top = 147
               Left = 73
               Bottom = 279
               Right = 343
   ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'NDNUsers'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'         End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 31
         Width = 284
         Width = 1500
         Width = 1500
         Width = 2475
         Width = 1500
         Width = 1500
         Width = 1920
         Width = 1500
         Width = 2370
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1380
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 6360
         Alias = 3075
         Table = 2445
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'NDNUsers'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'NDNUsers'
GO
