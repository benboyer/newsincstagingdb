USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[fd_ListMediaSources]
	@feedguid uniqueidentifier = null
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'fd_ListMediaSources'
	declare @partnerid int = (select partnerid from Partner (nolock) where FeedGUID = @feedguid)
exec cr_ListMediaSources @partnerid
	set nocount off
END
GO
