USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_DeleteOrphanAssets]
as

begin
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'usp_DeleteOrphanAssets' 

	insert into Archives.dbo.AssetsNullPath_Deleted 
	select AssetID,AssetTypeID,MimeTypeID,Filename,FileSize,FilePath,ImportStatusID,Height,Width,Bitrate,Duration,Active,isArchived,isDeleted,FeedContentAssetID,AltFeedContentAssetID
	from Asset a (nolock)
	where not exists (select 1
			from Content_Asset (nolock) caa
			where caa.AssetID = a.AssetID)
	order by a.AssetID desc


	delete b -- select *
	from	Archives.dbo.AssetsNullPath_Deleted (nolock) a
	join	Asset b
	on		a.AssetID = b.AssetID
	where not exists (select 1 from Content_Asset ca where ca.AssetID = b.AssetID)
 	
	set nocount off

end
GO
