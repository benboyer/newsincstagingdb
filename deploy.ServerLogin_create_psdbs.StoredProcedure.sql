USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [deploy].[ServerLogin_create_psdbs]
	@username varchar(20),
	@password varchar(20),
	@DefaultDB varchar(20),
	@schema varchar(20) = null,
	@runasUser varchar(20) = null,
	@runasPass varchar(20) = null
as
BEGIN
	IF exists (select	1
			from	distribution.dbo.MSdistribution_agents a
			join	master.sys.servers s      
			on		a.subscriber_id = s.server_id
			where a.publisher_db = (select DB_NAME()) and a.publication like 'Player%')
	BEGIN
		declare @counter int
		declare @sql1 varchar(max)
		declare @servers table(id int, servername varchar(40))
		insert into @servers(id, servername)
		select a.id, s.name
		from distribution.dbo.MSdistribution_agents a, master.sys.servers s      
		where a.publisher_db = (select DB_NAME()) and a.publication like 'Player%'-- and s.name not in (''IP-0A08A360'')      
		and a.subscriber_id = s.server_id      
		order by a.id
		while (select COUNT(*) from @servers) > 0
		begin
			set @counter = (select MIN(id) from @servers)
			set @sql1 = 'sqlcmd -S ' + (select servername from @servers where id = @counter) + ' -U ' + @runasUser + ' -P ' + @runasPass + ' ' + 'exec deploy.ServerLogin_create ' + @username + ',' +@password+','+@DefaultDB+','+@schema+','+@runasUser+','+@runasPass
			select @sql1
			--exec xp_cmdshell  @cmd  
			delete from @servers where id = @counter
		end
	END
END
GO
