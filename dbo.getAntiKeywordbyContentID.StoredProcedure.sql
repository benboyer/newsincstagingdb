USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[getAntiKeywordbyContentID] @contentid int as   
select c.antikw, c.partnerid from Content c where ContentID = @contentid
GO
