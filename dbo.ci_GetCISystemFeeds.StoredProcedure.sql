USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ci_GetCISystemFeeds]
	@ContentImportSystemID int
as
BEGIN
	set		fmtonly off
	set		nocount on
	exec	SprocTrackingUpdate 'ci_GetCISystemFeeds'
	select	FeedID, FeedPriorityID, PartnerID, FeedUrl, MaxDownloads, MaxUpdates
	from	Feed
	where	ContentImportSystemID = @ContentImportSystemID
	and		Active = 1
	set		nocount off
END
GO
