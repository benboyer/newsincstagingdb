USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BlockedSites](
	[BlockedSitesID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Domain] [varchar](1000) NOT NULL,
	[Active] [bit] NOT NULL,
	[DateCreatedGMT] [datetime] NOT NULL,
	[DateUpdatedGMT] [datetime] NULL,
	[CreatedBy] [varchar](60) NULL,
	[UpdatedBy] [varchar](60) NULL,
	[Comment] [varchar](200) NULL,
 CONSTRAINT [PK_BlockedSites] PRIMARY KEY CLUSTERED 
(
	[BlockedSitesID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_BlockedSites_active] ON [dbo].[BlockedSites] 
(
	[Active] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NDX_BlkSit_dom] ON [dbo].[BlockedSites] 
(
	[Domain] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BlockedSites] ADD  CONSTRAINT [DF_BlockedSites_Active]  DEFAULT ((1)) FOR [Active]
GO
ALTER TABLE [dbo].[BlockedSites] ADD  CONSTRAINT [DF_BlockedSites_Dateadded]  DEFAULT (getutcdate()) FOR [DateCreatedGMT]
GO
