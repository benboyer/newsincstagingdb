USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ps_AdUnits]    
 @TrackingGroup int    
 AS    
BEGIN    
 set nocount on    
 set fmtonly off    
 exec SprocTrackingUpdate 'ps_AdUnits'    
 if exists (select 1 from mtrackinggroup p join OwnershipGroup_Partner ogp on p.PartnerID = ogp.PartnerID  where TrackingGroup = @TrackingGroup)    
 begin    
--  select LOWER(dbo.getnospecialchars(og.OwnershipGroup)) OwnershipAdUnitName, lower(dbo.getnospecialchars(p.Name)+'_'+CONVERT(varchar(20), p.TrackingGroup)) TrackingGroupAdUnitName    
--  from OwnershipGroup_Partner ogp    
--  join Partner p on ogp.PartnerID = p.PartnerID    
--  join OwnershipGroup og    
--  on ogp.OwnershipGroupID = og.OwnershipGroupID    
--  where ogp.PartnerID = (select PartnerID from mTrackingGroup where TrackingGroup = @TrackingGroup)    
  select ogp.OwnershipAdUnitName, ogp.TrackingGroupAdUnitName    
  from (select partnerid from mtrackinggroup (nolock) where TrackingGroup = @TrackingGroup) p    
  join ownershipgroup_partner (nolock) ogp    
  on  p.partnerid = ogp.PartnerID    
 end    
 else    
 begin    
  select ogp.OwnershipAdUnitName, ogp.TrackingGroupAdUnitName    
  from (select partnerid from mtrackinggroup (nolock) where TrackingGroup = 10557) p    
  join ownershipgroup_partner (nolock) ogp    
  on  p.partnerid = ogp.PartnerID    
 end    
 set nocount off    
END
GO
