USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Widgets_UPDATE_sp]
	  @WidgetID int,
      @ContentItemID int = null,
      @UserId int,
      @DCId int = null,
      @VanityName varchar(32) = null,
      @Name nvarchar(50),
      @WidgetType int = null,
      @WidgetIdentificationName nvarchar(100) = null,
      @IsDefault bit = null,
      @DefaultTab int = null,
      @IsDeleted bit,
      @LastUpdated datetime = null,
      @GenerateNow bit = null,
      @TemplateZip varbinary(max) = null,
      @Height int = null,
      @Width int = null,
      @SiteSectionId int = null,
      @EnterDTM smalldatetime,
      @LastChgDTM datetime = null
AS
BEGIN
	set nocount on
	exec SprocTrackingUpdate 'Widgets_UPDATE_sp'

	Declare @NewID int;
	Declare @PartnerID int;

	BEGIN TRANSACTION

	Update Launcher Set Name=@Name, LauncherTypeID=@WidgetType, SectionID=@SiteSectionId, Active=(Case When @IsDeleted = 1 Then 0 Else 1 End), UpdatedDate=@LastChgDTM, UpdatedUserID=@UserId
	Where Launcher.LauncherID = @WidgetID

	IF @@ERROR = 0
	BEGIN

		Update WidgetsLegacy SET ContentItemID=@ContentItemID, DCId=@DCId, VanityName=@VanityName, WidgetIdentificationName=@WidgetIdentificationName, IsDefault=@IsDefault, DefaultTab=@DefaultTab, GenerateNow=@GenerateNow,
		TemplateZip=@TemplateZip, Height=@Height, Width=@Width
		Where WidgetsLEGACY.LauncherID = @WidgetID

	END
	IF @@ERROR = 0
		COMMIT TRANSACTION
	ELSE
		ROLLBACK TRANSACTION

	Select @NewID as ID;
	set nocount off
END
GO
