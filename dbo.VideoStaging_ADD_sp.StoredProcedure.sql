USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[VideoStaging_ADD_sp]
	@UserID int,
	@ClipURL nvarchar(200),
	@Title nvarchar(100),
	@Description nvarchar(1000),
	@EventDate smalldatetime,
	@Height decimal(10,2),
	@Width decimal(10,2),
	@Duration decimal(10,2)=null,
	@CreatedOn smalldatetime,
	@ThumbnailURL nvarchar(200),
	@PublishDate smalldatetime,
	@Keywords nvarchar(500),
	@Guid nvarchar(500),
	@ReadyForEncoding bit=null,
	@OriginalName nvarchar(200),
	@EncodingStatus smallint,
	@EncodingPercentCompleted int=null,
	@EncodingStarted datetime=null,
	@EncodingCompleted datetime=null,
	@ExpiryDate smalldatetime,
	@Status int=null,
	@Active bit,
	@IsEnabled bit,
	@IsDeleted bit,
	@IngestionSource nvarchar(50),
	@FileSize int=null,
	@StillFrameURL nvarchar(200)=null,
	@AudioTrackURL nvarchar(1000)=null,
	@EncodedURL nvarchar(1000),
	@VideoId int,
	@NexidiaStatus int=null,
	@Mp3Started datetime=null,
	@Mp3Completed datetime=null,
	@NiaStatus int,
	@CodecUsed nchar(100)=null,
	@FileExtension nvarchar(5)=null,
	@Category nvarchar(50)



AS
BEGIN
	exec SprocTrackingUpdate 'VideoStaging_ADD_sp'

	if Isnull(@FileExtension, '') = ''
	begin
		set @FileExtension = (select RIGHT(@ClipURL, CHARINDEX('.', reverse(@ClipURL))))
	end
	if Isnull(@FileExtension, '') = ''

	begin
		set @FileExtension = (select RIGHT(@OriginalName, CHARINDEX('.', reverse(@OriginalName))))
	end

	SET NOCOUNT ON;
	Declare @NewID int;
	Declare @ErrorCode int;

	Declare @PartnerID int;

	Select @PartnerID = PartnerID
	From [User]
	Where UserID = @UserID;

    BEGIN TRANSACTION

	Insert into Content (
	ContentTypeID,
	PartnerID,
	ContentSourceID,
	Name,
	[Description],
	Category,
	Keyword,
	EffectiveDate,
	ExpirationDate,
	Active,
	CreatedDate,
	CreatedUserID)
	Values(1,
	@PartnerID,
	1,
	@Title,
	@Description,
	@Category,
	@Keywords,
	@PublishDate,
	@ExpiryDate,
	@Active,
	@CreatedOn,
	@UserID)

	if @@ERROR = 0
	BEGIN


	 select @NewID = SCOPE_IDENTITY()

	 Insert into ContentVideo(ContentID, [GUID], ContentVideoImportStatusID, [FileName], ImportFilePath, Height,
	 Width, Duration, ThumbnailImportFilePath, ThumbnailFileName, StillframeImportFilePath, StillframeFileName)
	 Values(@NewID, @Guid, @EncodingStatus, null, @ClipURL, @Height, @Width, @Duration, @ThumbnailURL, Null, @StillFrameURL, Null)

	END
	if @@ERROR = 0
	BEGIN
		Insert Into VideoStagingLEGACY
		(ContentID, FileExtension, NexidiaStatus, NiaStatus)
		Values(@NewID, @FileExtension, 0, 0);
	END
	if @@ERROR = 0
		COMMIT TRANSACTION
	ELSE
		ROLLBACK TRANSACTION

	SELECT @NewID as ID

END
GO
