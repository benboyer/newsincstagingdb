USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_Rows_fmCommaSeparated]
(
	@String varchar(2000)
)
RETURNS @Cattable TABLE
(Value varchar(200))
AS
BEGIN
		-- Change 2/14-02-26, DBREQUEST-1345 (deleted "used" portion of remaining string instead of using Replace() function to eliminate used portion from remaining source)
		declare @wholecat varchar(120)
		declare @lastCat varchar(120)
		-- declare @PartnerID bigint

		set		@wholecat = @String
		
		while	(select CHARINDEX(',', @wholecat, 1)) > 0
		begin
			set @lastCat = (select	left(@wholecat, CHARINDEX(',', @wholecat, 1) -1))
			insert @Cattable select rtrim(ltrim(@lastCat))
			set @lastCat = @lastCat + ','
			set @wholecat = right(@wholecat, LEN(@wholecat) -  CHARINDEX(',', @wholecat, 1))
			--set @wholecat = replace(@wholecat, @lastcat, '')
		end
		insert @Cattable select rtrim(ltrim(@wholecat))

	RETURN
END;
GO
