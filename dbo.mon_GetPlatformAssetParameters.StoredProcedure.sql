USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[mon_GetPlatformAssetParameters]
as
select tp.TargetPlatformID, tp.Name, tp.Format, at.assettypeid, at.Name, mt.MediaType, mt.MimeType, 
		case when at.MinWidth is not null then at.MinWidth else '' end as MinWidth, 
		case when at.MaxWidth is not null then at.MaxWidth else '' end as MaxWidth,
		case when at.IdealWidth is not null then at.IdealWidth else '' end as IdealWidth, 
		case when at.MinBitrate is not null then at.MinBitrate else '' end as MinBitrate,  
		case when at.MaxBitrate is not null then at.MaxBitrate else '' end as MaxBitrate, 
		case when at.IdealBitrate is not null then at.IdealBitrate else '' end as IdealBitrate, 
		convert(varchar(200), replace(at.Comment, 'test data', '')) Comment, 
		case when at.Transcode is not null then at.Transcode else 1 end as CanTranscode, 
		case when at.CreateFile is not null then at.CreateFile else '' end as CanCreateFile
from TargetPlatform tp
join TargetPlatform_AssetType tpat
on		tp.TargetPlatformID = tpat.TargetPlatformID
join	AssetType at
on		tpat.AssetTypeID = at.AssetTypeID
join	MimeType mt
on		at.MimeTypeID = mt.MimeTypeID
order by tp.TargetPlatformID, tp.Format
GO
