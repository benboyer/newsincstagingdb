USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PartnerContentImportFolder](
	[PartnerFolderID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PartnerID] [int] NOT NULL,
	[FolderPath] [varchar](500) NOT NULL,
	[XsltPath] [varchar](500) NOT NULL,
	[Enabled] [bit] NOT NULL,
	[ContentSourceID] [int] NOT NULL,
 CONSTRAINT [PK_PartnerContentImportFolder] PRIMARY KEY CLUSTERED 
(
	[PartnerFolderID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [PCIF_PID] ON [dbo].[PartnerContentImportFolder] 
(
	[PartnerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PartnerContentImportFolder] ADD  CONSTRAINT [DF_PartnerContentImportFolder_Enabled]  DEFAULT ((0)) FOR [Enabled]
GO
ALTER TABLE [dbo].[PartnerContentImportFolder] ADD  CONSTRAINT [DF_PartnerContentImportFolder_ContentSourceID]  DEFAULT ((0)) FOR [ContentSourceID]
GO
