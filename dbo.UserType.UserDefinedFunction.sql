USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[UserType] 
(@userID int)
RETURNS int
AS
BEGIN
	DECLARE @Return int
	SELECT @Return = CAST((SELECT RoleID FROM dbo.User_Role WHERE RoleID = 1 AND UserID = (SELECT [ContactID] FROM dbo.[Partner] WHERE PartnerID = @userID)) AS int)
RETURN @return
END
GO
