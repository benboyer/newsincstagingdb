USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_GetPartnerInfoByTrackingGroup]
--	declare
	@trackinggroup int
as
begin
	SET FMTONLY OFF
	set nocount on
	exec SprocTrackingUpdate 'usp_GetPartnerInfoByTrackingGroup'

	select p.PartnerID, p.Name, p.ZoneID, p.LandingURL, p.LogoURL, p.FeedGUID
	from	mTrackingGroup a
	join	partner p
	on		a.partnerid = p.PartnerID
	where	a.TrackingGroup = @trackinggroup

	set nocount off
end
GO
