USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Content_AdBlockKeyword_AddUpdate3]
as
BEGIN
	set fmtonly off
	set nocount on

	exec sprocTrackingUpdate 'Content_AdBlockKeyword_AddUpdate'

	declare @fromdate datetime = dateadd(dd, -100, getdate())
select @fromdate	
	insert into ads_NoAdContent(VideoID, KeywordAddPhrase)
	select distinct ContentID, b.KeywordAddPhrase
		-- select * -- ContentID, PartnerID, Name, CreatedDate
	from	(select * from ads_PhrasesToBlockAds where filteractive = 1) b
	join	(select * from Content (nolock) where CreatedDate > @fromdate) c
	on		(c.Name like b.phrase
			or
			c.Description like b.phrase
			or
			c.Keyword like b.phrase)
	and		not exists(
				select 1
				from	ads_NoAdContent (nolock) v
				where	v.videoid = c.ContentID)

	-- 	declare @fromdate datetime = dateadd(dd, -100, getdate())
		update c set c.antikw = case when c.antikw is null then a.keywordaddphrase
									when c.antikw is not null and LTRIM(rtrim(c.antikw)) = '' then a.keywordaddphrase
									when LEN(c.antikw) > 1 then c.antikw + ',' + a.keywordaddphrase
									end,
					c.UpdatedDate = GETDATE(),
					c.UpdatedUserID = 7534 
	-- 	declare @fromdate datetime = dateadd(dd, -100, getdate()) select *
	from	(select * from ads_NoAdContent (nolock) where filteraddeddate is null)  a
	join	Content (nolock) c
	on		a.videoid = c.ContentID
	where	c.CreatedDate > @fromdate
	and		isnull(c.antikw, '') not like '%' + a.KeywordAddPhrase + '%'
	and LEN(isnull(c.antikw, '')) < 980

	update x 
	set		x.FilterAddedDate = GETDATE(),
			x.UpdatedDate = GETDATE(),
			x.FilterActive = 1
	-- select *
	from	(select * from ads_NoAdContent (nolock) where filteraddeddate is null)  a
	join	content c 
	on		a.videoid = c.contentid
	join	ads_NoAdContent x
	on		a.ads_NoAdContentID = x.ads_NoAdContentID
	where	isnull(c.antikw, '') like '%' + a.KeywordAddPhrase + '%'

	set nocount off
END
GO
