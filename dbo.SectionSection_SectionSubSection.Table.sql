USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SectionSection_SectionSubSection](
	[SectionSection_SectionSubSectionID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[SectionSectionID] [int] NULL,
	[SectionSubSectionID] [int] NULL,
	[AllowPageTypes] [int] NULL,
 CONSTRAINT [PK_SectionSectionSubSection] PRIMARY KEY CLUSTERED 
(
	[SectionSection_SectionSubSectionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ndx_ss_sss_section] ON [dbo].[SectionSection_SectionSubSection] 
(
	[SectionSectionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ndx_ss_sss_subsection] ON [dbo].[SectionSection_SectionSubSection] 
(
	[SectionSubSectionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SectionSection_SectionSubSection] ADD  CONSTRAINT [DF_SectionSection_SectionSubSection_AllowStoryLevel]  DEFAULT ((0)) FOR [AllowPageTypes]
GO
