USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Partner_Partner_xRef](
	[Partner_Partner_xrefID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[MasterPartnerID] [int] NOT NULL,
	[PartnerSourceID] [varchar](20) NULL,
	[PartnerIdentifier] [varchar](10) NULL,
	[PartnerID] [int] NULL,
 CONSTRAINT [PK_Partner_Partner_xRef] PRIMARY KEY CLUSTERED 
(
	[Partner_Partner_xrefID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
