USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetContentFeedContent_2]
--declare
	@feedtoken uniqueidentifier = null, --'00000000-0000-0000-0000-000000000000',
--   @feedtoken uniqueidentifier = '54B6CAE7-E19B-4FF7-AED1-BFAB914EC47E',
      @providerid int = null,
    @datefrom datetime = null,
    @dateto datetime = null,
    @numbertoreturn int = 100,
    @updates bit = 0
--set @feedtoken = 'F82EAE93-A2B9-4319-AE53-E49519AF7B7C'
--set @numbertoreturn =11
--set @datefrom = '2012-03-05'
--set @dateto = '2012-03-05 23:00:00'
--set @updates = 0
AS
BEGIN
	-- set @feedtoken = '54B6CAE7-E19B-4FF7-AED1-BFAB914EC47E'
    SET NOCOUNT ON;
	-- to use for procedure execution statistics when we get table SPROCTracker in place
	--exec SprocTrackingUpdate 'usp_GetContentFeedContent'
	if @providerid is null
		set @providerid = 0
	if @numbertoreturn = 0 or @numbertoreturn is null
		set @numbertoreturn = 100
	if @feedtoken is null
	    set @feedtoken = '00000000-0000-0000-0000-000000000000'
    DECLARE @accesspartnerid int, @landingurl varchar(120), @itrackinggroup int, @now datetime
		select	@accesspartnerid = PartnerID,
				@landingurl = LandingURL,
				@itrackinggroup = trackinggroup
		from Partner where FeedGUID = @feedtoken
		select @now = GETDATE()
	if @accesspartnerid is null
		set @accesspartnerid = 0

    SELECT	 --distinct
			TOP(@numbertoreturn) c.ContentID, c.ContentTypeID, c.PartnerID, c.ContentSourceID, c.Name, c.Description, c.Category, c.Keyword, c.EffectiveDate, c.ExpirationDate, c.CreatedDate, c.CreatedUserID, c.UpdatedDate, c.UpdatedUserID, c.Active,
			c.FeedID, partners.PartnerID, partners.ParentPartnerID, partners.Name, partners.ContactID, partners.Website, partners.StatusID, partners.CreatedDate, partners.CreatedDate, partners.CreatedUserID, Partners.UpdatedDate, partners.UpdateUserID, partners.TrackingGroup, partners.ShortName, @landingurl LandingURL, partners.isContentPrivate, partners.ZoneID, partners.FeedGUID, partners.isFeedGUIDActive, c.IsFileArchived,
			c.ContentID, c.ContentID, 0 NumberOfReviews, a.Duration, 0 as ClipPopularity, 0 as IsAdvertisement, null as CandidateOfInterest, 0 as NumberOfPlays, 1 as SentToFreeWheel, null as FileExtension, null as IngestionStatusMessage, a.IsDeleted,
			c.PartnerID, null as ContentItemId, null as Phone, pt.PartnerTypeID as OrganizationType, Partners.ShortName, null as CandidateOfficePlaceholder, case when pt.PartnerTypeID IN (2,3) then 1 else 0 end as AllowUploadingVideo, Partners.LogoUrl, case when pt.PartnerTypeID IN (2,3) then 1 else 0 end as IsMediaSource, partners.isContentPrivate,
			-- max(ac.AllowContentID),
			0 as AllowContentID,
			c.partnerid as  ContentProviderPartnerID,
			pd.Partnerid as DistributorPartnerID, c.CreatedDate
			, aa.FilePath + '/' + aa.Filename as TranscriptPath
			-- ac.ContentProviderPartnerID, ac.DistributorPartnerID, max(ac.CreatedDate) CreatedDate
	-- select top 10 *
	FROM	Content c
    join	Content_Asset ca
    on		c.ContentID = ca.contentid
    and		1 = ca.TargetPlatformID
    and		(1 = ca.AssetTypeID or 17 = ca.assettypeid)
    join	Asset a
    on		ca.assetid = a.AssetID
    left join Asset aa
    on		ca.AssetID = aa.AssetID
    and		ca.AssetTypeID = 17
    JOIN	Partner AS Partners (nolock)
    ON		c.PartnerID = Partners.PartnerID
    left join (select max(partnertypeid) PartnerTypeID, partnerid from Partner_PartnerType (nolock) group by partnerid) ppt
    on		Partners.partnerid = ppt.PartnerID
    left join PartnerType pt
    on		ppt.PartnerTypeID = pt.PartnerTypeID
	left	join mTrackingGroup pd
	on		@itrackinggroup = pd.TrackingGroup
	LEFT JOIN (select * from AllowContent (nolock) where ContentProviderPartnerID = @providerid) ac
	ON		pd.PartnerID = ac.ContentProviderPartnerID
	left	join DMAExclude dmaEx (nolock)
	on		c.ContentID = dmaEx.contentid
	left	join Partner_DMA pdma (nolock)
	on		dmaex.DMACode = pdma.DMACode
	and		pd.PartnerID = pdma.PartnerID
	WHERE  (
        (@datefrom IS NULL OR case when @updates = 1 then c.UpdatedDate else c.CreatedDate end >= @datefrom)
        AND (@dateto IS NULL OR case when @updates = 1 then c.updateddate else c.CreatedDate end <= @dateto)
		)
		--(
		--	case when @updates = 1 then c.UpdatedDate else c.createddate end >= isnull(@datefrom, c.createddate) -- (@datefrom IS NULL OR c.CreatedDate >= @datefrom)
        --AND case when @updates = 1 then c.UpdatedDate else c.CreatedDate end <= isnull(@dateto, c.CreatedDate) -- (@dateto IS NULL OR c.CreatedDate <= @dateto)
		--)
		AND (@providerid = 0 OR c.PartnerID = @providerid) AND
		c.active = 1 and ca.active = 1 and a.active = 1 and ca.TargetPlatformID = 1 and
		 ( ac.allowcontentid is null or (pd.PartnerID = c.PartnerID or (Partners.IsContentPrivate = 0 OR ac.DistributorPartnerID = pD.PartnerID)))
			and		((dmaEx.StartDTM IS NULL AND dmaEx.EndDTM IS NULL)
					OR (dmaEx.EndDTM IS NULL AND dmaEx.StartDTM < @now)
					OR (dmaEx.StartDTM IS NULL AND dmaEx.EndDTM > @now)
					OR (@now between dmaEx.StartDTM AND dmaEx.EndDTM)) -- )
    ORDER BY c.CreatedDate DESC, c.contentid desc

END
GO
