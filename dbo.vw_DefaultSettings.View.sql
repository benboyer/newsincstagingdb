USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_DefaultSettings]
AS
SELECT     ISNULL(CONVERT(varchar(20), par.PartnerID), N'All') AS PartnerID, sys.Entity, sys.Setting, ISNULL(par.Value, sys.Value) AS Value
FROM         (SELECT     dst.Name AS Entity, s.Name AS Setting, ds.Value
                       FROM          dbo.Setting AS s INNER JOIN
                                              dbo.DefaultSetting AS ds ON s.SettingID = ds.SettingID INNER JOIN
                                              dbo.DefaultSettingType AS dst ON ds.DefaultsettingTypeID = dst.DefaultSettingTypeID
                       WHERE      (dst.Active = 1)) AS sys LEFT OUTER JOIN
                          (SELECT     ds.PartnerID, dst.Name AS Entity, s.Name AS Setting, ds.Value
                            FROM          dbo.Setting AS s INNER JOIN
                                                   dbo.PartnerDefaultSetting AS ds ON s.SettingID = ds.SettingID INNER JOIN
                                                   dbo.DefaultSettingType AS dst ON ds.DefaultsettingTypeID = dst.DefaultSettingTypeID
                            WHERE      (dst.Active = 1)) AS par ON sys.Entity = par.Entity AND sys.Setting = par.Setting
GO
