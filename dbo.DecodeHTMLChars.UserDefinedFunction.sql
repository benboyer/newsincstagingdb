USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[DecodeHTMLChars]
(
	@String varchar(1500)
)
RETURNS varchar(1500)
AS

BEGIN
	-- declare @string varchar(1500) = 'Cook County Jail is now eight inmates away from full capacity and, we haven&#39;t even hit the summer crime wave.'
	DECLARE @ResultVar varchar(1500)
	set @ResultVar = REPLACE(@string, '&#39;', CHAR(39))
	set @ResultVar = REPLACE(@ResultVar, '&amp;', '&')
	set @ResultVar = REPLACE(@ResultVar, '&quot;', char(34))
	set @ResultVar = REPLACE(@ResultVar, '&nbsp;', ' ')
	set @ResultVar = REPLACE(@ResultVar, '&lt;', '<')
	set @ResultVar = REPLACE(@ResultVar, '&gt;', '>')
	set @ResultVar = REPLACE(@ResultVar, '&mdash;', '—')
-- select @ResultVar
	RETURN @ResultVar

END
GO
