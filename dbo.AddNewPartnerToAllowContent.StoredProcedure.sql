USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[AddNewPartnerToAllowContent]
      @newPartnerId int
AS
BEGIN
	set fmtonly off
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'AddNewPartnerToAllowContent'

	insert into AllowContent (ContentProviderPartnerID, DistributorPartnerID)
	-- declare @newPartnerID int = 1077
	select      p.PartnerID, @newPartnerId
	FROM        Partner p
	left join 	(select * from dbo.vw_partnerDefaultSettings -- select * from vw_sysDefaultSettings
				where Entity = 'AllowPrivateContentExceptions'
				and Setting = 'String'
				and Value = 'False') notme
	on			p.PartnerID = notme.PartnerID
	WHERE       p.isContentPrivate = 1
	and			notme.PartnerID is null
	and         not exists
				(select 1
				from        AllowContent ac
				where       ac.DistributorPartnerID = @newPartnerId
				and         ac.ContentProviderPartnerID = p.partnerid)
	SET NOCOUNT OFF;

END
GO
