USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[defaultsettingtype_defaultsetting](
	[DefaultSettingType_DefaultSettingID] [int] IDENTITY(1,1) NOT NULL,
	[DefaultSettingTypeID] [int] NULL,
	[SettingID] [int] NULL,
	[Active] [bit] NULL
) ON [PRIMARY]
GO
