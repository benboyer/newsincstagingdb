USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Playlist_Keyword](
	[PlaylistID] [int] NOT NULL,
	[KeywordID] [int] NOT NULL,
 CONSTRAINT [PK_Playlist_Keyword] PRIMARY KEY CLUSTERED 
(
	[PlaylistID] ASC,
	[KeywordID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Playlist_Keyword]  WITH CHECK ADD  CONSTRAINT [FK_Playlist_Keyword_Keyword] FOREIGN KEY([KeywordID])
REFERENCES [dbo].[Keyword] ([KeywordID])
GO
ALTER TABLE [dbo].[Playlist_Keyword] CHECK CONSTRAINT [FK_Playlist_Keyword_Keyword]
GO
ALTER TABLE [dbo].[Playlist_Keyword]  WITH CHECK ADD  CONSTRAINT [FK_Playlist_Keyword_Playlist] FOREIGN KEY([PlaylistID])
REFERENCES [dbo].[Playlist] ([PlaylistID])
GO
ALTER TABLE [dbo].[Playlist_Keyword] CHECK CONSTRAINT [FK_Playlist_Keyword_Playlist]
GO
