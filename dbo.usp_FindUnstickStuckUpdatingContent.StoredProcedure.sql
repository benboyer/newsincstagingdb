USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_FindUnstickStuckUpdatingContent]
as
BEGIN
	declare @now datetime = getdate()

-- #1
	select distinct c.ContentID 
	into	#badCID
	from	Content c (nolock)
	join	(select *
			from Content_Asset (nolock)
			where	ImportStatusID = 6
			and ContentID not in (23536382, 23526763)) ca
	on		c.ContentID = ca.ContentID
	join	(select MIN(a.AssetID) assetid, ContentID, /*c.TargetPlatformID, */COUNT(distinct tp.assettypeid) AssetCount
			from	Content_Asset c  (nolock) join Asset a  (nolock) on c.AssetID = a.AssetID
			join TargetPlatform_AssetType tp on c.TargetPlatformID = tp.TargetPlatformID and c.AssetTypeID = tp.AssetTypeID
			where	c.importstatusid = 6 and a.importstatusid = 5 and a.Active = 1
			group by ContentID/*, c.TargetPlatformID*/) camin
	on		ca.ContentID = camin.ContentID
	and		ca.AssetID = camin.assetid
	join	(select Max(a.AssetID) assetid, ContentID, /*c.TargetPlatformID, */COUNT(distinct tp.assettypeid) AssetCount
			from	Content_Asset c  (nolock) join Asset a  (nolock) on c.AssetID = a.AssetID
			join TargetPlatform_AssetType tp on c.TargetPlatformID = tp.TargetPlatformID and c.AssetTypeID = tp.AssetTypeID
			where	c.importstatusid = 7 and a.importstatusid = 5 and a.Active = 1
			group by ContentID /*, c.TargetPlatformID*/) camax
	on		ca.ContentID = camax.ContentID
	and		((camin.AssetCount = camax.AssetCount and c.Active = 1)
			or
			(c.Active = 0))
	where	EffectiveDate < GETDATE()
	and		isnull(c.updateddate, c.CreatedDate) < DATEADD(n, -30, @now)
order by c.ContentID
	
	declare @minCID bigint
	while (select COUNT(*) from #badCID) > 0
	begin
		--	declare @minCID bigint
			set @minCID = (select MIN(contentid) from #badCID)
			select @mincid
			exec usp_DeleteAssetsOlderThanUpdates @minCID
			delete from #badCID where ContentID = @minCID
	end	
	-- Drop table #badCID
-- end #1
-------------------------------------------------------------------
-- #2
/*	insert into #badCID(ContentID)
	select distinct d.ContentID
--	into	#badCID
	from	(select distinct ContentID, TargetPlatformID 
			from	Content_Asset a
			where	ImportStatusID = 6
			and		Active = 1
			and		ContentID not in (23536382, 23526763)
			and not exists 
				(select 1 
				from	Content_Asset c 
				where	c.ContentID = a.ContentID
				and		(c.ImportStatusID <> 6
						or
						c.Active = 0))) b
	join	Content d
	on		b.ContentID = d.ContentID
	join	Content_Asset ca
	on		d.ContentID = ca.ContentID
	and		b.TargetPlatformID = ca.TargetPlatformID
	where	d.ContentImportStatus = 7
	and		d.UpdatedDate < DATEADD(hh, -6, @now)
	and not exists (select 1
			from	Asset a
			where	ca.AssetID = a.AssetID
			and		(a.ImportStatusID <> 5
			or		a.Active <> 1))

	set @minCID = 0
	while (select COUNT(*) from #badCID) > 0
	begin
		--	declare @minCID bigint
			set @minCID = (select MIN(contentid) from #badCID)
			select @mincid
			exec dbo.usp_SetContentToError_all @minCID
			delete from #badCID where ContentID = @minCID
	end	

	Drop table #badCID

-- end #2
-------------------------------------------------------------
-- #3

	insert into #badCID(contentid)
	select distinct c.*
	--into	#badCID
	from	Content c
	where	c.CreatedDate < DATEADD(hh, -6, @now)
	and		c.UpdatedDate is null
	and		c.EffectiveDate < @now)
	and		c.ContentImportStatus not in (5, 9)

	set @minCID = 0
	while (select COUNT(*) from #badCID) > 0
	begin
		--	declare @minCID bigint
			set @minCID = (select MIN(contentid) from #badCID)
			select @mincid
			exec dbo.usp_SetContentToError_all @minCID
			delete from #badCID where ContentID = @minCID
	end	
*/
	Drop table #badCID
-- end #3

	--- Free up assets when update came in before earlier assets were done downloading/encoding, and the earlier assets have been abandoned and newer assets are active. 
	--- (Old assets are blocking the content from being active)
	select d.ContentID, d.TargetPlatformID, d.assettypeid, d.AssetID
	into #blockingassets
	from (select c.ContentID, c.ContentImportStatus, ca.TargetPlatformID, ca.assettypeid, MIN(ca.AssetID) AssetID
		from Content c
		join	Content_Asset ca
		on		c.ContentID = ca.ContentID
		join	Asset a
		on		ca.assetid = a.AssetID
		where c.ContentImportStatus not in (5,9,11)
		and ISNULL(c.EffectiveDate, c.CreatedDate) <= GETDATE()
		group by c.ContentID, c.ContentImportStatus, ca.TargetPlatformID, ca.assettypeid) d
	join Asset da
	on	d.AssetID = da.AssetID
	where da.ImportStatusID in (1,2)

	if (select COUNT(*)
		from	#blockingassets d
		where exists
			(select 1 
			from Content c
			join	Content_Asset ca
			on		c.ContentID = ca.ContentID
			join	Asset a
			on		ca.assetid = a.AssetID
			where	d.ContentID = c.ContentID
			and		d.TargetPlatformID = ca.TargetPlatformID
			and		d.AssetTypeID = ca.AssetTypeID
			and		d.AssetID < a.AssetID
			and		a.ImportStatusID = 5
			and		a.Active = 1))
		=
		(select COUNT(*) from #blockingassets)	
	begin

		delete b
		from	#blockingassets a
		join	Content_Asset b
		on		a.ContentID = b.ContentID
		and		a.AssetID = b.AssetID
		
	end			


	drop table #blockingassets


END
GO
