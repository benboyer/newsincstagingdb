USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[BlockedSites_listInternal_id_2]
	@includeInactive bit = 0,
	@id int = null
AS
BEGIN
	set nocount on
	set fmtonly off
	exec SprocTrackingUpdate 'BlockedSites_List'

	SELECT	BlockedSiteID, Host, Rules, Active, isnull(DateUpdatedGMT, DateCreatedGMT) LastUpdatedDate, Comment
	  FROM	BlockedSite
	  WHERE ([Active] = 1 or @includeInactive = 1)
	  and	 (@id is null or BlockedSiteID = @id)
	set nocount off
END
GO
