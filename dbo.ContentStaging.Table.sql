USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContentStaging](
	[ContentID] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ContentTypeID] [smallint] NOT NULL,
	[PartnerID] [int] NOT NULL,
	[ContentSourceID] [smallint] NOT NULL,
	[Name] [varchar](500) NOT NULL,
	[Description] [varchar](1000) NOT NULL,
	[Category] [varchar](250) NULL,
	[Keyword] [varchar](1000) NULL,
	[Duration] [decimal](10, 2) NULL,
	[EffectiveDate] [smalldatetime] NOT NULL,
	[ExpirationDate] [smalldatetime] NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[CreatedUserID] [int] NOT NULL,
	[UpdatedDate] [smalldatetime] NULL,
	[UpdatedUserID] [int] NULL,
	[Active] [bit] NOT NULL,
	[IsArchived] [bit] NULL,
	[isDeleted] [bit] NULL,
	[FeedID] [int] NULL,
 CONSTRAINT [PK_ContentStaging_1] PRIMARY KEY CLUSTERED 
(
	[ContentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ContentStaging]  WITH CHECK ADD  CONSTRAINT [FK_ContentStaging_ContentSource] FOREIGN KEY([ContentSourceID])
REFERENCES [dbo].[ContentSource] ([ContentSourceID])
GO
ALTER TABLE [dbo].[ContentStaging] CHECK CONSTRAINT [FK_ContentStaging_ContentSource]
GO
ALTER TABLE [dbo].[ContentStaging]  WITH CHECK ADD  CONSTRAINT [Partner_ContentStaging] FOREIGN KEY([PartnerID])
REFERENCES [dbo].[Partner] ([PartnerID])
GO
ALTER TABLE [dbo].[ContentStaging] CHECK CONSTRAINT [Partner_ContentStaging]
GO
ALTER TABLE [dbo].[ContentStaging] ADD  CONSTRAINT [DF__ContentStaging__Created__452A2A23]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[ContentStaging] ADD  CONSTRAINT [DF_ContentStaging_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
ALTER TABLE [dbo].[ContentStaging] ADD  CONSTRAINT [DF_ContentStaging_Active]  DEFAULT ((1)) FOR [Active]
GO
ALTER TABLE [dbo].[ContentStaging] ADD  CONSTRAINT [DF_ContentStaging_IsFileArchived]  DEFAULT ((0)) FOR [IsArchived]
GO
ALTER TABLE [dbo].[ContentStaging] ADD  CONSTRAINT [DF_ContentStaging_isDeleted]  DEFAULT ((0)) FOR [isDeleted]
GO
