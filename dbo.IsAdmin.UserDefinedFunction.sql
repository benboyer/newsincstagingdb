USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[IsAdmin]
(@UserId int)
RETURNS bit
AS
BEGIN
	DECLARE @Return bit
	SET @Return = CAST((CASE (SELECT RoleID FROM dbo.User_Role WHERE RoleID = 1 AND UserID = @UserId) WHEN 1 THEN 1 ELSE 0 END) AS bit)
RETURN @return
END
GO
