USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ps_PlaylistContentIDs]
 @widgetID int = null,            
 @playlistID int = null,        
 @trackingGroup int = 10557,            
 @contentid bigint = null,            
 @devicetype int = 1,        
 @listcount smallint = 50          
AS            
BEGIN            
 set fmtonly off            
 set nocount on            
 exec SprocTrackingUpdate 'ps_PlaylistContentIDs'            
           
 if ( @trackingGroup = 0 ) set @trackingGroup = 10557    
 if ( @listcount is null ) set @listcount = 50  
 declare @partnerID int = ( select partnerID from mTrackingGroup where TrackingGroup = @trackingGroup )
     
 /*
 if @PlaylistID = 9999999        
 begin            
  exec ps_SingleContent @contentid, @TrackingGroup, @deviceType            
  return            
 end            
 */
 
 -- declare @iwidgetid int = 3233, @iplaylistid int = 10203, @itrackinggroup int = 10557, @idevicetype int = 1, @now datetime = getdate()            
 select top (@listcount) pc.playlistid, pc.ContentID, c.UpdatedDate, c.Name, ([Order] + 1) as [ContentOrder]                
 from Playlist_Content pc (nolock)                  
 join Content c (nolock)
 on pc.ContentID = c.ContentID             
 join dbo.AllowedContentByPlaylistID(@playlistID, @partnerID) ac 
 on ac.ContentID = c.ContentID
 where pc.PlaylistID = @playlistID            
 -- and dbo.AllowedContentID(pc.ContentID, @trackinggroup, 1) = 1                 
 and dbo.AllowedLauncherContentP(@widgetID, c.PartnerID) = 1            
 ORDER BY pc.[Order], pc.ContentID            
            
 set nocount off            
END
GO
