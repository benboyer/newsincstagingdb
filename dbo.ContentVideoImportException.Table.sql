USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContentVideoImportException](
	[FeedID] [int] NOT NULL,
	[GUID] [varchar](500) NOT NULL,
	[ExceptionArea] [smallint] NOT NULL,
	[ContentID] [int] NULL,
	[DisplayErrorMessage] [nvarchar](1000) NULL,
	[Exception] [nvarchar](1000) NULL,
	[InnerException] [nvarchar](1000) NULL,
	[StackTrace] [nvarchar](2000) NULL,
 CONSTRAINT [PK_ContentVideoImportException] PRIMARY KEY CLUSTERED 
(
	[FeedID] ASC,
	[GUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
