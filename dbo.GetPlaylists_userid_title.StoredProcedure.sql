USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetPlaylists_userid_title]
	-- declare
	@userid int,
	@TitleFragment varchar(max) = '',
	@includeshared bit = 1,
	@PlaylistTypeID int = 1
as
BEGIN
	set fmtonly OFF
	set nocount ON
	exec SprocTrackingUpdate 'GetPlaylists_userid_title'
	--	8125 -- 8284 = pid -1
	--	select @userid = 8125, @titlefragment = '', @PlaylistTypeid = 1

	declare @PartnerID int
	set @PartnerID = (select top 1 partnerid from [User] where UserID = @userid)
-- select top 100 * from playlist
	-- declare @partnerid int = 1429, @titlefragment varchar(200) = '', @includeshared bit = 1
	-- select * from partner where PartnerID = @PartnerID
	Select pl.PartnerID, pl.PlaylistID, pl.Name PlaylistName, p.name PartnerName, COUNT(pc.ContentID) VideoCount
	from	Playlist pl
	join	playlist_content pc
	on		pl.PlaylistID = pc.PlaylistID
	join	Content c
	on		pc.ContentID = c.ContentID
	join	Partner  p
	on		pl.PartnerID = p.PartnerID
	join	PlaylistsLEGACY pll
	on		pl.PlaylistID = pll.PlaylistID
	where	pl.PlaylistTypeID = @playlistTypeID
	and		pl.Active = 1
	and		c.Active = 1
	and		c.isDeleted = 0
	and		pl.Name like '%' + @TitleFragment + '%'
	and		(	(@PlaylistTypeID <> 3
				and		(pl.PartnerID = @PartnerID or (@includeShared = 1 and pll.SharingOption = 2))
				and		pl.CreatedUserID = case when pl.PartnerID = -1 then @userid else pl.CreatedUserID end )
--				and	(pl.CreatedUserID = @userID or (@includeShared = 1 and pll.SharingOption = 2)) )
			or
				(@PlaylistTypeID = 3
				and
				exists (select *
				from	(select * from Partner_LandingURL where PartnerID = @PartnerID) plu
				join	LandingURL lu
				on		plu.LandingURLID = lu.LandingURLID
				where lu.LandingURLTypeID = 2)))
		group by pl.PartnerID, pl.PlaylistID, pl.Name, p.name
	order by pl.name

/*
	select pl.PartnerID, pl.PlaylistID, pl.Name PlaylistName, p.name PartnerName, COUNT(pc.ContentID) VideoCount
	from playlist pl
	join	PlaylistsLEGACY pll
	on		pl.PlaylistID = pll.PlaylistID
	join	partner p
	on		pl.PartnerID = p.PartnerID
	left join playlist_content pc
	on	pl.PlaylistID = pc.PlaylistID
	where	pc.PlaylistID is not null
	and		pl.Active = 1
	and		(pl.PartnerID = @PartnerID or (@includeShared = 1 and pll.SharingOption = 2))
	and		pl.Name like '%' + @TitleFragment + '%'
	group by pl.PartnerID, pl.PlaylistID, pl.Name, p.name
	order by pl.name
*/
	set nocount OFF

END
GO
