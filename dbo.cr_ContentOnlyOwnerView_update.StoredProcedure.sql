USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[cr_ContentOnlyOwnerView_update]
	@contentid bigint,
	@Value bit = null
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'cr_ContentOnlyOwnerView_update'
	-- declare @contentid  bigint = 23802256, 	@Value bit
	-- select @contentid, @Value
	IF EXISTS (select 1 from Content where ContentID = @contentid)
	BEGIN
		if @Value is not null
		begin
			update	Content
			set		OnlyOwnerView = @Value, UpdatedDate = GETDATE(), UpdatedUserID = 7534
			WHERE	ContentID = @contentid
		end
		select	ContentID, OnlyOwnerView
		from	Content (nolock)
		where	ContentID = @contentid
	END
	set nocount off
END
GO
