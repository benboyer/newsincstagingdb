USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[WidgetExcludedOrgs_ADD_sp]
	@ContentItemID int = null,
	@WidgetId int,
	@OrganizationId int
AS
BEGIN
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'WidgetExcludedOrgs_ADD_sp'

	Insert into Launcher_ContentExclusion (LauncherID, PartnerID)
	Values (@WidgetId, @OrganizationId)


END
GO
