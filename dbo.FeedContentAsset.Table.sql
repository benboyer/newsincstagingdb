USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FeedContentAsset](
	[FeedContentAssetID] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[FeedContentID] [bigint] NOT NULL,
	[MimeTypeID] [int] NULL,
	[MimeType] [varchar](20) NULL,
	[Height] [int] NULL,
	[FilePath] [varchar](1000) NULL,
	[Width] [int] NULL,
	[Bitrate] [int] NULL,
	[Duration] [decimal](20, 2) NULL,
	[Final] [bit] NULL,
 CONSTRAINT [PK_FeedContentAssets] PRIMARY KEY CLUSTERED 
(
	[FeedContentAssetID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [NDX_FCA_fcid] ON [dbo].[FeedContentAsset] 
(
	[FeedContentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FeedContentAsset]  WITH CHECK ADD  CONSTRAINT [FK_FeedContentAsset_FeedContent] FOREIGN KEY([FeedContentID])
REFERENCES [dbo].[FeedContent] ([FeedContentID])
GO
ALTER TABLE [dbo].[FeedContentAsset] CHECK CONSTRAINT [FK_FeedContentAsset_FeedContent]
GO
ALTER TABLE [dbo].[FeedContentAsset]  WITH CHECK ADD  CONSTRAINT [FK_FeedContentAsset_MimeType] FOREIGN KEY([MimeTypeID])
REFERENCES [dbo].[MimeType] ([MimeTypeID])
GO
ALTER TABLE [dbo].[FeedContentAsset] CHECK CONSTRAINT [FK_FeedContentAsset_MimeType]
GO
