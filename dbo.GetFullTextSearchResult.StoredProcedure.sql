USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetFullTextSearchResult]
(	-- declare
	@SearchString NVARCHAR(4000),
	@WidgetId INT = NULL,
	@UserId INT = 0,
	@OrgId INT =0,
	@Count INT = 100,
	@OrderClause NVARCHAR(MAX) = null,
	@StatusType INT = 0,
	@IsAdmin BIT = 0,
	@WhereCondition NVARCHAR(MAX) = ''
	, @StartDTM datetime = null
	, @EndDTM datetime = null,
	 @MinVideoID BIGINT = 0, -- max value of the current external record set: used for pagination
	 @MaxVideoID BIGINT = 0 -- min value of the current external record set: used for pagination
)

AS
BEGIN
	---------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------
-- select @SearchString, @WidgetId, @UserId, @OrgId, @Count, @OrderClause, @StatusType, @IsAdmin, @WhereCondition, @StartDTM, @EndDTM, @MinVideoID, @MaxVideoID
--exec [GetSearchResults] @SearchString, @WidgetId, @UserId, @OrgId, @Count, @OrderClause, @StatusType, @IsAdmin, @WhereCondition, @StartDTM, @EndDTM, @MinVideoID, @MaxVideoID
--return
---------------------------------------------------------------------------------------------------------------------------------------


	DECLARE @iStartDTM nvarchar(12),
			@iEndDTM nvarchar(12),
			@queryString NVARCHAR(MAX),
			@WhereClause NVARCHAR(MAX),
			@OrganizationId INT,
			@range varchar(max),
			@userTG varchar(10)

	set @iStartDTM = convert(varchar(12), @StartDTM, 111)
	set @iEndDTM = convert(varchar(12), @EndDTM, 111)
	if @iStartDTM is not null
		set @range = ' AND vs.Publishdate between ' + '''' + @iStartDTM + '''' + ' and ' + '''' + @iEndDTM + ''''
	--select @range ranger

	SELECT @OrganizationId = OrganizationId FROM NDNUsers WHERE UserID = @UserId
	set @userTG = (select convert(varchar(10), trackinggroup) from Partner where PartnerID = @OrganizationId)

	declare @count_external int set @count_external = isnull(@count, 100)
	declare @OrderClause_external varchar(max) set @OrderClause_external = @OrderClause

	set @count = @count * 4
	set @OrderClause = 'PublishDate DESC'


	SET @WidgetId =  ISNULL(@WidgetId,0)
	SET @UserId =  ISNULL(@UserId,0)
	SET @OrgId =  ISNULL(@OrgId,0)
	SET @Count =  ISNULL(@Count,100)
	SET @OrderClause =  ISNULL(@OrderClause,'')
	SET	@WhereClause =  ISNULL(@WhereClause,'')


	SET @queryString=
		' SELECT  TOP ' + CONVERT(NVARCHAR(20),@Count) + ' vs.[VideoID]
		  ,vs.[UserID]
		  ,vs.[UserType]
		  ,vs.[ClipURL]
		  ,vs.[Title]
		  ,vs.[Description]
		  ,ISNULL(vs.[EventDate], vs.[PublishDate]) AS EventDate
		  ,vs.[NumberofReviews]
		  ,vs.[Height]
		  ,vs.[Width]
		  ,vs.[Duration]
		  ,vs.[ClipPopularity]
		  ,vs.[IsAdvertisement]
		  ,vs.[CreatedOn]
		  ,vs.[ThumbnailURL]
		  ,vs.[PublishDate]
		  ,ISNULL(vs.[Keywords], '''') AS Keywords
		  ,vs.[NumberOfPlays]
		  ,vs.[OriginalName]
		  ,vs.[ExpiryDate]
		  ,vs.[IsEnabled]
		  ,vs.[IsDeleted]
		  ,isnull(us.FirstName,'''') + '' '' + isnull(us.LastName,'''') AS UserName
		  ,us.OrganizationID
		  ,KT.[Rank]
		  ,isnull(org.Name,'''') as OrganizationName
		  ,isnull(org.IsMediaSource, 0) as IsMediaSource
		  ,us.isadmin
		  -- select top 100 * from assettype at join mimetype mt on at.mimetypeid = mt.mimetypeid
			FROM Videos (nolock) vs join content (nolock) c on vs.videoid = c.contentid
			join content_asset (nolock) ca on c.contentid = ca.contentid and 1 = ca.targetplatformid
			INNER JOIN FREETEXTTABLE(Videos, ([Description], Title, Keywords), ''' + @SearchString + ''') AS
			KT ON vs.VideoID = KT.[KEY]
			INNER JOIN NDNUsers (nolock) us ON vs.UserId = us.UserId
			INNER JOIN Organizations (nolock) AS org ON org.OrganizationID = us.OrganizationID
			WHERE 1 = 1 '


	IF(@StatusType IS NOT NULL AND @StatusType != -1  )
	BEGIN
		SELECT @WhereClause = ' AND  '
		SELECT @WhereClause = @WhereClause + (CASE CONVERT(NVARCHAR(1),@StatusType)
									WHEN '0' THEN '  vs.IsEnabled = 1 AND vs.IsDeleted = 0 AND (vs.ExpiryDate IS NULL OR vs.ExpiryDate > GETDATE()) '
									WHEN '1' THEN '  ( (vs.IsEnabled = 0 OR vs.ExpiryDate < getdate()) AND vs.IsDeleted = 0) '
									WHEN '2' THEN '  vs.IsDeleted = 1 '
									END)
	END

--	IF (@IsAdmin = 0)
--		SELECT @WhereClause = @WhereClause + ' AND vs.UserId = ' + CONVERT(NVARCHAR(20),@UserID) ;

	IF (@WhereCondition IS NOT NULL AND @WhereCondition != '')
		SELECT @WhereClause = @WhereClause + ' AND '+ @WhereCondition ;

	IF(@OrgId IS NULL OR @OrgId >0)
		SELECT @WhereClause = @Whereclause + ' AND org.OrganizationId = ' + CONVERT(NVARCHAR(20),@OrgId)

	IF (@MinVideoID > 0)
		SELECT @WhereClause = @WhereClause + ' AND vs.VideoID < ' + CONVERT(NVARCHAR(20),@MinVideoID)  ;

	IF (@MaxVideoID > 0)
		SELECT @WhereClause = @WhereClause + ' AND vs.VideoID >= ' + CONVERT(NVARCHAR(20),@MaxVideoID)  ;


	SET @queryString = @queryString + ISNULL(@WhereClause,'')

	if (@StartDTM) is not null and (@EndDTM) is not null
		SELECT @queryString = @queryString + @range
	-- select @range
	-- select @WhereClause
	-- select @queryString

-- select @queryString
	-- IF(@WhereClause IS NOT NULL AND @OrderClause IS NOT NULL AND @OrderClause != '')
		SET @queryString = @queryString + ' Order By ' + @OrderClause

	create table #ftsr
			(VideoID int, UserID int, UserType int, ClipURL varchar(max), title varchar(max), Description varchar(max),
			EventDate datetime, NumberOfReviews int, height decimal(10,4), width decimal(10,4), duration decimal(10,4),
			ClipPopularity float, IsAdvertisement bit, createdon datetime, ThumbnailURL varchar(max), PublishDate datetime,
			Keywords varchar(max), NumberOfPlays int, originalname varchar(max), Expirydate datetime, isEnabled bit,
			IsDeleted bit, UserName varchar(max), organizationid int, [rank] int, OrganizationName varchar(max), isMediaSource bit, isAdmin bit)
	insert into #ftsr(VideoID, UserID, UserType, ClipURL, title, Description,
			EventDate, NumberOfReviews, height, width, duration,
			ClipPopularity, IsAdvertisement, createdon, ThumbnailURL, PublishDate,
			Keywords, NumberOfPlays, originalname, Expirydate, isEnabled,
			IsDeleted, UserName,  organizationid, [rank], OrganizationName, isMediaSource, isAdmin)

	EXEC ( @queryString );
	-- select @count_external
	set @queryString = ' select distinct top ' + convert(varchar(20), @count_external) + '
			a.VideoID, a.UserID, a.UserType, a.ClipURL, a.title, left(a.Description, 500) Description,
			a.EventDate, a.NumberOfReviews, a.Height, a.Width, a.Duration,
			isnull(a.ClipPopularity, 0) ClipPopularity, a.IsAdvertisement, a.createdon, a.ThumbnailURL, a.PublishDate,
			left(a.Keywords, 100) Keywords, a.NumberOfPlays, a.originalname, a.Expirydate, a.isEnabled,
			a.IsDeleted, xyz.OnlyOwnerView, a.UserName,  a.organizationid, a.[rank], a.OrganizationName, a.isMediaSource, a.isAdmin
			from #ftsr (nolock) a join content (nolock) xyz on a.VideoID = xyz.ContentID
			where	(dbo.AllowedContentID(a.videoid, ' + @userTG + ', 1) = 1 or a.organizationid  = ' + '''' + convert(varchar(10), @organizationid) + '''' + ' or a.isAdmin = 1 )
					' +  case
							when isnull(@widgetid, 0) = 0 then ''
							else ' and (dbo.AllowedLauncherContent(' + convert(varchar(20), @widgetid) + ', ' + @userTG + ') = 1 or ' + convert(varchar(20), @widgetid) + ' = null) '
							end
					+ '
					order by a.PublishDate DESC' --  + isnull(@OrderClause_external, 'PublishDate DESC')
	-- select @queryString
	EXEC ( @queryString );

	drop table #ftsr


END
GO
