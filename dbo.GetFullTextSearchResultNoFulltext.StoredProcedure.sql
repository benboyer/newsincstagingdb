USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetFullTextSearchResultNoFulltext]
(
	@SearchString NVARCHAR(1000),
	@WidgetId INT = NULL,
	@UserId INT = 0,
	@OrgId INT =0,
	@Count INT = 100,
	@OrderClause NVARCHAR(MAX) = '',
	@StatusType INT = 0,
	@IsAdmin BIT = 0,
	@WhereCondition NVARCHAR(MAX) = ''
)

AS
BEGIN

	set nocount on
	exec SprocTrackingUpdate 'GetFullTextSearchResultNoFulltext'

	DECLARE @queryString NVARCHAR(MAX)
	DECLARE @WhereClause NVARCHAR(MAX)

	DECLARE @OrganizationId INT
	SELECT @OrganizationId = OrganizationId FROM NDNUsers WHERE UserID = @UserId



	SET @WidgetId =  ISNULL(@WidgetId,0)
	SET @UserId =  ISNULL(@UserId,0)
	SET @OrgId =  ISNULL(@OrgId,0)
	SET @Count =  ISNULL(@Count,100)
	SET @OrderClause =  ISNULL(@OrderClause,'')
	SET	@WhereClause =  ISNULL(@WhereClause,'')

	/*
	create table #Content_Videos (Ranking int not null identity(1,1) primary key, VideoID bigint, HitCount bigint)
	insert into #Content_Videos(VideoID, HitCount)
	-- declare @SearchString varchar(20) set @SearchString = 'sheen'
	select VideoID, HitCount + isnull(NumberOfPlays, 0) as Ranking --, CreatedOn
	from	(select VideoID , NumberOfPlays, CreatedOn, COUNT(*) as HitCount from Videos where [Description] like '%' + @SearchString + '%' or Keywords like '%' + @SearchString + '%'  or title like '%' + @SearchString + '%'  group by VideoID, CreatedOn, NumberOfPlays) a
	order by CreatedOn, Ranking DESC
	*/

	SET @queryString= 'create table #Content_Videos (Ranking int not null identity(1,1) primary key, VideoID bigint, HitCount bigint)
				insert into #Content_Videos(VideoID, HitCount)
				select VideoID, HitCount + isnull(NumberOfPlays, 0) as Ranking
					from	(select VideoID , NumberOfPlays, CreatedOn, COUNT(*) as HitCount
							from Videos where [Description] like ' + '''' + '%' + @SearchString + '%' + '''' + ' or Keywords like ' + '''' + '%' + @SearchString + '%' + '''' + '  or title like ' + '''' + '%' + @SearchString + '%' + '''' + '  group by VideoID, CreatedOn, NumberOfPlays) a
					order by CreatedOn, Ranking DESC' +
	' SELECT  TOP ' + CONVERT(NVARCHAR(20),@Count) + ' vs.[VideoID]
      ,vs.[UserID]
      ,vs.[UserType]
      ,vs.[ClipURL]
      ,vs.[Title]
      ,vs.[Description]
      ,ISNULL(vs.[EventDate], vs.[PublishDate]) AS EventDate
      ,vs.[NumberofReviews]
      ,vs.[Height]
      ,vs.[Width]
      ,vs.[Duration]
      ,vs.[ClipPopularity]
      ,vs.[IsAdvertisement]
      ,vs.[CreatedOn]
      ,vs.[ThumbnailURL]
      ,vs.[PublishDate]
      ,ISNULL(vs.[Keywords], '''') AS Keywords
      ,vs.[NumberOfPlays]
	  ,vs.[OriginalName]
	  ,vs.[ExpiryDate]
	  ,vs.[IsEnabled]
	  ,vs.[IsDeleted]
	  ,isnull(us.FirstName,'''') + '' '' + isnull(us.LastName,'''') AS UserName
	  ,us.OrganizationID
	  ,0 as Rank -- KT.[Rank]
	  ,isnull(org.Name,'''') as OrganizationName
	  ,isnull(org.IsMediaSource, 0) as IsMediaSource
		FROM Videos vs
		INNER JOIN (select VideoID as [KEY], Ranking as [Rank] from #Content_Videos) as --  (select VideoID as [KEY] from Videos where [Description] like ' + '''' + '%' + @SearchString + '%' + '''' + ' or Keywords like ' + '''' + '%' + @SearchString + '%' + '''' + ' or title like ' + '''' + '%' + @SearchString + '%' + '''' + ') AS  -- FREETEXTTABLE(Videos, ([Description], Title, Keywords), ''' + @SearchString + ''') AS
		KT ON vs.VideoID = KT.[KEY]
		INNER JOIN NDNUsers us ON vs.UserId = us.UserId
		INNER JOIN Organizations AS org ON org.OrganizationID = us.OrganizationID
		WHERE  org.OrganizationId not in (select OrganizationId from WidgetExcludedOrgs where widgetId = isnull(' + CONVERT(NVARCHAR(20),@WidgetId) + ',0))
		AND  (((CASE
        WHEN ((SELECT IsAdmin FROM NDNUsers WHERE UserID = ' + CONVERT(NVARCHAR(20),@UserId) + ' ) = 1 )THEN 1
        WHEN [vs].[UserID] = ' + CONVERT(NVARCHAR(20),@UserId) + ' THEN 1
	    WHEN (((SELECT UserTypeID FROM NDNUsers WHERE UserID = ' + CONVERT(NVARCHAR(20),@UserId) + ') = 1) and org.[IsContentPrivate] = 1) THEN CONVERT(Int,
            (CASE
                WHEN EXISTS(
                    SELECT NULL AS [EMPTY]
                    FROM [dbo].[AllowVideoView] AS [AllowVideoView], [dbo].[NDNUsers] AS [NDNUsers], [dbo].[Organizations] AS [Organizations]
                    WHERE ([NDNUsers].[UserID] = [vs].[UserID]) AND (([AllowVideoView].[ContentProviderOrgID]) = [NDNUsers].[OrganizationID])
					AND (([Organizations].[OrganizationId]) = [NDNUsers].[OrganizationID]) AND ([NDNUsers].[UserID] = [vs].[UserID])
						AND ([AllowVideoView].[DistributorOrgID] = 0)
                    ) THEN 1
                ELSE 0
             END))
        WHEN org.[IsContentPrivate] = 1 THEN CONVERT(Int,
            (CASE
                WHEN EXISTS(
                    SELECT NULL AS [EMPTY]
                    FROM [dbo].[AllowVideoView] AS [AllowVideoView], [dbo].[NDNUsers] AS [NDNUsers], [dbo].[Organizations] AS [Organizations]
                    WHERE ([NDNUsers].[UserID] = [vs].[UserID]) AND (([AllowVideoView].[ContentProviderOrgID]) = [NDNUsers].[OrganizationID])
					AND (([Organizations].[OrganizationId]) = [NDNUsers].[OrganizationID])
						AND (([AllowVideoView].[DistributorOrgID]) = ' + +CONVERT(VARCHAR(20),ISNULL(@OrganizationId,0))+')
                    ) THEN 1
                ELSE 0
             END))
        ELSE 1
     END)) = 1) '



	IF(@StatusType IS NOT NULL AND @StatusType != -1  )
	BEGIN
		SELECT @WhereClause = ' AND  '
		SELECT @WhereClause = @WhereClause + (CASE CONVERT(NVARCHAR(1),@StatusType)
									WHEN '0' THEN '  IsEnabled = 1 AND IsDeleted = 0 AND (ExpiryDate IS NULL OR ExpiryDate > GETDATE()) '
									WHEN '1' THEN '  ( (IsEnabled = 0 OR ExpiryDate < getdate()) AND IsDeleted = 0) '
									WHEN '2' THEN '  IsDeleted = 1 '
									END)
	END

	IF (@IsAdmin = 0)
		SELECT @WhereClause = @WhereClause + ' AND vs.UserId = ' + CONVERT(NVARCHAR(20),@UserID) ;

	IF (@WhereCondition IS NOT NULL AND @WhereCondition != '')
		SELECT @WhereClause = @WhereClause + ' AND '+ @WhereCondition ;

	IF(@OrgId IS NULL OR @OrgId >0)
	 SELECT @WhereClause = ' AND org.OrganizationId = ' + CONVERT(NVARCHAR(20),@OrgId)

	SET @queryString = @queryString + ISNULL(@WhereClause,'')

	IF(@WhereClause IS NOT NULL AND @OrderClause IS NOT NULL AND @OrderClause != '')
		SET @queryString = @queryString + ' Order By ' + @OrderClause


	EXEC ( @queryString );
	-- drop table #Content_Videos

END
GO
