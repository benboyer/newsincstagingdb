USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[dmev_Property_Partner] 
	-- declare	
	@ZoneID int = null,
	@PartnerID int = null,
	@Name varchar(60) = null
as

select * 
from	dme_Property_Partner (nolock) pp
where	((@Zoneid is not null and pp.ZoneID = @ZoneID)
		or		(@PartnerID is not null and pp.PartnerID = @PartnerID)
		or		(@name is not null and pp.Name like '%' + @name + '%'))
or	(@ZoneID is null and @PartnerID is null and @Name is null)
order by PartnerID desc
GO
