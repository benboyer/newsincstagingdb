USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetSiteSectionSection]
	@sectionid int = null
as
begin
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'GetSiteSectionSection'

	select SectionSectionID, Value, Description
	from SectionSection
	where SectionSectionID = ISNULL(@Sectionid, SectionSectionID)
	order by case when Description = 'Home Page' then 1 else 2 end, Description

	set nocount off
end
GO
