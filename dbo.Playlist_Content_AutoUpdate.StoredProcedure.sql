USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Playlist_Content_AutoUpdate]
as
BEGIN
	set FMTONLY OFF
	set NOCOUNT ON
	exec SprocTrackingUpdate 'Playlist_Content_AutoUpdate'
	
	declare @pid int
	select partnerid into #autopop from Playlist_Content_AutoPopulate where Active = 1

	while (select count(*)from #autopop) > 0
	BEGIN
		set @pid = (select min(partnerid) from #autopop)
		exec usp_Playlist_Content_DynMapManual @pid
		delete from #autopop where partnerid = @pid
	END

	set NOCOUNT OFF
END
GO
