USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WidgetsLEGACY](
	[LauncherID] [int] NOT NULL,
	[ContentItemID] [int] NULL,
	[DCId] [int] NULL,
	[VanityName] [varchar](32) NULL,
	[WidgetIdentificationName] [nvarchar](100) NULL,
	[IsDefault] [bit] NULL,
	[DefaultTab] [int] NULL,
	[GenerateNow] [bit] NULL,
	[TemplateZip] [varbinary](max) NULL,
	[Height] [int] NULL,
	[Width] [int] NULL,
 CONSTRAINT [PK_WidgetsExt] PRIMARY KEY CLUSTERED 
(
	[LauncherID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[WidgetsLEGACY]  WITH CHECK ADD  CONSTRAINT [FK_WidgetsLEGACY_Launcher] FOREIGN KEY([LauncherID])
REFERENCES [dbo].[Launcher] ([LauncherID])
GO
ALTER TABLE [dbo].[WidgetsLEGACY] CHECK CONSTRAINT [FK_WidgetsLEGACY_Launcher]
GO
