USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[mon_Get24HourPartnerContent]
	-- declare 
	@partnerid int
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'mon_Get24HourPartnerContent'
	
	declare @now datetime = getdate()
	declare @then datetime = dateadd(hh, -24, @now)
	--select @now, @then
		-- declare @partnerid int = 446--127 --427
	select * 
	into #mycontent
	from	Content (nolock) 
	where	PartnerID = @partnerid 
	and		CreatedDate between @then and @now

	select c.contentid, c.CreatedDate, c.UpdatedDate ActiveDate, c.EffectiveDate PublishDate, c.name ContentName, replace(replace(replace(replace(at.Name, 'Trans', ' Trans'), 'Feed', ' Feed'), 'PC', ' PC'), 'Phone', ' Phone') Asset, 
			case when c.active = 1 then 'Yes' else 'No' end as ContentActive, 
			case when a.active = 1 then 'Yes' else 'No' end as AssetActive,
			case when a.EncodingID is null then 'No' else 'Yes' end as Transcoded,
			fca.width as SourceWidth, -- case when a.EncodingID is not null then fca.width else null end as SourceWidth,
			fca.bitrate as SourceBitRate, --case when a.EncodingID is not null then fca.Bitrate else null end as SourceBitrate,
			fca.MimeType as SourceMimeType -- case when a.EncodingID is not null then fca.MimeType else null end as SourceMimeType
	from	(select * 
			from	#myContent (nolock) ) c
	join	Content_Asset ca (nolock)
	on		c.ContentID = ca.ContentID
	join	AssetType at (nolock)
	on		ca.AssetTypeID = at.AssetTypeID
	join	Asset a (nolock)
	on		ca.AssetID = a.AssetID
	join	FeedContent fc (nolock)
	on		c.ContentID = fc.ContentID
	join	FeedContentAsset fca (nolock)
	on		fc.FeedContentID = fca.FeedContentID
	and		a.FeedContentAssetID = fca.FeedContentAssetID
	order by c.CreatedDate desc, c.ContentID desc, at.AssetTypeID

	drop table #mycontent
	set nocount off
END
GO
