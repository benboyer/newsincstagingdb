USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Asset](
	[AssetID] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[AssetTypeID] [int] NULL,
	[MimeTypeID] [int] NULL,
	[Filename] [varchar](200) NULL,
	[FileSize] [bigint] NULL,
	[FilePath] [varchar](200) NULL,
	[ImportStatusID] [smallint] NULL,
	[Height] [int] NULL,
	[Width] [int] NULL,
	[Bitrate] [decimal](10, 2) NULL,
	[Duration] [decimal](20, 2) NULL,
	[Active] [bit] NOT NULL,
	[isArchived] [bit] NULL,
	[isDeleted] [bit] NULL,
	[FeedContentAssetID] [bigint] NULL,
	[AltFeedContentAssetID] [bigint] NULL,
	[EncodingID] [varchar](40) NULL,
	[DownloadTries] [int] NOT NULL,
	[DownloadMessage] [varchar](300) NULL,
	[StagingPath] [varchar](200) NULL,
	[RetryTimes] [int] NOT NULL,
	[RetryMessage] [varchar](300) NULL,
	[RetryStart] [datetime] NULL,
	[EncodingProgress] [int] NOT NULL,
	[EncodingErrorMessage] [varchar](300) NULL,
 CONSTRAINT [PK_Asset] PRIMARY KEY CLUSTERED 
(
	[AssetID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_Asset_filename] ON [dbo].[Asset] 
(
	[Filename] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Asset_mimetypeid] ON [dbo].[Asset] 
(
	[MimeTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Asset_StatusID] ON [dbo].[Asset] 
(
	[ImportStatusID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NDX_asset_MTidAIDfname] ON [dbo].[Asset] 
(
	[MimeTypeID] ASC
)
INCLUDE ( [AssetID],
[Filename]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Asset]  WITH CHECK ADD  CONSTRAINT [FK_Asset_FeedContentAsset] FOREIGN KEY([FeedContentAssetID])
REFERENCES [dbo].[FeedContentAsset] ([FeedContentAssetID])
GO
ALTER TABLE [dbo].[Asset] CHECK CONSTRAINT [FK_Asset_FeedContentAsset]
GO
ALTER TABLE [dbo].[Asset]  WITH CHECK ADD  CONSTRAINT [FK_Asset_MimeType] FOREIGN KEY([MimeTypeID])
REFERENCES [dbo].[MimeType] ([MimeTypeID])
GO
ALTER TABLE [dbo].[Asset] CHECK CONSTRAINT [FK_Asset_MimeType]
GO
ALTER TABLE [dbo].[Asset] ADD  CONSTRAINT [DF_Asset_Active]  DEFAULT ((0)) FOR [Active]
GO
ALTER TABLE [dbo].[Asset] ADD  CONSTRAINT [DF_Asset_DownloadTries]  DEFAULT ((0)) FOR [DownloadTries]
GO
ALTER TABLE [dbo].[Asset] ADD  CONSTRAINT [DF_Asset_RetryTimes]  DEFAULT ((0)) FOR [RetryTimes]
GO
ALTER TABLE [dbo].[Asset] ADD  CONSTRAINT [DF_Asset_EncodingProgress]  DEFAULT ((0)) FOR [EncodingProgress]
GO
