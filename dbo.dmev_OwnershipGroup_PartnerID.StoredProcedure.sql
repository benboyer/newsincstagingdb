USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[dmev_OwnershipGroup_PartnerID]
	--declare
	@CreatedDate datetime = null,
	@Zoneid int = null,
	@Partnerid int = null,
	@TrackingGroup int = null,
	@name varchar(60) = null
 as
	-- set @Partnerid = 1896

select	* 
from	dme_OwnershipGroup_Partner (nolock) ogp
where	ogp.CreatedDate >= ISNULL(@CreatedDate, ogp.CreatedDate)
and		ogp.OwnershipGroupID_ZoneID = ISNULL(@ZoneID, ogp.OwnershipGroupID_ZoneID)
and		ogp.PartnerID = ISNULL(@PartnerID, ogp.partnerid)
and		ogp.TrackingGroup = ISNULL(@TrackingGroup, ogp.TrackingGroup)
and		ogp.Name like '%' + ISNULL(@name, ogp.Name) + '%'
GO
