USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EncodingDotComOutput](
	[EncodingDotComOutputID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[AssetTypeID] [int] NOT NULL,
	[OutputPlatform] [varchar](50) NULL,
 CONSTRAINT [PK_EncodingDotComOutput] PRIMARY KEY CLUSTERED 
(
	[EncodingDotComOutputID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[EncodingDotComOutput]  WITH CHECK ADD  CONSTRAINT [FK_EncodingDotComOutput_AssetType] FOREIGN KEY([AssetTypeID])
REFERENCES [dbo].[AssetType] ([AssetTypeID])
GO
ALTER TABLE [dbo].[EncodingDotComOutput] CHECK CONSTRAINT [FK_EncodingDotComOutput_AssetType]
GO
