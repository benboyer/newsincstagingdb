USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[adm_OwnershipGroup_list]
	@groupid int = null
as
BEGIN
	set fmtonly OFF
	set nocount on
	exec SprocTrackingUpdate 'adm_OwnershipGroup_list'

	select OwnershipGroupID, OwnershipGroup, AdUnit, UpdatedDate, UpdatedBy 
	from ownershipgroup
	where ownershipgroupid = isnull(@groupid, ownershipgroupid)

	set nocount off
END
GO
