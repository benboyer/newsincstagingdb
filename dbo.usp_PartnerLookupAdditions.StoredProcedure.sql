USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_PartnerLookupAdditions]
	@Name varchar(60),
	@DPID bigint,
	@Constant varchar(60) = null,
	@Description varchar(100) = null,
	@DisplayConstant varchar(100) = null
as
BEGIN
exec SprocTrackingUpdate 'usp_PartnerLookupAdditions'

	if (select COUNT(*) from PartnerLookup where PartnerID = @dpid) > 0
		Select 'Already in table' MessageBack
	else
		insert into PartnerLookup (PartnerID, Name, Constant, Description, DisplayConstant)
		Select @DPID, @Name, ISNULL(@Constant, ''), ISNULL(@Description, ''), ISNULL(@DisplayConstant, '')
END
GO
