USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UploadContentAsset](
	[UploadContentAssetID] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[UploadContentID] [bigint] NOT NULL,
	[MimeType] [varchar](20) NULL,
	[Height] [int] NULL,
	[FilePath] [varchar](1000) NULL,
	[Width] [int] NULL,
	[Bitrate] [int] NULL,
	[Duration] [decimal](10, 2) NULL,
	[Final] [bit] NULL,
 CONSTRAINT [PK_UploadContentAsset] PRIMARY KEY CLUSTERED 
(
	[UploadContentAssetID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [NDX_UCA_ucid_etc] ON [dbo].[UploadContentAsset] 
(
	[UploadContentID] ASC
)
INCLUDE ( [UploadContentAssetID],
[MimeType],
[Height],
[FilePath],
[Width],
[Bitrate],
[Duration],
[Final]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UploadContentAsset]  WITH CHECK ADD  CONSTRAINT [FK_UploadContentAsset_UploadContent] FOREIGN KEY([UploadContentID])
REFERENCES [dbo].[UploadContent] ([UploadContentID])
GO
ALTER TABLE [dbo].[UploadContentAsset] CHECK CONSTRAINT [FK_UploadContentAsset_UploadContent]
GO
