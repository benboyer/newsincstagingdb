USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_LandingURL_Update]
	-- declare
		@partnerid int,
		@LandingURL varchar(max),
		@LandingURLTypeID int = 1,
		@Active bit = 1
as
BEGIN
	SET FMTONLY OFF
	set nocount on
	exec SprocTrackingUpdate 'usp_LandingURL_Update'


	declare @LandingURLID int
	
	if @Active is null 
		set @Active = 1

	if @LandingURLTypeID is null
		set @LandingURLTypeID = 1
		
	if @partnerid is null or @LandingURL is null
	begin
		select 'You must provide a PartnerID and URL for the Landing Page' as MessageBack
		return
	end
	-- declare @TrackingGroup int = 58285, @LandingURL varchar(max) = 'http://www.suntimes.com/video/index.html ', @landingurltypeid int = 1

--	if @partnerid is null
--	begin
--		if	(select COUNT(*)
--			from partner p
--			where TrackingGroup = @TrackingGroup)
--			> 1
--		begin
--			set @partnerid = (select partnerid from mTrackingGroup where TrackingGroup = @TrackingGroup)
--		end

--		if @partnerid is null
--			set @partnerid = (select PartnerID from Partner where TrackingGroup = @TrackingGroup)
--	end
	-- Update the partner table (partner table is interium usage, soon to become legacy usage)
	-- select * from partner where partnerid = 127
	-- declare @partnerid int = 1653, @LandingURL varchar(max) = 'http://landing.newsinc.com/wtvc/video.html', @landingurltypeid int = 1, @active bit = 1, @LandingURLID int

	if @LandingURLTypeID = 1
	begin
		update partner set LandingURL = @LandingURL
		where PartnerID = @partnerid

	-- Update the NDNUsersLegacy table (legacy usage)
		update b set b.LandingURL = @LandingURL 
		from	(select MAX(usertypeid) usertypeid, PartnerId
				from NDNUsersLEGACY
				where PartnerId = (select TrackingGroup from Partner where PartnerID = @partnerid)
				group by PartnerId) a
		join	NDNUsersLEGACY b
		on		a.PartnerId = b.PartnerId and a.usertypeid = b.UserTypeID
	end

	-- Now work on the new/current stuff

	--	First, add to (or update) the LandingURL table
	-- declare @active bit = 1, @landingurlid int, @partnerid int = 127, @TrackingGroup int = 58285, @LandingURL varchar(max) = 'http://www.suntimes.com/video/index.html ', @landingurltypeid int = 1
	if not exists 
		(select 1 from LandingURL where LandingURL = @LandingURL)
	begin
		insert into LandingURL(LandingURL, LandingURLTypeID, Active)
		select @LandingURL, @LandingURLTypeID, @Active
	end
	else	-- it exists, so we must be updating something.  About the only thing to update is the Active flag or the type
	begin
		update LandingURL 
		set	LandingURLTypeID = @LandingURLTypeID,
			Active = @Active
			where	LandingURL = @LandingURL
	end
	--	Second, get the ID of the row we just inserted
	set @LandingURLID = (select LandingURLID from LandingURL where LandingURL = @LandingURL)

	-- select @LandingURLID
	-- select * from LandingURL where LandingURLID = 35 
	-- exec GetLauncherShow 1294, 58285, 1

	--	Third, Deactivate any old links from the partner to this type of landingurl if one we are adding is active
	-- declare @active bit = 1, @landingurlid int = 35, @partnerid int = 127, @TrackingGroup int = 58285, @LandingURL varchar(max) = 'http://www.suntimes.com/video/index.html ', @landingurltypeid int = 1

	if		@Active = 1
	begin
		update	l
		set		l.active = 0
		from	Partner_LandingURL plu
		join	LandingURL l
		on		plu.LandingURLID = l.LandingURLID
		where	partnerid = @partnerid
		and		l.LandingURLTypeID = @LandingURLTypeID
		and		l.landingurl <> @LandingURL
	end
	
	--	Fourth, Add the Link between the partner and the landingurl, and we're done
	if not exists
		(select 1
		from	Partner_LandingURL
		where	partnerid = @partnerid
		and		LandingURLID = @LandingURLID)
	begin
		insert into Partner_LandingURL(PartnerID, LandingURLID)
		select	@partnerid, @LandingURLID
	end	

	if @@ERROR = 0
	exec usp_ListLandingUrls @partnerid
		
	set nocount off
END
GO
