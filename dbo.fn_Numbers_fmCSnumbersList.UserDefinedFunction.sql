USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_Numbers_fmCSnumbersList]
(
	@Numberslist varchar(max)
)
RETURNS @Numberstable TABLE
(RowNumber int, Number bigint)
AS
BEGIN
		declare @wholeNumber varchar(max),
				@lastNumber varchar(120),
				@rownum int = 1

		set		@wholeNumber = @Numberslist

		while	(select CHARINDEX(',', @wholeNumber, 1)) > 0
		begin
			set @lastNumber = (select	left(@wholeNumber, CHARINDEX(',', @wholeNumber, 1) -1))
			insert @Numberstable(RowNumber, Number) select @rownum, rtrim(ltrim(@lastNumber))
			set @lastNumber = @lastNumber + ','
			set @wholeNumber = replace(@wholeNumber, @lastNumber, '')
			set @rownum = @rownum + 1
		end
		insert @Numberstable (RowNumber, Number) select @rownum, rtrim(ltrim(@wholeNumber))
	RETURN
END;
GO
