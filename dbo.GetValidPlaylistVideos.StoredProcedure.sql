USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetValidPlaylistVideos]
	-- Add the parameters for the stored procedure here
	@userId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @trackinggroup int = (select trackinggroup from mTrackingGroup where partnerid = (select partnerid from [User] where UserID = @userId))
	exec SprocTrackingUpdate 'GetValidPlaylistVideos'
    -- Insert statements for procedure here
	SELECT COUNT([pv].[PlaylistID]) AS [value], [p].[PlaylistID] AS [key]
	FROM [dbo].[Playlists] AS [p]
	left outer JOIN [dbo].[PlaylistVideos] AS [pv] ON [p].[PlaylistID] = [pv].[PlaylistID]
	left outer join [dbo].[Videos] AS [v] ON [pv].[VideoID] = [v].[VideoID]
	WHERE (([p].[UserID] = @userId or [p].SharingOption = 2) and [p].isdeleted=0)
	AND ((isnull([v].[IsDeleted],0) = 0) )
	AND (isnull([v].[IsEnabled],1) = 1)
	AND (([v].[ExpiryDate] IS NULL) OR ([v].[ExpiryDate] > getdate()))
	and	dbo.AllowedContentID(v.videoid, @trackinggroup, 1) = 1
	GROUP BY [p].[PlaylistID]

END
GO
