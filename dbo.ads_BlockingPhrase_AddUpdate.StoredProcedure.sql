USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[ads_BlockingPhrase_AddUpdate]
	--declare
	@ID int = null,
	@keyingPhrase varchar(100),
	@active bit,
	@blockingPhrase varchar(100),
	@user varchar(20),
	@deleteit bit = 0
AS
	-- select @ID = 24, @keyingphrase =  'are we blocked?', @active = 0, @blockingphrase =  'YesWeAre', @user = 'bubba', @deleteit = 0
BEGIN
	set fmtonly OFF
	set nocount ON
	exec SprocTrackingUpdate 'ads_AddBlockingPhrase'

	select @keyingPhrase = ltrim(RTRIM(@keyingPhrase))
	select @blockingPhrase = ltrim(RTRIM(@blockingPhrase))

	if	(ISNULL(@keyingPhrase, '') = '' or ISNULL(@blockingPhrase, '') = '' ) and @deleteit = 0
	begin
		select 'Nice Try.  Use real values' as MessageBack
		return
	End

	if LEFT(@keyingPhrase, 1) <> '%'
		select @keyingPhrase = '%' + @keyingPhrase

	if RIGHT(@keyingPhrase, 1) <> '%'
		select @keyingPhrase = @keyingPhrase + '%' 

	if	@user is null
	begin
		select 'Nice Try.  Identify yourself!' as MessageBack
		return
	End

	if @deleteit is null
		set @deleteit = 0
-- select * from ads_PhrasesToBlockAds
	if (select COUNT(*) from ads_PhrasesToBlockAds (nolock) where PhrasesToFilterAds = @ID) = 1 and @deleteit = 0
	begin
		update ads_PhrasesToBlockAds set phrase = @keyingPhrase, KeywordAddPhrase = @blockingPhrase, filterActive = @active, UpdatedDate = GETDATE(), AddUpdateUser = @user where PhrasesToFilterAds = @ID
		--update ads_PhrasesToBlockAds set phrase = @keyingPhrase, UPdatedDate = GETDATE(), AddUpdateUser = @user where PhrasesToFilterAds = @ID and phrase <> @keyingPhrase
		--update ads_PhrasesToBlockAds set KeywordAddPhrase = @blockingPhrase, UpdatedDate= GETDATE(), AddUpdateUser = @user where PhrasesToFilterAds = @ID and KeywordAddPhrase <> @blockingPhrase
	end
	else
	begin
		if @deleteit = 0
		begin
			insert into ads_PhrasesToBlockAds(phrase, filterAddedDate, filterActive, KeywordAddPhrase, AddUpdateUser)
			select @keyingPhrase, GETDATE(), @active, @blockingPhrase, @user
			where not exists (select 1
							from ads_PhrasesToBlockAds
							where	Phrase = @keyingPhrase
							and		KeywordAddPhrase <> @blockingPhrase)
		end
		if @deleteit = 1
		begin
			delete from ads_PhrasesToBlockAds where PhrasesToFilterAds = @ID
		end
	end
	set nocount OFF
END
GO
