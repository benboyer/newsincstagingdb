USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [deploy].[promoteQAtoStaging]
	-- declare 
	@forder int = null
as
BEGIN
	update	a 
	set		a.depends_on = b.depends_on,
			a.installation_code = b.installation_code,
			a.for_schema = b.for_schema,
			a.tickets_applicable = b.tickets_applicable,
			a.description = b.description -- declare @forder int = 2 select *
	from NewsincQA.deploy.schema_feature a
	join deploy.schema_feature b
	on	a.feature_order = b.feature_order
	and	a.feature_type = b.feature_type
	and a.feature_name = b.feature_name
	where	(@forder is null or a.feature_order = @forder)
	and		(a.installation_code <> b.installation_code
			or 
			a.for_schema <> b.for_schema
			or
			isnull(a.depends_on, 0) <> isnull(b.depends_on, 0))

	insert into deploy.schema_feature(feature_order, feature_type, feature_name, defined_on, defined_by, depends_on, installation_code, for_schema, tickets_applicable, description)
	-- declare @forder int = 2
		select a.feature_order, a.feature_type, a.feature_name, a.defined_on, a.defined_by, a.depends_on, a.installation_code, a.for_schema, a.tickets_applicable, a.description
	from NewsincQA.deploy.schema_feature a
	left join deploy.schema_feature b
	on	a.feature_order = b.feature_order
	and	a.feature_type = b.feature_type
	and a.feature_name = b.feature_name
	where	(@forder is null or a.feature_order = @forder)
	and		a.completed_on is not null
	and		b.feature_order is null
END
GO
