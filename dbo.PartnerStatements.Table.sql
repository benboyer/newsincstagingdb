USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PartnerStatements](
	[PartnerStatementID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PartnerID] [int] NOT NULL,
	[DPID] [int] NOT NULL,
	[RootPath] [varchar](200) NOT NULL,
	[YearMonthPath] [varchar](20) NOT NULL,
	[TypePath] [varchar](20) NOT NULL,
	[FileName] [varchar](20) NOT NULL,
 CONSTRAINT [PK_PartnerStatements] PRIMARY KEY CLUSTERED 
(
	[PartnerStatementID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[PartnerStatements] ADD  CONSTRAINT [DF_PartnerStatments_Active]  DEFAULT ('1') FOR [YearMonthPath]
GO
