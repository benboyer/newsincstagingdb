USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Launcher](
	[LauncherID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[PartnerID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[LauncherTypeID] [int] NULL,
	[PlayerTypeID] [int] NULL,
	[SectionID] [int] NULL,
	[Active] [bit] NOT NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[CreatedUserID] [int] NULL,
	[UpdatedDate] [smalldatetime] NOT NULL,
	[UpdatedUserID] [int] NULL,
	[isDefault] [bit] NULL,
	[GenerateNow] [bit] NULL,
	[Height] [int] NULL,
	[Width] [int] NULL,
	[ContinuousPlay] [int] NULL,
	[DisableEmailButton] [bit] NULL,
	[DisableAdsOnPlayer] [bit] NULL,
	[DisableShareButton] [bit] NULL,
	[DisableAutoPlay] [bit] NULL,
	[DisableFullScreen] [bit] NULL,
	[LauncherTemplateID] [int] NULL,
	[LaunchInLandingPage] [bit] NOT NULL,
	[TransitionSeconds] [int] NOT NULL,
 CONSTRAINT [PK_Launcher] PRIMARY KEY CLUSTERED 
(
	[LauncherID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [NDX_Lau_actLIDltid] ON [dbo].[Launcher] 
(
	[Active] ASC,
	[LauncherID] ASC,
	[LauncherTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Launcher]  WITH CHECK ADD  CONSTRAINT [FK_Launcher_Launcher_template] FOREIGN KEY([LauncherTemplateID])
REFERENCES [dbo].[LauncherTemplate] ([LauncherTemplateID])
GO
ALTER TABLE [dbo].[Launcher] CHECK CONSTRAINT [FK_Launcher_Launcher_template]
GO
ALTER TABLE [dbo].[Launcher]  WITH CHECK ADD  CONSTRAINT [FK_Launcher_LauncherType] FOREIGN KEY([LauncherTypeID])
REFERENCES [dbo].[LauncherType] ([LauncherTypeID])
GO
ALTER TABLE [dbo].[Launcher] CHECK CONSTRAINT [FK_Launcher_LauncherType]
GO
ALTER TABLE [dbo].[Launcher]  WITH CHECK ADD  CONSTRAINT [FK_Launcher_Partner] FOREIGN KEY([PartnerID])
REFERENCES [dbo].[Partner] ([PartnerID])
GO
ALTER TABLE [dbo].[Launcher] CHECK CONSTRAINT [FK_Launcher_Partner]
GO
ALTER TABLE [dbo].[Launcher] ADD  CONSTRAINT [DF_Launcher_PlayerTypeID]  DEFAULT ((1)) FOR [PlayerTypeID]
GO
ALTER TABLE [dbo].[Launcher] ADD  CONSTRAINT [DF__Launcher__Create__226AFDCB]  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Launcher] ADD  CONSTRAINT [DF_Launcher_isDefault]  DEFAULT ((0)) FOR [isDefault]
GO
ALTER TABLE [dbo].[Launcher] ADD  CONSTRAINT [DF_Launcher_GenerateNow]  DEFAULT ((0)) FOR [GenerateNow]
GO
ALTER TABLE [dbo].[Launcher] ADD  CONSTRAINT [DF_Launcher_DisableSocialNetworking]  DEFAULT ((0)) FOR [DisableEmailButton]
GO
ALTER TABLE [dbo].[Launcher] ADD  CONSTRAINT [DF_Launcher_DisableAds]  DEFAULT ((0)) FOR [DisableAdsOnPlayer]
GO
ALTER TABLE [dbo].[Launcher] ADD  CONSTRAINT [DF_Launcher_DisableSingleImbed]  DEFAULT ((0)) FOR [DisableShareButton]
GO
ALTER TABLE [dbo].[Launcher] ADD  CONSTRAINT [DF_Launcher_DisableVideoPlayOnLoad]  DEFAULT ((0)) FOR [DisableAutoPlay]
GO
ALTER TABLE [dbo].[Launcher] ADD  CONSTRAINT [DF_Launcher_DisableFullScreen]  DEFAULT ((0)) FOR [DisableFullScreen]
GO
ALTER TABLE [dbo].[Launcher] ADD  CONSTRAINT [DF_Launcher_LaunchInLandingPage]  DEFAULT ((0)) FOR [LaunchInLandingPage]
GO
ALTER TABLE [dbo].[Launcher] ADD  CONSTRAINT [DF_Launcher_TransitionSeconds]  DEFAULT ((3)) FOR [TransitionSeconds]
GO
