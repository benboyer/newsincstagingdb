USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_Set_Partner_TargetPlatform]
--	declare
		@PartnerID int,
		@PlatformID int,
		@IngestOn bit,
		@DistributeOn bit,
		@ProfileDescriptionID int = null

as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'usp_Set_Partner_TargetPlatform'

		-- select @PartnerID = 499, @PlatformID = 2, @ProfileDescriptionID = 4, @IngestOn = 1, @DistributeOn = 1
		if @PartnerID = 0 and @PlatformID = 0
		begin
			select * from TargetPlatform
			select * from ProfileDescription
			return
		end

		if	not exists (select 1 from Partner where PartnerID = @PartnerID)
		begin
			select 'Invalid PartnerID'
			return
		end
		if not exists (select 1 from TargetPlatform where TargetPlatformID = @PlatformID)
		begin
			select 'Invalid TargetPlatformID'
			-- select * from TargetPlatform
			return
		end
		if @ProfileDescriptionID is not null and not exists (select 1 from ProfileDescription where ProfileDescriptionID = @ProfileDescriptionID)
		begin
			select 'Invalid ProfileDescriptionID'
			--select * from ProfileDescription
			return
		end

		if	(select COUNT(*)
			from	Partner_TargetPlatformProfile
			where	PartnerID = @PartnerID
			and		TargetPlatformID = @PlatformID) > 1
		begin
			select 'Duplicate profiles exists for this partner for this platform.  Please fix.'
			return
		end

		if	(select COUNT(*)
			from	Partner_TargetPlatformProfile
			where	PartnerID = @PartnerID
			and		TargetPlatformID = @PlatformID) = 1
		begin
			update	Partner_TargetPlatformProfile
			set		ProfileDescriptionID = isnull(@ProfileDescriptionID, ProfileDescriptionID), isImportEnabled = @IngestOn, isDistributionEnabled = @DistributeOn
			where	PartnerID = @PartnerID
			and		TargetPlatformID = @PlatformID

			select 'Profile Updated'
			return
		end


		if	(select COUNT(*)
			from	Partner_TargetPlatformProfile
			where	PartnerID = @PartnerID
			and		TargetPlatformID = @PlatformID) = 0
		begin
			insert into Partner_TargetPlatformProfile (PartnerID, ProfileDescriptionID, TargetPlatformID, isImportEnabled, isDistributionEnabled)
			select @PartnerID, @ProfileDescriptionID, @PlatformID, @IngestOn, @DistributeOn
			select 'Profile Created'
			return
		end

	set nocount off
END
GO
