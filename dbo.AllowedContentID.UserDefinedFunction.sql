USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
-- THIS LOGIC NEEDS TO BE UPDATED IN FOUR PLACES  
-- AllowedContentByPartnerID  
-- AllowedContentByLauncherID  
-- AllowedContentByPlaylistID  
-- AllowedContentID  
-- updated 9/11/2013 (and -> or in dmaexclude check)
-- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
CREATE FUNCTION [dbo].[AllowedContentID]    
(    
  @videoID bigint,    
  @trackinggroup int,    
  @targetplatform int    
)    
RETURNS bit    
AS    
BEGIN    
 DECLARE @allowed bit = 0,    
  -- @AllowedContentID bigint,    
  -- declare @videoid int = 24341359, @trackinggroup int = 99999, @targetplatform int = 1,    
   @now datetime = getdate()    
 if @targetplatform = 3    
  set @targetplatform = 1    
 set  @Allowed =    
   isnull((select top 1 1    
 -- select isnull((select 1    
 -- declare @videoid int = 23802912, @trackinggroup int = 99999, @targetplatform int = 1, @now datetime = getdate() select *    
    
 from (select * from Content (nolock) where contentid = @videoid and active = 1) c    
 join Partner (nolock) p on c.partnerid = p.partnerid    
 join (select * from Partner_TargetPlatformProfile (nolock) where targetplatformid = @targetplatform and isDistributionEnabled = 1) ptp    
 on  c.PartnerID = ptp.PartnerID    
 and  @targetplatform = ptp.TargetPlatformID    
 left join (select partnerid, TrackingGroup from mtrackinggroup (nolock) where TrackingGroup = @trackinggroup) pd    
 on  @trackinggroup = pd.TrackingGroup    
 LEFT JOIN AllowContent ac  (nolock)    
 ON  c.PartnerID = ac.ContentProviderPartnerID    
 and  pd.partnerid = ac.DistributorPartnerID    
 left join DMAExclude dmaEx (nolock) -- select * from DMAExclude -- select top 100 * from mtrackinggroup    
 on  c.ContentID = dmaEx.contentid    
 left join Partner_DMA pdma (nolock)    
 on  dmaex.DMACode = pdma.DMACode    
 and  pd.PartnerID = pdma.PartnerID    
 left join DMAInclude dmaIn (nolock)    
 on  c.ContentID = dmain.ContentID    
 left join Partner_DMA pdmai (nolock)    
 on  dmain.dmacode = pdmai.dmacode    
 and  pd.partnerid = pdmai.PartnerID    
 where c.PartnerID = pd.partnerid    
 or  (c.OnlyOwnerView <> 1    
   and  (p.isContentPrivate = 0    
     or  (p.isContentPrivate = 1 and ac.AllowContentID is not null    
       and (pdma.PartnerID is null    
--        and  (dmaEx.StartDTM IS NULL AND dmaEx.EndDTM IS NULL)    
        or  (dmaEx.StartDTM IS NULL AND dmaEx.EndDTM IS NULL)    
          OR (dmaEx.EndDTM IS NULL AND dmaEx.StartDTM > @now)    
          OR (dmaEx.StartDTM IS NULL AND dmaEx.EndDTM < @now)    
          OR (@now not between dmaEx.StartDTM AND dmaEx.EndDTM)))   
---        or  ((dmaEx.StartDTM IS NULL AND dmaEx.EndDTM IS NULL)    
---          OR (dmaEx.EndDTM IS NULL AND dmaEx.StartDTM > @now)    
---          OR (dmaEx.StartDTM IS NULL AND dmaEx.EndDTM < @now)    
---          OR (@now not between dmaEx.StartDTM AND dmaEx.EndDTM)))))    
     or  (p.isContentPrivate = 1 -- and ac.AllowContentID is null    
       and (pdmai.PartnerID is not null    
        and  (dmaIn.StartDTM IS NULL AND dmaIn.EndDTM IS NULL)    
          OR (dmaIn.EndDTM IS NULL AND dmaIn.StartDTM <= @now)    
          OR (dmaIn.StartDTM IS NULL AND dmaIn.EndDTM >= @now)    
          OR (@now between dmaIn.StartDTM AND dmaIn.EndDTM)))))    
    
  ), 0)    
    
 RETURN @Allowed    
 /*    
 -- When should we allow a partner to see another partner's content?    
 p.partnerid = pD.partnerid    
 p.partnerid <> dp>patnerid and onlyownerview <> 0    
  and p.private and pD.in allowcontent and (dmaexcludeid is null or within allowable dates)    
   or p.private and pD. not in allowcontent and (dmaincludeid not null and no dates or within allowable dates)    
 */    
    
END
GO
