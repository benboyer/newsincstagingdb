USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[vid_ShowPlatformsForVideo]
	--declare
	@VideoID bigint

as
BEGIN
	set FMTONLY OFF
	set NOCOUNT ON
	exec SprocTrackingUpdate 'vid_ShowPlatformsForVideo'

	select distinct tp.Name
	from (select * from Content (nolock) where ContentID = @VideoID) a
	join Content_Asset (nolock) ca
	on		a.ContentID = ca.ContentID
	join	TargetPlatform tp
	on		ca.TargetPlatformID = tp.TargetPlatformID

	set NOCOUNT OFF
END
GO
