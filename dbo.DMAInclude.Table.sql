USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DMAInclude](
	[DMAIncludeID] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ContentID] [int] NOT NULL,
	[DMACode] [int] NULL,
	[StartDTM] [smalldatetime] NULL,
	[EndDTM] [smalldatetime] NULL,
 CONSTRAINT [PK_DMAInclude] PRIMARY KEY CLUSTERED 
(
	[DMAIncludeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DMAIncludeCIDdmacd] ON [dbo].[DMAInclude] 
(
	[ContentID] ASC,
	[DMACode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_DMAIncludeStdtEnDt] ON [dbo].[DMAInclude] 
(
	[StartDTM] ASC,
	[EndDTM] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DMAInclude]  WITH CHECK ADD  CONSTRAINT [FK_DMAInclude_DMAInclude] FOREIGN KEY([ContentID])
REFERENCES [dbo].[Content] ([ContentID])
GO
ALTER TABLE [dbo].[DMAInclude] CHECK CONSTRAINT [FK_DMAInclude_DMAInclude]
GO
ALTER TABLE [dbo].[DMAInclude]  WITH CHECK ADD  CONSTRAINT [FK_DMAInclude_DMAIncludeDMAcd] FOREIGN KEY([DMACode])
REFERENCES [dbo].[DMA] ([DMACode])
GO
ALTER TABLE [dbo].[DMAInclude] CHECK CONSTRAINT [FK_DMAInclude_DMAIncludeDMAcd]
GO
