USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [deploy].[installed_features] as
select *
from deploy.schema_feature
where completed_on is not null
GO
