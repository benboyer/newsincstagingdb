USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SectionPartner_Delete]
--	declare
	@SectionPartnerID int = null,
	@partnerid int,
	@deleteold bit = 0
AS
BEGIN
	set fmtonly off
	SET NOCOUNT ON
	exec SprocTrackingUpdate 'SectionPartner_Delete'

	if @deleteold = 1
	BEGIN
		delete from Section where SectionPartnerID = @SectionPartnerID and PartnerID = @partnerid
	END
	ELSE
	BEGIN
		delete from SectionPartner
		where	SectionPartnerID = @SectionPartnerID
		and PartnerID = @partnerid
	END

	set nocount off
END
GO
