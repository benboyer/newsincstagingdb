USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExternalEncodingStatus](
	[VideoID] [int] NOT NULL,
	[ThumbnailMediaId] [int] NULL,
	[ThumbnailFinished] [bit] NULL,
	[StillFrameMediaId] [int] NULL,
	[StillFrameFinished] [bit] NULL,
	[VideoMediaId] [int] NULL,
	[VideoFinished] [bit] NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'Note', @value=N'Legacy Table that needs to be rethought or eventually deleted.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExternalEncodingStatus'
GO
