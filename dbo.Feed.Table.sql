USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Feed](
	[FeedId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[FeedUrl] [nvarchar](1000) NULL,
	[PartnerID] [int] NOT NULL,
	[XSLT] [text] NOT NULL,
	[UploadXSLT] [nvarchar](1000) NULL,
	[Active] [bit] NOT NULL,
	[UploadActive] [bit] NULL,
	[UserName] [nvarchar](255) NULL,
	[Password] [nvarchar](255) NULL,
	[NotifyEmail] [varchar](100) NULL,
	[SMILSXLT] [nchar](200) NULL,
	[SourceID] [smallint] NOT NULL,
	[FilterIncludes] [bit] NULL,
	[FilterValues] [varbinary](150) NULL,
	[GetInterval] [varchar](10) NULL,
	[IntervalValue] [int] NULL,
	[TranscriptXslt] [nvarchar](1000) NULL,
	[MaxUpdates] [int] NOT NULL,
	[MaxUpdatesExceptions] [varchar](200) NULL,
	[MaxDownloads] [int] NOT NULL,
	[ContentImportSystemID] [int] NOT NULL,
	[FeedPriorityID] [int] NOT NULL,
	[MaxItems] [int] NULL,
	[ControlDate] [datetime] NULL,
	[ControlDateEnd] [datetime] NULL,
 CONSTRAINT [PK_RSSFeeds] PRIMARY KEY CLUSTERED 
(
	[FeedId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_FeedPID] ON [dbo].[Feed] 
(
	[PartnerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Feed]  WITH CHECK ADD  CONSTRAINT [FK_Feed_Partner] FOREIGN KEY([PartnerID])
REFERENCES [dbo].[Partner] ([PartnerID])
GO
ALTER TABLE [dbo].[Feed] CHECK CONSTRAINT [FK_Feed_Partner]
GO
ALTER TABLE [dbo].[Feed] ADD  CONSTRAINT [DF__RSSFeeds__Active__7246E95D]  DEFAULT ((1)) FOR [Active]
GO
ALTER TABLE [dbo].[Feed] ADD  CONSTRAINT [DF_Feed_MaxUpdates]  DEFAULT ((5)) FOR [MaxUpdates]
GO
ALTER TABLE [dbo].[Feed] ADD  CONSTRAINT [DF_Feed_MaxDownloads]  DEFAULT ((3)) FOR [MaxDownloads]
GO
ALTER TABLE [dbo].[Feed] ADD  CONSTRAINT [DF_Feed_ContentImportSystemID]  DEFAULT ((0)) FOR [ContentImportSystemID]
GO
ALTER TABLE [dbo].[Feed] ADD  CONSTRAINT [DF_Feed_FeedPriorityID]  DEFAULT ((2)) FOR [FeedPriorityID]
GO
