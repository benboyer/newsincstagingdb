USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ps_fn_getAssetDataMultiAsset](@partnerID int, @contentID int)
	RETURNS @assetinfo TABLE
(
	 AssetID int,
	 FileExtension varchar(20),
	 [Filename] varchar(200),
	 FilePath varchar(200),
	 Name nvarchar(80),
	 Duration [decimal](20, 2),
	 AssetLocation varchar (500) NULL,
	 AssetMimeType varchar (500) NULL,
	 AssetSortOrder int NULL,
	 AssetLocation2 varchar (500) NULL,
	 AssetMimeType2 varchar (500) NULL,
	 AssetSortOrder2 int NULL
)
AS
BEGIN

	/*
	declare @defaulturl varchar(500)
	 set @defaulturl = (select FilePath + '/' + Filename from Asset where AssetID = @assetid)
	if LEFT(@defaulturl, 7) <> 'http://' and LEFT(@defaulturl, 7) <> 'rtmp://'
	 set @defaulturl = 'http://'+@defaulturl
	*/
-- @ext not in ('txt')
	insert into @assetinfo(AssetID, FileExtension, [Filename], FilePath, Name, Duration, AssetLocation, AssetMimeType, AssetSortOrder, AssetLocation2, AssetMimeType2, AssetSortOrder2)
	select
		fa.AssetID,
		fmt.FileExtension,
		fa.[Filename],
		fa.FilePath,

		fat.Name,
		fa.duration,

		ISNULL(
		 REPLACE(replace(replace(ft1.urlline, '{assetid}', convert(varchar,fca.AssetID)), '{partnerid}', convert(varchar,@partnerID)), '{contentid}', convert(varchar,fca.ContentID)),
		 (select FilePath + '/' + Filename from Asset where AssetID = fca.AssetID)) as url,
		ft1.MimeType, ft1.SortOrder,

		ISNULL(
		 REPLACE(replace(replace(ft2.urlline, '{assetid}', convert(varchar,fca.AssetID)), '{partnerid}', convert(varchar,@partnerID)), '{contentid}', convert(varchar,fca.ContentID)),
		 (select FilePath + '/' + Filename from Asset where AssetID = fca.AssetID)) as url,
		ft2.MimeType, ft2.SortOrder

	from	Content_Asset fca
	inner join	assettype fat
  -- on fca.ContentID = @contentID and fca.AssetTypeID = fat.AssetTypeID and fca.AssetTypeID in (1,2,3,9,17)--and (fca.TargetPlatformID = 1 or fca.TargetPlatformID = 2 and fca.AssetTypeID = 9)
	on	fca.ContentID = @contentID and fca.AssetTypeID = fat.AssetTypeID and (fca.TargetPlatformID = 1 or fca.TargetPlatformID = 2 and fca.AssetTypeID = 9)
	inner join	Asset fa
	on	fa.AssetID = fca.AssetID
	inner join	MimeType fmt
	on	fa.mimetypeid = fmt.mimetypeid
--*******************
/*
 inner join
Partner_TargetPlatformProfile ptp
  on @partnerID = ptp.PartnerID --and (3 = ptp.TargetPlatformID)
join TargetPlatform_AssetType tpat
  on ptp.TargetPlatformID = tpat.TargetPlatformID and tpat.AssetTypeID = fat.AssetTypeID
*/
--*******************
	left outer join	vw_partnerDefaultSettings fpds1
	on	fpds1.PartnerID = @partnerID and fpds1.Setting = FileExtension + 'stream' and fpds1.Entity = 'PlayerServiceDeliveryTemplate'
	left outer join	vw_partnerDefaultSettings fpds2
	on	fpds2.PartnerID = @partnerID and fpds1.Setting = FileExtension + 'progressive' and fpds2.Entity = 'PlayerServiceDeliveryTemplate'
	left outer join	vw_sysDefaultSettings fvds1
	on	fvds1.Setting = FileExtension + 'stream' and fvds1.Entity = 'PlayerServiceDeliveryTemplate'
	left outer join	vw_sysDefaultSettings fvds2
	on	fvds2.Setting = FileExtension + 'progressive' and fvds2.Entity = 'PlayerServiceDeliveryTemplate'
	left outer join	Templates ft1
	on	ft1.TemplatesID = coalesce(fpds1.Value, fvds1.Value)
	left outer join	Templates ft2
	on	ft2.TemplatesID = coalesce(fpds2.Value, fvds2.Value)

	update	@assetinfo 
	set		AssetLocation = 'http://'+AssetLocation
	where	LEFT(AssetLocation, 7) <> 'http://' and LEFT(AssetLocation, 7) <> 'rtmp://' and Name not like 'external%'


	update @assetinfo 
	set AssetLocation2 = 'http://'+AssetLocation2
	where LEFT(AssetLocation2, 7) <> 'http://' and LEFT(AssetLocation2, 7) <> 'rtmp://' and Name not like 'external%'

return

END
GO
