USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContentLinkLog](
	[ContentLinkLogID] [bigint] IDENTITY(1,1) NOT NULL,
	[PartnerID] [int] NULL,
	[StoryGUID] [varchar](64) NULL,
	[ContentID] [bigint] NULL,
	[VideoGUID] [varchar](64) NULL,
	[StoryStorageLink] [varchar](200) NULL,
	[LastUpdated] [datetime] NULL,
	[UpdateMethod] [int] NULL,
	[MatchRank] [float] NULL,
 CONSTRAINT [PK_ContentLinkLog] PRIMARY KEY CLUSTERED 
(
	[ContentLinkLogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
