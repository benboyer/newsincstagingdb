USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[VideoStaging] as
SELECT     Content.ContentID AS VideoStagingID, Content.CreatedUserID AS UserID, CAST(1 AS int) AS UserType, ISNULL(ContentVideo.ImportFilePath, v.VideoFilePath) 
                      AS ClipURL, Content.Name AS Title, Content.Description, Content.EffectiveDate AS EventDate, ISNULL(ContentVideo.Height, v.Height) AS Height, 
                      ISNULL(ContentVideo.Width, v.Width) AS Width, ISNULL(ContentVideo.Duration, v.Duration) AS Duration, Content.CreatedDate AS CreatedOn, 
                      ISNULL(ContentVideo.ThumbnailImportFilePath, t.ThumbnailFilePath) AS ThumbnailURL, Content.EffectiveDate AS PublishDate, Content.Keyword AS Keywords, 
                      ContentVideo.GUID, ISNULL(ContentVideo.ImportFilePath, v.VideoFilePath) AS OriginalName, CONVERT(smallint, 
                      CASE WHEN content.ContentImportStatus = 5 THEN 4 WHEN contentvideo.ContentVideoImportStatusID = 5 THEN 4 WHEN ContentVideo.ContentVideoImportStatusID = 7
                       THEN 2 WHEN v.ImportStatusID = 5 THEN 4 WHEN v.ImportStatusID <> 5 THEN 2 WHEN ContentVideo.ContentVideoImportStatusID IS NOT NULL 
                      THEN ContentVideo.ContentVideoImportStatusID ELSE NULL END) AS EncodingStatus, Content.ExpirationDate AS ExpiryDate, ISNULL(Content.Active, v.Active) 
                      AS Active, ISNULL(Content.Active, v.Active) AS IsEnabled, CAST(ISNULL(Content.isDeleted, 0) AS bit) AS IsDeleted, ContentSource.Name AS IngestionSource, 
                      ISNULL(ContentVideo.StillframeImportFilePath, s.StillFrameFilePath) AS StillFrameURL, Content.ContentID AS VideoId, Content.Category, 
                      VideoStagingLEGACY.ReadyForEncoding, VideoStagingLEGACY.EncodingPercentComplete AS EncodingPercentCompleted, 
                      VideoStagingLEGACY.EncodingStarted, VideoStagingLEGACY.EncodingCompleted, VideoStagingLEGACY.Status, VideoStagingLEGACY.FileSize, 
                      VideoStagingLEGACY.AudioTrackURL, VideoStagingLEGACY.EncodedURL, VideoStagingLEGACY.NexidiaStatus, VideoStagingLEGACY.Mp3Started, 
                      VideoStagingLEGACY.Mp3Completed, VideoStagingLEGACY.CodecUsed, VideoStagingLEGACY.FileExtension, VideoStagingLEGACY.NiaStatus
FROM         (SELECT     ContentID, ContentTypeID, PartnerID, Version, ContentSourceID, Name, Description, Category, Keyword, Duration, EffectiveDate, ExpirationDate, 
                                              CreatedDate, CreatedUserID, UpdatedDate, UpdatedUserID, Active, IsFileArchived, isDeleted, FeedID, SourceGUID, ContentImportStatus
                       FROM          Content WITH (nolock)
                       WHERE      (ContentImportStatus <> 9)) AS Content INNER JOIN
                          (SELECT     ContentID, AssetTypeID, MAX(AssetID) AS assetid
                            FROM          Content_Asset
                            WHERE      (TargetPlatformID = 1)
                            GROUP BY ContentID, AssetTypeID) AS mca ON Content.ContentID = mca.ContentID INNER JOIN
                      ContentSource WITH (nolock) ON Content.ContentSourceID = ContentSource.ContentSourceID LEFT OUTER JOIN
                      ContentVideo WITH (nolock) ON Content.ContentID = ContentVideo.ContentID LEFT OUTER JOIN
                      VideoStagingLEGACY WITH (nolock) ON ContentVideo.ContentID = VideoStagingLEGACY.ContentID INNER JOIN
                          (SELECT     ca.ContentID, av.Height, av.Width, ISNULL(av.FilePath, '') + '/' + ISNULL(av.Filename, '') AS VideoFilePath, av.Duration, av.ImportStatusID, av.Active, 
                                                   av.AssetID
                            FROM          Content_Asset AS ca WITH (nolock) LEFT OUTER JOIN
                                                   Asset AS av WITH (nolock) ON ca.AssetID = av.AssetID LEFT OUTER JOIN
                                                   AssetType AS atv WITH (nolock) ON ca.AssetTypeID = atv.AssetTypeID
                            WHERE      (ca.TargetPlatformID = 1) AND (atv.Name = 'VideoPC')) AS v ON Content.ContentID = v.ContentID AND mca.assetid = v.AssetID LEFT OUTER JOIN
                          (SELECT     ca.ContentID, asf.FilePath AS StillFrameFilePath, asf.AssetID
                            FROM          Content_Asset AS ca WITH (nolock) INNER JOIN
                                                   Asset AS asf WITH (nolock) ON ca.AssetID = asf.AssetID INNER JOIN
                                                   AssetType AS atsf WITH (nolock) ON ca.AssetTypeID = atsf.AssetTypeID
                            WHERE      (ca.TargetPlatformID = 1) AND (atsf.Name = 'StillFramePC')) AS s ON Content.ContentID = s.ContentID AND mca.assetid = s.AssetID LEFT OUTER JOIN
                          (SELECT     ca.ContentID, ath.FilePath AS ThumbnailFilePath, ath.AssetID
                            FROM          Content_Asset AS ca WITH (nolock) INNER JOIN
                                                   Asset AS ath WITH (nolock) ON ca.AssetID = ath.AssetID INNER JOIN
                                                   AssetType AS atth WITH (nolock) ON ca.AssetTypeID = atth.AssetTypeID
                            WHERE      (ca.TargetPlatformID = 1) AND (atth.Name = 'ThumbnailPC')) AS t ON Content.ContentID = t.ContentID AND mca.assetid = t.AssetID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Content"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 220
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ContentSource"
            Begin Extent = 
               Top = 6
               Left = 258
               Bottom = 84
               Right = 421
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ContentVideo"
            Begin Extent = 
               Top = 84
               Left = 258
               Bottom = 192
               Right = 477
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "VideoStagingLEGACY"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 243
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "v"
            Begin Extent = 
               Top = 192
               Left = 281
               Bottom = 300
               Right = 435
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "s"
            Begin Extent = 
               Top = 222
               Left = 38
               Bottom = 300
               Right = 202
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t"
            Begin Extent = 
               Top = 300
               Left = 38
               Bottom = 378
               Right = 204
            End
            DisplayFlags = 280' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VideoStaging'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VideoStaging'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VideoStaging'
GO
