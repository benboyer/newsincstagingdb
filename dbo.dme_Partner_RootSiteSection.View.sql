USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[dme_Partner_RootSiteSection]
as
select partnerid, isnull(isDefault, 0) isDefault, value
from sectionpartner
GO
