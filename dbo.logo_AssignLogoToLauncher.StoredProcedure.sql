USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create procedure [dbo].[logo_AssignLogoToLauncher]
	@PartnerID int,
	@LauncherID int,
	@LogoID int
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'logo_AssignLancherToLogo'
			-- select * from logo
	-- declare @partnerid int = 127, @launcherID int = 10800, @logoid int = 5470 -- 9825 is the one to put back -- 9680 is the one we took out
	if exists
		(select 1
		from	Partner_Logo pl (nolock)
		join	Logo lo (nolock)
		on		pl.LogoID = lo.LogoID
		where	pl.LauncherID = @LauncherID
		and		lo.LogoTypeID = (select LogoTypeID from Logo where LogoID = @LogoID)
		and		lo.LogoID <> @LogoID)
	BEGIN
		delete	pl1
		from	(select pl.Partner_LogoID
				from	Partner_Logo pl (nolock)
				join	Logo lo (nolock)
				on		pl.LogoID = lo.LogoID
				where	pl.PartnerID = @partnerid
				and		pl.LauncherID = @LauncherID
				and		lo.LogoTypeID = (select LogoTypeID from Logo where LogoID = @LogoID)
				and		lo.LogoID <> @LogoID) a
		join	partner_Logo pl1
		on		a.Partner_LogoID = pl1.Partner_LogoID
	END
	insert into Partner_Logo (PartnerID, LauncherID, LogoID)
	select @PartnerID, @LauncherID, @LogoID
	where not exists
		(select 1
		from	Partner_Logo pl
		join	Logo l
		on		pl.LogoID = l.LogoID
		where	pl.PartnerID = @PartnerID
		and		pl.LauncherID = @LauncherID
		and		l.LogoTypeID = (select LogoTypeID from Logo where LogoID = @LogoID))

	set nocount off
END
GO
