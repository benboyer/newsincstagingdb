USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[BlockedSites_list]
	@includeInactive bit = 0,
	@domain varchar(1000) = null
AS
BEGIN
	set nocount on
	set fmtonly off
	exec SprocTrackingUpdate 'BlockedSites_List'

	SELECT	BlockedSiteID, Host Domain, isnull(DateUpdatedGMT, DateCreatedGMT) LastUpdatedDate
	  FROM	BlockedSite
	  WHERE ([Active] = 1 or @includeInactive = 1 or Host = @domain)
	  and	 Host = ISNULL(@domain, Host)
	set nocount off
END
GO
