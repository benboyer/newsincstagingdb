USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SetFeedGUIDStatus]
	@PartnerID int = null,
	@status bit = null
AS
BEGIN
	exec SprocTrackingUpdate 'SetFeedGUIDStatus'
	-- declare 	@partnerid int = 1, @status bit = null

	if (@partnerid is null or not exists (select 1 from Partner where PartnerID = @partnerid))
	begin
		select 'Invalid PartnerID submitted.  Check it and try again' as MessageBack
		return
	end
	
	update	Partner
	set		isFeedGUIDActive = @status,
			UpdatedDate = GETDATE(),
			UpdateUserID = 7534
	where	PartnerID = @partnerid
	if (@@ERROR = 0)
		select 'FeedGuid Status Updated' as MessageBack
	else
		select @@ERROR as ErrorCode, 'You have an error, Will Robinson' as MessageBack
end
GO
