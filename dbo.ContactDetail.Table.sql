USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContactDetail](
	[ContactDetailID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[UserID_Conv] [int] NULL,
	[ContactTypeID] [smallint] NOT NULL,
	[Value] [nvarchar](50) NOT NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
	[CreatedUserID] [int] NULL,
 CONSTRAINT [PK_ContactDetail] PRIMARY KEY CLUSTERED 
(
	[ContactDetailID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ContactDetail]  WITH CHECK ADD  CONSTRAINT [FK_ContactDetail_ContactType] FOREIGN KEY([ContactTypeID])
REFERENCES [dbo].[ContactType] ([ContactTypeID])
GO
ALTER TABLE [dbo].[ContactDetail] CHECK CONSTRAINT [FK_ContactDetail_ContactType]
GO
ALTER TABLE [dbo].[ContactDetail] ADD  CONSTRAINT [DF_ContactDetail_CreatedDate]  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
