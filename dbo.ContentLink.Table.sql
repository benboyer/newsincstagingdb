USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContentLink](
	[PartnerID] [int] NOT NULL,
	[StoryGUID] [varchar](64) NOT NULL,
	[VideoGUID] [varchar](64) NULL,
	[ContentID] [bigint] NULL,
	[StoryStorageLink] [varchar](200) NULL,
	[LastUpdated] [datetime] NOT NULL,
	[UpdateMethod] [int] NULL,
	[MatchRank] [float] NULL,
	[TimesUpdated] [int] NOT NULL,
 CONSTRAINT [PK_ContentLink] PRIMARY KEY CLUSTERED 
(
	[PartnerID] ASC,
	[StoryGUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[ContentLink] ADD  CONSTRAINT [DF_ContentLink_MatchRank]  DEFAULT ((0.5)) FOR [MatchRank]
GO
ALTER TABLE [dbo].[ContentLink] ADD  CONSTRAINT [DF_ContentLink_TimesUpdated]  DEFAULT ((0)) FOR [TimesUpdated]
GO
