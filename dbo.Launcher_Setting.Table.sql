USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Launcher_Setting](
	[LauncherID] [int] NOT NULL,
	[SettingID] [int] NOT NULL,
	[Value] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Launcher_Setting] PRIMARY KEY CLUSTERED 
(
	[LauncherID] ASC,
	[SettingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Launcher_Setting]  WITH CHECK ADD  CONSTRAINT [FK_Launcher_Setting_Launcher] FOREIGN KEY([LauncherID])
REFERENCES [dbo].[Launcher] ([LauncherID])
GO
ALTER TABLE [dbo].[Launcher_Setting] CHECK CONSTRAINT [FK_Launcher_Setting_Launcher]
GO
ALTER TABLE [dbo].[Launcher_Setting]  WITH CHECK ADD  CONSTRAINT [FK_Launcher_Setting_Setting] FOREIGN KEY([SettingID])
REFERENCES [dbo].[Setting] ([SettingID])
GO
ALTER TABLE [dbo].[Launcher_Setting] CHECK CONSTRAINT [FK_Launcher_Setting_Setting]
GO
