USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[PlaylistAutoPopulate_Add]
	-- declare 
	@partnerid int
	, @playlistid int = null
	, @SourceCategory varchar(40) = null
	, @DestinationCategory varchar(40) = null
	, @maxVideoCount int = 30
	, @active bit = 1
	, @AllContent bit = 0
	, @deletemap bit = 0
	
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'PlaylistAutoPopulate_Add'

	set	@SourceCategory = LTRIM(RTRIM(@SourceCategory))
	set	@DestinationCategory = LTRIM(RTRIM(@DestinationCategory))

	--	set @partnerid = 1635
	--	set @playlistid = 13595  -- select * from playlist where playlistid = 13595 
	--	set @SourceCategory = 'StLouisPD'	
	--	set @DestinationCategory = 'StLouisPD'	
	--	set @maxVideoCount = 30
	--	set @AllContent = 1
	--	set @deletemap = 0

	if @maxVideoCount > 48 set @maxVideoCount = 48
	
	if not exists (select 1
				from Playlist
				where PlaylistID = @playlistid)
	begin
		select 'Playlist does not exist.  Check it!' as Messageback
		return
	end

	if not exists (select * 
				from Playlist_Content_AutoPopulate 
				where PartnerID = @partnerid)
	begin
		insert into Playlist_Content_AutoPopulate (PartnerID, Active)
		select @partnerid, @active
	end
	if	@AllContent = 1
		and
		(select COUNT(*) from Playlist_Content_PartnerMapDefaultCategory2 where PartnerID = @partnerid and Category = @SourceCategory) = 0
		and
		(select COUNT(*) from Playlist_Content_PartnerMapDefaultCategory where PartnerID = @partnerid  and Category = @SourceCategory) = 0
	begin
		insert into Playlist_Content_PartnerMapDefaultCategory2(PartnerID, Category)
		select @partnerid, @SourceCategory
	end

	if @Playlistid is null and @SourceCategory is null and @DestinationCategory is null and isnull(@deletemap, 0) = 0
	begin
		update Playlist_Content_AutoPopulate
		set Active = @active
		where PartnerID = @partnerid
	end
	
	if (select COUNT(*) from Playlist_Content_PartnerMap
		where PartnerID = @partnerid
		and		PlaylistID = @playlistid
		and		SourceCategory = @SourceCategory
		and		DestinationCategory = @DestinationCategory) = 0
	begin
		 if (select COUNT(*) from Playlist_Content_PartnerMap where PartnerID = @partnerid and PlaylistID is null) = 0
		 and isnull(@deletemap, 0) = 0
		begin
			-- We have no row with an empty playlist map available, so we must insert a row.
			insert into Playlist_Content_PartnerMap(PartnerID, PlaylistID, SourceCategory, DestinationCategory, MaxVideoCount, LastContentID)
			select	@partnerid, @playlistid, @SourceCategory, @DestinationCategory, @maxVideoCount, isnull(MAX(contentid), 0)
			from	Content
			where	PartnerID = @partnerid
			and not exists
				(select 1
				from Playlist_Content_PartnerMap pm
				where pm.PartnerID = @partnerid
				and	pm.PlaylistID = @playlistid
				and pm.SourceCategory = @SourceCategory
				and pm.DestinationCategory = @DestinationCategory)
		end
		else
		begin
			if isnull(@deletemap, 0) = 0
			begin
				-- there is a row with a playlist map available, so fill it
				update Playlist_Content_PartnerMap
				set	PlaylistID = @playlistid, SourceCategory = @SourceCategory, DestinationCategory = @DestinationCategory, MaxVideoCount = @maxVideoCount
					, LastContentID = isnull((select MAX(contentid)
											from Content c
											where	PartnerID = @partnerid), 0)
				where	PartnerID = @partnerid
				and		PlaylistID is null
				and not exists
					(select 1
					from Playlist_Content_PartnerMap pm
					where pm.PartnerID = @partnerid
					and	pm.PlaylistID = @playlistid
					and pm.SourceCategory = @SourceCategory
					and pm.DestinationCategory = @DestinationCategory)
			end
		end
	end

	if isnull(@deletemap, 0) = 1
		and exists 
			(select 1
			from	Playlist_Content_PartnerMap 
			where	PartnerID = @partnerid
			and		PlaylistID = @playlistid
			and		SourceCategory = @SourceCategory
			and		DestinationCategory = @DestinationCategory)
	begin
		delete
		from Playlist_Content_PartnerMap
		where	PartnerID = @partnerid
		and		PlaylistID = @playlistid
		and		SourceCategory = @SourceCategory
		and		DestinationCategory = @DestinationCategory
	end
		

	-- declare @partnerid int = 1461, @playlistid int = 12183
	select pm.Playlist_Content_PartnerMapID MapTableID,  p.PartnerID, p.Name Partner, case when dc.Category is null then pm.SourceCategory else 'All Content' end as SourceCategory, pl.PlaylistID, pl.Name Playlist, pm.MaxVideoCount--, ap.Active
	-- select *
	from Playlist_Content_PartnerMap pm
	join Partner p
	on	pm.PartnerID = p.PartnerID
--	join Playlist_Content_AutoPopulate ap
--	on	p.PartnerID = ap.PartnerID
	join Playlist pl
	on	pm.PlaylistID = pl.PlaylistID
	left join Playlist_Content_PartnerMapDefaultCategory dc
	on	pm.PartnerID = dc.partnerid
	and	 pm.SourceCategory = dc.Category
	where pm.PartnerID = @partnerid

	set nocount off

END
GO
