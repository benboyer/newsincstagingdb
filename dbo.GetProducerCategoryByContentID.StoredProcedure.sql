USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetProducerCategoryByContentID]
	--declare
	@contentid bigint
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'GetProducerCategoryByContentID'

	select top 1 c.ContentID, p.name ProducerName, isnull(replace(nc.NDNCategory, 'NDN Viral Network', 'News Distribution Network, Inc.'), 'news') ProducerCategory
	from	(select * from Content (nolock) where contentid = @contentid) c
	join	Partner p
	on		c.PartnerID = p.PartnerID
	left join	Playlist_Content_PartnerMap pm
	on		c.PartnerID = pm.PartnerID
	left join	NDNCategory nc
	on		pm.NDNCategoryID = nc.NDNCategoryID
	where c.Active = 1
	-- and		pm.FeedID is not null
	-- and		c.ContentID = @contentid
	order by isnull(pm.NDNCategoryID, 0) desc

	set nocount off
END
GO
