USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OwnershipGroup](
	[OwnershipGroupID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[OwnershipGroup] [varchar](100) NULL,
	[AdUnit] [varchar](200) NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[UpdatedBy] [varchar](20) NOT NULL,
 CONSTRAINT [PK__Ownershi__C6A0DEEF0BCDA75D] PRIMARY KEY CLUSTERED 
(
	[OwnershipGroupID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[OwnershipGroup] ADD  CONSTRAINT [DF_OwnershipGroup_UpdatedDate]  DEFAULT (getdate()) FOR [UpdatedDate]
GO
