USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [deploy].[ServerLogin_create]
	@username varchar(20),
	@password varchar(20),
	@DefaultDB varchar(20),
	@schema varchar(20) = null,
	@runasUser varchar(20) = null,
	@runasPass varchar(20) = null
as
BEGIN
	if @runasUser is null set @runasUser = (select SUSER_NAME())

	Declare @sql varchar(max) 

	if not exists (select 1 from sys.sql_logins where name = @username)
	begin
		set @sql = 'CREATE LOGIN ' + @username + ' WITH PASSWORD = ' + '''' + @password + '''' + ', DEFAULT_DATABASE=' + @DefaultDB + ', DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF'
		select @sql
		exec (@sql)

		set @sql = 'CREATE USER ' + @username + ' FOR LOGIN ' + @username
		select @sql
		exec (@sql)
	end

	if not exists(select 1 from sys.schemas where name = @schema) 
	begin
		set @sql = 'CREATE SCHEMA ' + @schema  + ' AUTHORIZATION ' + @username
		exec (@sql)
	end


	if exists (select	1
			from	distribution.dbo.MSdistribution_agents a
			join	master.sys.servers s      
			on		a.subscriber_id = s.server_id
			where a.publisher_db = (select DB_NAME()) and a.publication like 'Player%')
	BEGIN
		declare @counter int
		declare @servers table(id int, servername varchar(40))
		insert into @servers(id, servername)
		select a.id, s.name
		from distribution.dbo.MSdistribution_agents a, master.sys.servers s      
		where a.publisher_db = (select DB_NAME()) and a.publication like 'Player%'-- and s.name not in (''IP-0A08A360'')      
		and a.subscriber_id = s.server_id      
		order by a.id
		while (select COUNT(*) from @servers) > 0
		begin
			set @counter = (select MIN(id) from @servers)
			set @sql = 'sqlcmd -S ' + (select servername from @servers where id = @counter) + ' -U ' + @runasUser + ' -P ' + @runasPass + ' ' + @sql
			select @sql
			delete from @servers where id = @counter
		end
	END
END
GO
