USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[RefTableInserts]
(
--	declare 
	@refnumber bigint,			-- ContentID
	@inputstring varchar(max),	-- Category/Keyword
	@params varchar(200) = null -- execution flag
)
as
BEGIN
	set FMTONLY OFF
	set NOCOUNT ON
	-- Change 2/14-02-26, DBREQUEST-1345 (added KeywordsToCategory section)

	exec SprocTrackingUpdate 'RefTableInserts'
	if @params = 'FDMAItoDMAInclude'
	begin
		if exists (select 1
				from	FeedContent
				where	feedcontentid = @refnumber
				and		contentid is not null
				and		importcomplete = 1)
		begin
				insert into DMAInclude(ContentID, DMACode)
				select	a.ContentID, value 
				from	dbo.fn_Rows_fmCommaSeparated(@inputstring),
						FeedContent a
				where	a.FeedContentID = @refnumber
				and	not exists 
						(select 1
						from	DMAInclude r
						where	r.ContentID = a.ContentID
						and		r.DMACode in (select * from dbo.fn_Rows_fmCommaSeparated(@inputstring)))
		end 
	END
	if @params = 'CIDtoDMAInclude'
	begin
		-- select 'hi'
		insert into DMAInclude(ContentID, DMACode)
		select	@refnumber, value from dbo.fn_Rows_fmCommaSeparated(@inputstring)
		where	not exists 
				(select 1
				from	DMAInclude r
				where	r.ContentID = @refnumber
				and		r.DMACode in (select * from dbo.fn_Rows_fmCommaSeparated(@inputstring)))
	end
	set NOCOUNT OFF
	if @params = 'KeywordsToCategory'
	begin
		update Content set Category = @inputstring
		where ContentID = @refnumber
		and	(Category is null or Category <> @inputstring)
	end

END
GO
