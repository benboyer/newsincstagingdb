USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[AllowContent_Delete]
	@ContentProviderPartnerID int,
	@DistributorPartnerID int
AS
Begin
	set FMTONLY OFF
	SET NOCOUNT ON
	exec SprocTrackingUpdate 'AllowContent_Delete'

	DELETE from AllowContent
	WHERE
		ContentProviderPartnerID = @ContentProviderPartnerID
		and DistributorPartnerID = @DistributorPartnerID

	SET NOCOUNT OFF
End
GO
