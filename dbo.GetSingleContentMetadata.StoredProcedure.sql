USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetSingleContentMetadata]
--  declare
	@ContentID bigint,
	@trackingGroup int = null,
	@devicetype int = 1
AS
	set FMTONLY OFF
	set nocount on
	exec SprocTrackingUpdate 'GetSingleContentMetadata'

	-- declare @ContentID bigint = 23523610, @trackinggroup int = 10557, @devicetype int = 1 -- exec GetWidgetPlaylistVideos @widgetid, @trackinggroup, @devicetype
	Declare	@now datetime,
			@iContentID bigint,
			@itrackinggroup int,
			@idevicetype int
	set		@now = GETDATE()
	select	@iContentID = @ContentID, @itrackinggroup = @trackingGroup, @idevicetype = @devicetype


	select  distinct -- a.iscontentprivate, a.partnerid OwningPartnerid, pd.partnerid DistribPartnerid,
				--a.PlaylistID,
				convert(int, 9999999) as PlaylistID,
				a.ContentID, a.Name, a.Description, a.TrackingGroup ContentPartnerID, a.VideoGroupName, a.Duration, convert(date, a.PubDate) PubDate, a.Keyword, convert(varchar(120), null) Timeline,
				case	when a.AssetTypeName like 'Video%' then 'src'
						--when a.AssetTypeName like 'Thumb%' then 'thumbnail'
						--when a.AssetTypeName like 'Still%' then 'stillFrame'
						-- when a.AssetTypeName in ('ExternalContentID', 'ExternalImageURL') then a.AssetTypeName
						else a.AssetTypeName  -- 'unk'
				end as AssetType,
				case when a.AssetTypeName like 'External%' then a.Filename
				else
					a.DeliveryPath
				end as AssetLocation,
				a.ProducerName, a.ProducerNameAlt, isnull(a.logourl, 'http://assets.newsinc.com/newsinconebyone.png') ProducerLogo, convert(int, 0) as ContentOrder, a.ProducerCategory

	from
		(select	pt.partnerid, pt.TrackingGroup, pt.isContentPrivate, c.ContentID, c.Name,
				c.Description, c.EffectiveDate PubDate, null as [order], aa.FilePath, aa.Filename, aa.Duration, c.Keyword,
				--VG.VideoGroupName,
				null as VideoGroupName, at.Name AssetTypeName, mt.FileExtension, pt.logourl, pt.Name ProducerName, pt.ShortName ProducerNameAlt, isnull(replace(nc.NDNCategory, 'NDN Viral Network', 'News Distribution Network, Inc.'), 'News') ProducerCategory,
				dbo.GetAssetDeliveryPath(mt.FileExtension, c.partnerid, ca.contentid, ca.assetid) DeliveryPath
		from	(select * from Content (nolock) where contentid = @iContentID) c
		join	Partner_TargetPlatformProfile ptp (nolock)
		on		c.PartnerID = ptp.PartnerID
		and		@idevicetype = ptp.TargetPlatformID
		join	TargetPlatform_AssetType tpat (nolock)
		on		ptp.TargetPlatformID = tpat.TargetPlatformID

		join	Content_Asset ca (nolock)
		on		c.ContentID = ca.ContentID
		join	AssetType at (nolock)
		on		tpat.AssetTypeID = at.AssetTypeID
		and		ca.AssetTypeID = at.AssetTypeID
		join	Asset aa (nolock)
		on		ca.AssetID = aa.AssetID

		join	MimeType mt (nolock)
		on		at.MimeTypeid = mt.MimeTypeID
		JOIN	partner pt (nolock)
		ON		c.PartnerID = pt.PartnerID
		left join	Playlist_Content_PartnerMap pm (nolock)
		on			c.PartnerID = pm.PartnerID
		and		c.FeedID = pm.FeedID
		left join	NDNCategory nc (nolock)
		on			pm.NDNCategoryID = nc.NDNCategoryID

		where	ca.active = 1
		and		aa.Active = 1
		) a
		where		dbo.AllowedContentID(a.contentid, @itrackinggroup, @devicetype) = 1
		order by case	when a.AssetTypeName like 'Video%' then 'src'
						else a.AssetTypeName
				end
set nocount off
GO
