USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlayerType](
	[PlayerTypeID] [smallint] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](200) NOT NULL,
	[Order] [smallint] NOT NULL,
	[ConfigurationFileName] [nvarchar](100) NULL,
	[Height] [int] NULL,
	[Width] [int] NULL,
	[CreatedDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_PlayerType] PRIMARY KEY CLUSTERED 
(
	[PlayerTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'Notes', @value=N'ConfigurationFileName may not be relevant if the way our Flash players work ever changes.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PlayerType'
GO
ALTER TABLE [dbo].[PlayerType] ADD  CONSTRAINT [DF_PlayerType_CreatedDate]  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
