USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Organizations_UPDATE_sp]
	@OrganizationID int,
	@ContentItemId int = null,
	@Name varchar(50),
	@Acronym nvarchar(50) = null,
	@CandidateOfficePlaceholder nvarchar(100) = null,
	@AllowUploadingVideo bit,
	@UserID int,
	@LogoURL nvarchar(1000) = null,
	@IsMediaSource bit,
	@IsContentPrivate bit,
	@OrganizationType int,
	@Phone nchar(10) = null

AS
BEGIN
	set fmtonly off
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'Organizations_UPDATE_sp'

	Update [Partner]
	Set Name = @Name,
		isContentPrivate = @IsContentPrivate,
		logourl = case when @LogoURL like 'http://%' then @LogoURL
				else 'http://assets.newsinc.com/' + @LogoURL
			end
		-- LogoURL = @LogoURL
	where PartnerID = @OrganizationID

	if ((@ismediasource = 0 or @OrganizationType <> 3) and (select COUNT(*) from Partner_PartnerType where PartnerID = @OrganizationID and PartnerTypeID = 3) = 1)
	begin
			delete from Partner_PartnerType where PartnerID = @OrganizationID and PartnerTypeID = 3
	end

	if ((@ismediasource = 1 or @OrganizationType = 3) and (select COUNT(*) from Partner_PartnerType where PartnerID = @OrganizationID and PartnerTypeID = 3) = 0)
	begin
		insert into Partner_PartnerType(PartnerID, PartnerTypeID)
		select @OrganizationID, 3
	end

	Update OrganizationsLEGACY
	Set ContentItemId = @ContentItemId,
	Phone = @Phone,
	Acronym = @Acronym,
	CandidateOfficePlaceholder= @CandidateOfficePlaceholder,
	LogoUrl = @LogoURL,
	IsContentPrivate = @IsContentPrivate,
	IsMediaSource = @IsMediaSource
    Where PartnerID = @OrganizationID

	set NOCOUNT OFF
END
GO
