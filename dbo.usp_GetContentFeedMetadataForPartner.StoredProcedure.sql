USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetContentFeedMetadataForPartner]
	-- Add the parameters for the stored procedure here
	@FeedToken uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
exec SprocTrackingUpdate 'usp_GetContentFeedMetadataForPartner'

	DECLARE @Metadata Table
	(
		SiteSection nvarchar(50),
		TrackingGroup int
	)

	DECLARE @SiteSection nvarchar(50)
	DECLARE @PartnerID int = 0
	DECLARE @TrackingGroup int

--	IF @FeedToken = '47C9ECC1-E1B7-4258-A204-E15C7AD9C32A'
--  BEGIN

--		SELECT @PartnerID = 538
--
--    END
--    ELSE
--    BEGIN
		--To be used once Partner table changes have been implemented...
		SET @PartnerID = (select isnull(PartnerID, 0)
		FROM Partner Where Partner.FeedGUID = @FeedToken AND
		Partner.isFeedGUIDActive = 1)
 --   END
	if @PartnerID is null set @PartnerID = 0

    IF @PartnerID <> 0
		BEGIN

			SELECT @SiteSection = [Name]
			FROM Section
			WHERE Section.PartnerID = @PartnerID AND Section.[Default] = 1

			SELECT @TrackingGroup = Partner.TrackingGroup
			FROM Partner
			WHERE Partner.PartnerID = @PartnerID

		END
    ELSE
		BEGIN

			Select @SiteSection = 'ndn'
			Select @TrackingGroup = 10557

		END

	Select @SiteSection as SiteSectionName, @TrackingGroup as TrackingGroup

END
GO
