USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Content_ExpireDelete]
as
BEGIN
	declare @now datetime = (select getdate())
	-- select @now
	declare @Inactivatedate datetime,
			@pid int
	declare @partnerlist as table(PartnerID int, deletedate datetime, expireit bit, deleteit bit)

---------------------------------------------------------------------------------------------------------
-- First section expires all partner's content based on dates.  Second section deals with individual content
--	records with expiration dataes

	-- select * from @partnerlist
	-- Global expire 'true' and partner not 'false'
	if (select COUNT(*) from vw_sysDefaultSettings where Entity = 'ExpireContentAutomatically' and Value = 'true') > 0
	begin
		set @Inactivatedate = (select dateadd(dd, (select -(convert(int,Value)) from vw_sysDefaultSettings where Entity = 'ExpireContentAfter_Days' and Setting = 'Number'), @now))

		insert into @partnerlist(PartnerID,deletedate, expireit, deleteit)
		select partnerid, max(createddate), 1, case when z.value = 'true' then 1 else 0 end
		from	Content,
		(select * from vw_sysDefaultSettings where Entity = 'DeleteContentWhenExpired' and Setting = 'String' and Value = 'true') z
		where	CreatedDate <= @Inactivatedate and active = 1
		and		PartnerID not in (select PartnerID
								from vw_partnerDefaultSettings
								where Entity = 'ExpireContentAutomatically' and Value = 'false')
		group by partnerid, z.Value
		having not exists (select 1
						from @partnerlist pl
						where pl.PartnerID = content.PartnerID)
	end

	--select * from @partnerlist
	-- Partner expire 'true'
	if (select COUNT(*) from vw_sysDefaultSettings where Entity = 'ExpireContentAutomatically' and Value = 'true') = 0
	begin
			-- declare @now datetime = (select getdate()) select @now declare @partnerlist as table(PartnerID int, deletedate datetime, expireit bit, deleteit bit)
		insert into @partnerlist(PartnerID,deletedate, expireit, deleteit)
			-- declare @now datetime = (select getdate()) select @now declare @partnerlist as table(PartnerID int, deletedate datetime, expireit bit, deleteit bit)
		select distinct a.PartnerID, DATEADD(dd, -convert(int,v.value), @now) ExpiresDate, 1, case when z.value = 'true' then 1 else 0 end  -- select *
		from	(select partnerid from vw_partnerDefaultSettings where Entity = 'ExpireContentAutomatically' and Value = 'true') a
		join	(select * from vw_DefaultSettings where Entity = 'ExpireContentAfter_Days') v
		on		a.partnerid = v.PartnerID
		join	Content c
		on		a.partnerid = c.partnerid
		join (select * from vw_partnerDefaultSettings where Entity = 'DeleteContentWhenExpired' and Setting = 'String' and Value = 'true') z
		on		c.PartnerID = z.PartnerID
		where	c.CreatedDate <= DATEADD(dd, -convert(int,v.value), @now)
		and		c.Active = 1
		and not exists
			(select 1
			from @partnerlist pl
			where pl.PartnerID = a.PartnerID)
		-- select * from @partnerlist
	end

	--	select * from @partnerlist

	while (select COUNT(*) from @partnerlist) > 0
	begin
			-- declare @pid int, @now datetime = (select getdate()) select @now declare @partnerlist as table(PartnerID int, deletedate datetime, expireit bit, deleteit bit)
		set @pid = (select MIN(partnerid) from @partnerlist)

		select	a.partnerid, COUNT(*) PlaylistVideosToDelete
		-- delete pc
		from	@partnerlist a
		join	Content c
		on		a.PartnerID = c.PartnerID
		join	PlaylistVideos pc
		on		c.ContentID = pc.VideoID
		where	a.PartnerID = @pid
		and c.CreatedDate < a.deletedate
		group by a.PartnerID

		select	a.partnerid, COUNT(*) ContentPlaylistToDelete
		-- delete pc
		from	@partnerlist a
		join	Content c
		on		a.PartnerID = c.PartnerID
		join	Playlist_Content pc
		on		c.ContentID = pc.ContentID
		where	a.PartnerID = @pid
		and c.CreatedDate < a.deletedate
		group by a.PartnerID

		select a.partnerid, COUNT(*) VidsLegToDel
		-- update v set v.isdeleted = 1
		from	@partnerlist a
		join	Content c

	on		a.PartnerID = c.PartnerID
		join	VideosLEGACY v
		on		c.ContentID = v.contentid
		where	a.PartnerID = @pid
		and		a.deleteit = 1
		and c.CreatedDate < a.deletedate
		group by a.PartnerID

		select	a.partnerid, COUNT(*) ContentToDeact
		-- update c set c.Active = 0, UpdatedUserID = 7534, UpdatedDate = getdate()
		from	@partnerlist a
		join	Content c
		on		a.PartnerID = c.PartnerID
		where	a.PartnerID = @pid
		and c.CreatedDate < a.deletedate
		group by a.PartnerID

		select	a.partnerid, COUNT(*) ContentToDel
		-- update c set c.isdeleted = 1, UpdatedUserID = 7534, UpdatedDate = getdate()
		from	@partnerlist a
		join	Content c
		on		a.PartnerID = c.PartnerID
		where	a.PartnerID = @pid
		and		a.deleteit = 1
		and c.CreatedDate < a.deletedate
		group by a.PartnerID

		delete from @partnerlist
		where PartnerID = @pid
	end
---------------------------------------------------------------------------------------
-- This section deals with individual content records with expiration dates

	-- declare @now datetime = getdate(), @Inactivatedate datetime, @pid int declare @partnerlist as table(PartnerID int, deletedate datetime, expireit bit, deleteit bit)
	insert into @partnerlist(PartnerID,deletedate, expireit, deleteit)
			-- declare @now datetime = (select getdate()) select @now
	-- declare @now datetime = getdate(), @Inactivatedate datetime, @pid int declare @partnerlist as table(PartnerID int, deletedate datetime, expireit bit, deleteit bit)
	select c.partnerid, max(createddate), 1,
			case	when isnull(y.Value, 'false') = 'false' and z.Value = 'true' then 1
					when y.Value = 'true' and isnull(z.Value, 'false') = 'false' then 1
			else 0
			 end
	from Content c
	left join	(select * from vw_partnerDefaultSettings where Entity = 'DeleteContentWhenExpired' and Setting = 'String') z
	on		c.PartnerID = z.PartnerID,
		(select * from vw_sysDefaultSettings where Entity = 'DeleteContentWhenExpired' and Setting = 'String') y
	where ExpirationDate <= @now and active = 1
	group by c.PartnerID, y.Value, z.Value
		having not exists (select 1
						from @partnerlist pl
						where pl.PartnerID = c.PartnerID)

-- select * from @partnerlist

	while (select COUNT(*) from @partnerlist) > 0
	begin
		set @pid = (select MIN(partnerid) from @partnerlist)

		select	a.partnerid, COUNT(*) PlaylistVideosToDelete
		-- delete pc
		from	@partnerlist a
		join	Content c
		on		a.PartnerID = c.PartnerID
		join	PlaylistVideos pc
		on		c.ContentID = pc.VideoID
		where	a.PartnerID = @pid
		and c.CreatedDate < a.deletedate
		group by a.PartnerID

		select	a.partnerid, COUNT(*) ContentPlaylistToDelete
		-- delete pc
		from	@partnerlist a
		join	Content c
		on		a.PartnerID = c.PartnerID
		join	Playlist_Content pc
		on		c.ContentID = pc.ContentID
		where	a.PartnerID = @pid
		and c.CreatedDate < a.deletedate
		group by a.PartnerID

		select	a.partnerid, COUNT(*) VidsLegToDel
		-- update v set v.isdeleted = 1
		from	@partnerlist a
		join	Content c
		on		a.PartnerID = c.PartnerID
		join	VideosLEGACY v
		on		c.ContentID = v.contentid
		where	a.PartnerID = @pid
		and		a.deleteit = 1
		and		c.CreatedDate <= a.deletedate
		and		c.ExpirationDate <= @now
		group by a.PartnerID

		select	a.partnerid, COUNT(*) ContentToDeact
		-- update c set c.Active = 0, UpdatedUserID = 7534, UpdatedDate = getdate()
		from	@partnerlist a
		join	Content c
		on		a.PartnerID = c.PartnerID
		where	a.PartnerID = @pid
		and		c.CreatedDate <= a.deletedate
		and		c.ExpirationDate <= @now
		group by a.PartnerID

		select	a.partnerid, COUNT(*) ContentToDel
		-- update c set c.isdeleted = 1, UpdatedUserID = 7534, UpdatedDate = getdate()
		from	@partnerlist a
		join	Content c
		on		a.PartnerID = c.PartnerID
		where	a.PartnerID = @pid
		and		a.deleteit = 1
		and		c.CreatedDate <= a.deletedate
		and		c.ExpirationDate <= @now
		group by a.PartnerID

		delete from @partnerlist
		where PartnerID = @pid
	end
END
-------------------------------------------------------------------
GO
