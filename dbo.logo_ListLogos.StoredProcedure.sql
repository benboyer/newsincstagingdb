USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[logo_ListLogos]
	-- declare @partnerid int = 127, @logotypeid int -- = 2
	@partnerid int,
	@logotypeid int = null,
	@TrackingGroup int = null
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'logo_ListLogos'
	if @partnerid is null
		set @partnerid = (select partnerid from mTrackingGroup where trackinggroup = @TrackingGroup)

	select	p.PartnerID, p.Name, p.TrackingGroup, lo.LogoID, lt.Name LogoType, lt.Description, lo.LogoURL as 'LogoURL/Image', lo.PartnerDefault, lo.Active, COUNT(distinct pl.launcherid) LaunchersAssigned
	from	partner p
	join	Partner_Logo pl
	on		p.PartnerID = pl.PartnerID
	join	Logo lo
	on		pl.LogoID = lo.LogoID
	join	LogoType lt
	on		lo.LogoTypeID = lt.LogoTypeID
	where	p.PartnerID = @PartnerID
	and		(lo.LogoTypeID = @logotypeid or @logotypeid is null)
	group by p.PartnerID, p.Name, p.TrackingGroup, lo.LogoTypeID, lo.LogoID, lt.Name, lt.Description, lo.LogoURL, lo.PartnerDefault, lo.Active
	order by p.partnerid, lo.LogoTypeID, lo.logoid
	set nocount off
END
GO
