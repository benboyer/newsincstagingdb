USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[NDNAddresses_UPDATE_sp]
	@AddressID int,
	@Address1 nvarchar(100) = null,
	@Address2 nvarchar(100) = null,
	@City nvarchar(100) = null,
	@Zip nvarchar(10) = null,
	@CountryID int,
	@State nvarchar(2) = null,
	@Phone nvarchar(50) = null

AS
BEGIN
	SET NOCOUNT ON;
	exec SprocTrackingUpdate 'NDNAddresses_UPDATE_sp'
    Update Address Set CountryID = @CountryID, Address1 = @Address1, Address2 = @Address2, City = @City, Zip = @Zip,
    AddressTypeID = 1, IsVerified = 1, IsPrimary = 1
    WHERE AddressID = @AddressID

    UPDATE NDNAddressesLegacy Set Phone = @Phone, [State] = @State
    Where AddressID = @AddressID

END
GO
