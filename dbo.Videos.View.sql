USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Videos]
WITH SCHEMABINDING
AS
SELECT     dbo.[Content].ContentID AS VideoID, dbo.VideosLEGACY.ContentItemID, dbo.ContentVideo.FileName AS ClipURL, dbo.[Content].Name AS Title,
                      dbo.[Content].Description, dbo.[Content].EffectiveDate AS EventDate, dbo.VideosLEGACY.NumberOfReviews, dbo.ContentVideo.Height, dbo.ContentVideo.Width,
                      dbo.ContentVideo.Duration, dbo.[Content].CreatedDate AS CreatedOn, dbo.VideosLEGACY.ClipPopularity, dbo.VideosLEGACY.CandidateOfInterest,
                      dbo.ContentVideo.ThumbnailFileName AS ThumbnailURL, dbo.[Content].EffectiveDate AS PublishDate, dbo.[Content].Keyword AS Keywords,
                      dbo.VideosLEGACY.NumberOfPlays, dbo.VideosLEGACY.SentToFreeWheel, dbo.[Content].Active AS IsEnabled, dbo.VideosLEGACY.IsDeleted,
                      dbo.[Content].ExpirationDate AS ExpiryDate, dbo.ContentVideo.ImportFilePath AS OriginalName, dbo.ContentVideo.StillframeFileName AS StillFrameURL,
                      dbo.VideosLEGACY.FileExtension, dbo.[Content].Category, dbo.[Content].CreatedDate AS EnterDTM, dbo.[Content].UpdatedDate AS LastChgDTM,
                      dbo.VideosLEGACY.IngestionStatusMessage, ISNULL(dbo.VideosLEGACY.IsAdvertisement, 0) AS IsAdvertisement, CONVERT(int, 3) AS UserType,
                      dbo.[Content].CreatedUserID AS UserID, dbo.[Content].IsFileArchived, dbo.[Content].OnlyOwnerView
FROM         dbo.VideosLEGACY INNER JOIN
                      dbo.[Content] INNER JOIN
                      dbo.ContentVideo ON dbo.[Content].ContentID = dbo.ContentVideo.ContentID ON dbo.VideosLEGACY.ContentID = dbo.ContentVideo.ContentID
GO
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET NUMERIC_ROUNDABORT OFF
GO
CREATE UNIQUE CLUSTERED INDEX [NDX_VIDEOSVID] ON [dbo].[Videos] 
(
	[VideoID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = ON, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
