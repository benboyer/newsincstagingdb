USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetLauncher_PlaylistAssigned]
	-- declare
		@LauncherID int
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'GetLauncher_PlaylistAssigned'

	select lp.PlaylistID, pl.name, lp.[Order]
	from Launcher_Playlist lp
	join	playlist pl
	on		lp.PlaylistID = pl.PlaylistID
	where LauncherID = @LauncherID
	order by [order]
	
	set nocount off
END
GO
