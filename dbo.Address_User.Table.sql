USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address_User](
	[UserID] [int] NOT NULL,
	[AddressID] [int] NOT NULL,
 CONSTRAINT [PK_AddressUser] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[AddressID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Address_User]  WITH CHECK ADD  CONSTRAINT [FK_AddressUser_Address] FOREIGN KEY([AddressID])
REFERENCES [dbo].[Address] ([AddressID])
GO
ALTER TABLE [dbo].[Address_User] CHECK CONSTRAINT [FK_AddressUser_Address]
GO
ALTER TABLE [dbo].[Address_User]  WITH CHECK ADD  CONSTRAINT [FK_AddressUser_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Address_User] CHECK CONSTRAINT [FK_AddressUser_User]
GO
