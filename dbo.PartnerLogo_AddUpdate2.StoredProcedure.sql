USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[PartnerLogo_AddUpdate2]
	-- declare
	@LogoTypeID int,
	@LogoURLpath varchar(max) = null,
	@LogoURLfilename varchar(max) = null,
	@PartnerID int = null,
	@LauncherID int = null,
	@logoID int = null,
	@Active bit = 1,
	@default bit = 1,
	@PlayerTypesID int = null
as
BEGIN
--	set @logoID =  9810
--	set @LauncherID = 9250
--	--set @PartnerID = 127
--	--set	@LogoURLpath = 'http://widget.newsinc.com/partnerlogo'
--	--set @LogoURLfilename = 'ChicagoSunTimes600x50.jpg''
--	set @LogoTypeID = 2
--	set @Active = 1
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'PartnerLogo_AddUpdate2'
	
	set @LogoURLfilename = LTRIM(rtrim(@logoURLfilename))
	set @LogoURLpath = LTRIM(rtrim(@LogoURLpath))
	
	if @LogoURLfilename = '' set @LogoURLfilename = null
	if @LogoURLpath = '' set @LogoURLpath = null
	
	if (@PartnerID is null and @LauncherID is null)
	begin
		select 'Get real.  Gimme a partnerid or launcherid, please' as MessageBack
		return
	end

--	select @PartnerID, @LogoURLpath, @LogoURLfilename, @LogoTypeID, @logoID, @Active
		if @logoID is not null -- assume it is an update to an existing logo 
		begin
			update Logo set Active = @Active where LogoID = @logoID			-- active flag
			if @LogoURLpath is not null and @LogoURLfilename is not null and @LogoURLpath + '/' +  @LogoURLfilename <> (select logourl from Logo where LogoID = @logoid)
			begin
				-- (file replacement)
				update Logo set LogoURL = @LogoURLpath + '/' + @LogoURLfilename where LogoID = @logoID	and LogoURL <> @LogoURLpath + '/' + @LogoURLfilename -- Logo table
				
				if @Active = 1 and  @LogoTypeID = 5
				begin
					--update Partner and other tables
					update Partner set LogoURL = @LogoURLpath + '/' + @LogoURLfilename where PartnerID = @PartnerID
					update organizationslegacy set LogoURL = @LogoURLfilename where PartnerID = @Partnerid
				end
			end
			if @Active = 1 and  @LogoTypeID <> 5
			begin 
				if @PartnerID is null 
					set @PartnerID = (select partnerid from Launcher where LauncherID = @LauncherID)
				insert into Partner_Logo(PartnerID, LogoID, LauncherID)
				select	@PartnerID, @logoID, @LauncherID
				where not exists (select 1 from Partner_Logo where PartnerID = @PartnerID and LauncherID = @LauncherID and LogoID = @logoID)
			end
			exec dbo.usp_ListPartnerLogos_pretty @partnerid

		end
			-- select * from LogoType		
			--select @PartnerID, @LogoURLpath, @LogoURLfilename, @LogoTypeID, @logoID, @Active
		
		if @logoID is null	-- assume it is a new logo
		begin
			insert into Logo(LogoTypeID, LogoURL, Active, PartnerDefault, PlayerTypesID)
			select @LogoTypeID, @LogoURLpath + '/' + @LogoURLfilename, @Active, @default, @PlayerTypesID
			where not exists 
					(select 1 
					from Logo 
					where LogoTypeID = @logotypeid 
					and LogoURL = @LogoURLpath + '/' + @LogoURLfilename
					and	isnull(PlayerTypesID, 0) = isnull(@PlayerTypesID, 0))
			select @logoID = logoid from Logo where LogoURL = @LogoURLpath + '/' + @LogoURLfilename
	--select @PartnerID, @LogoURLpath, @LogoURLfilename, @LogoTypeID, @logoID, @Active
	--end
	--go
	--	declare @LauncherID int, @PartnerID int = 704, @LogoURLpath varchar(max) = 'http://assets.newsinc.com', @LogoURLfilename varchar(max) = 'CSN_bay.png', @LogoTypeID int = 5, @logoID int = 9765, @Active bit = 1
	--select @PartnerID, @LogoURLpath, @LogoURLfilename, @LogoTypeID, @logoID, @Active
	--		select * from Partner_Logo pl join logo lo on pl.logoid= lo.logoid where PartnerID = @PartnerID and LogoTypeID = @LogoTypeID

			if @Active = 1 and  @LogoTypeID = 5
			begin -- Not a dup from above, this handls new provider logos and partnerid must be provided as argument (above handles updated logos)
				update Partner set LogoURL = @LogoURLpath + '/' + @LogoURLfilename where PartnerID = @PartnerID
				update organizationslegacy set LogoURL = @LogoURLfilename where PartnerID = @Partnerid
			end

			if @PartnerID is null -- this handles new logos being assigned to a launcher, and we can presume the launcher owner is the partner to be updated (if partnerid is passed in, a 'foreign' partner may be using the launcher
				set @PartnerID = (select partnerid from Launcher where LauncherID = @LauncherID)		
			insert into Partner_Logo(PartnerID, LauncherID, LogoID)
				select @PartnerID, @LauncherID, @logoID
				where not exists (select 1 from Partner_Logo where PartnerID = @PartnerID and  LogoID = @logoID)

			exec dbo.usp_ListPartnerLogos_pretty @partnerid
		end
	set nocount off
end
GO
