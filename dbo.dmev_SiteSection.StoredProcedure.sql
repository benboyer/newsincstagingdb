USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[dmev_SiteSection]
	@partnerid int
as
begin
	select PartnerID, SectionID, CreatedUserID, FullSiteSection, SectionPartnerID, SectionPartnerUnique, DefaultUnique, Section, SubSection, PageType
	from dme_SiteSection (nolock)
	where partnerid = isnull(@partnerid, partnerid)
end
GO
