USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Content_Keyword](
	[ContentID] [int] NOT NULL,
	[KeywordID] [int] NOT NULL,
 CONSTRAINT [PK_Content_Keyword] PRIMARY KEY CLUSTERED 
(
	[ContentID] ASC,
	[KeywordID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Content_Keyword]  WITH CHECK ADD  CONSTRAINT [FK_Content_Keyword_Content] FOREIGN KEY([ContentID])
REFERENCES [dbo].[Content] ([ContentID])
GO
ALTER TABLE [dbo].[Content_Keyword] CHECK CONSTRAINT [FK_Content_Keyword_Content]
GO
ALTER TABLE [dbo].[Content_Keyword]  WITH CHECK ADD  CONSTRAINT [FK_Content_Keyword_Keyword] FOREIGN KEY([KeywordID])
REFERENCES [dbo].[Keyword] ([KeywordID])
GO
ALTER TABLE [dbo].[Content_Keyword] CHECK CONSTRAINT [FK_Content_Keyword_Keyword]
GO
