USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sysCredentials](
	[sysCredentialsID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](40) NULL,
	[Description] [varchar](120) NULL,
	[Value] [varchar](128) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
