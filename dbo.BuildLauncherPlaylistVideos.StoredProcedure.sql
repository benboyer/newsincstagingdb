USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BuildLauncherPlaylistVideos]
--  declare
	@widgetID int = null,
	@playlistID int = null,
	@trackingGroup int = null,
	@contentid bigint = null,
	@devicetype int = 1
AS
BEGIN
	-- rolled back to this 2013-09-19
		set FMTONLY OFF
		set nocount on
	exec SprocTrackingUpdate 'BuildLauncherPlaylistVideos'

	Declare	@now datetime,
			@iwidgetID int,
			@iplaylistID int,
			@itrackinggroup int,
			@idevicetype int
	set		@now = GETDATE()
	select @iwidgetID = isnull(@widgetID, 1), @iplaylistID = isnull(@playlistID, 507), @itrackinggroup = @trackingGroup, @idevicetype = isnull(@devicetype, 1)

	if	@PlaylistID = 9999999
	begin
		exec GetSingleContent @contentid, @iTrackingGroup, @deviceType
		return
	end
	select  distinct -- a.iscontentprivate, a.partnerid OwningPartnerid, pd.partnerid DistribPartnerid,
					a.PlaylistID,
					a.ContentID, a.Name, a.Description, a.TrackingGroup ContentPartnerID, a.VideoGroupName, a.Duration, convert(date, a.PubDate) PubDate, a.Keyword, convert(varchar(120), null) Timeline,
					case	when a.AssetTypeName like 'Video%' then 'src'
							--when a.AssetTypeName like 'Thumb%' then 'thumbnail'
							--when a.AssetTypeName like 'Still%' then 'stillFrame'
							else a.AssetTypeName --'unk'
					end as AssetType,
	--	Change source of file paths here - either build dynamically or use path stored in database
					--- dfp.RootURL + '\' + dfp.DirectoryLevel1 + '\' + CONVERT(varchar(20), a.partnerid) + '\' + convert(varchar(20), a.ContentID) + '\' + a.Filename as AssetLocation,
					case when left(isnull(a.filepath, '') + '/' + a.FileName, 4) <> 'http' then 'http://' + isnull(a.filepath, '') + '/' + a.Filename
						else isnull(a.filepath, '') + '/' + a.Filename
						end as AssetLocation,
						convert(varchar(20), null) as AssetMimeType, convert(int, null) as AssetSortOrder,
						convert(varchar(500), null) as AssetLocation2, convert(varchar(20),null) as AssetMimeType2, convert(int,null) as AssetSortOrder2,
	--					null as AssetLocation2,
	--
					a.ProducerName, null as ProducerNameAlt, a.logourl ProducerLogo, a.[Order]+1 ContentOrder, null ProducerCategory, null SEOFriendlyTitle, null embedCodeAccessible
				-- into drop table #mainset
		from	(select	pt.partnerid, pt.TrackingGroup, pt.isContentPrivate, p.playlistid, pc.ContentID, c.Name,
					c.Description, c.EffectiveDate PubDate, pc.[Order], aa.FilePath, aa.Filename, aa.Duration, c.Keyword,
					VG.VideoGroupName, at.Name AssetTypeName, mt.FileExtension, pt.logourl, pt.Name ProducerName, c.OnlyOwnerView
			from	Playlist p (nolock)
			join	Playlist_Content pc (nolock)
			on		p.PlaylistID = pc.PlaylistID
			join	Content c
			on		pc.ContentID = c.ContentID
			join	Partner_TargetPlatformProfile ptp
			on		c.PartnerID = ptp.PartnerID
			and		@idevicetype = ptp.TargetPlatformID
			join	TargetPlatform_AssetType tpat
			on		ptp.TargetPlatformID = tpat.TargetPlatformID

			join	Content_Asset ca
			on		c.ContentID = ca.ContentID
			join	AssetType at
			on		tpat.AssetTypeID = at.AssetTypeID
			and		ca.AssetTypeID = at.AssetTypeID
			join	Asset aa
			on		ca.AssetID = aa.AssetID

			join	MimeType mt
			on		at.MimeTypeid = mt.MimeTypeID
			JOIN	partner pt (nolock)
			ON		c.PartnerID = pt.PartnerID
			left join	VideoGroups VG
			on		p.PlaylistID = vg.VideoGroupId
			where	ptp.isDistributionEnabled = 1
			and		c.Active = 1
			and		ca.active = 1
			and		aa.Active = 1
			and		p.PlaylistID = @iplaylistID
			and		isnull(aa.isDeleted, 0) = 0
			) a
		-- join	DistributionFilePath dfp
		-- on		a.FileExtension = dfp.MimeFileExtension -- select * from partner where partnerid = -1
			left join partner pd
			on	@itrackinggroup = pd.TrackingGroup
			LEFT JOIN AllowContent ac  (nolock)
			ON		a.PartnerID = ac.ContentProviderPartnerID
			left join DMAExclude dmaEx (nolock)
			on	a.ContentID = dmaEx.contentid
			left join Partner_DMA pdma (nolock)
			on	dmaex.DMACode = pdma.DMACode
			and	pd.PartnerID = pdma.PartnerID
		WHERE dbo.AllowedContentID(a.ContentID, @itrackinggroup, @idevicetype) = 1
		ORDER BY a.[Order]+1
		set nocount off
END
GO
