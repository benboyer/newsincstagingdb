USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_SetAssetActive]
	@assetid bigint
as
BEGIN
	update Asset 
	set Active = 1, 
		ImportStatusID = 5
	where AssetID = @assetid
	select AssetID, ImportStatusID, Active
	from Asset
	where AssetID = @assetid
	 
END
GO
