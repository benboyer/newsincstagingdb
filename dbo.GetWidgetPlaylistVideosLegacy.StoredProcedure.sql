USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetWidgetPlaylistVideosLegacy]
	@widgetID int,
	@playlistID int
AS
	-- rolled back to this on 2013-09-19
	exec SprocTrackingUpdate 'GetWidgetPlaylistVideosLegacy'

	declare @iwidgetid int,
			@iplaylistid int,
			@trackinggroup int
	select @iwidgetid = @widgetID, @iplaylistid = @playlistID
	set @trackinggroup = (select trackinggroup from mTrackingGroup where partnerid = (select partnerid from Playlist where PlaylistID = @iplaylistid))
	SELECT v.VideoID, v.Title
	FROM PlaylistVideos pv
	INNER JOIN Videos v ON v.VideoID = pv.VideoID
	INNER JOIN NDNUsers ndnuser ON ndnuser.UserID = v.UserID
	INNER JOIN Organizations org ON org.OrganizationId = ndnuser.OrganizationID
	-- LEFT JOIN WidgetExcludedOrgs weo ON (weo.WidgetId = @iwidgetid AND weo.OrganizationId = org.OrganizationId)
	WHERE dbo.AllowedLauncherContent(@iwidgetid, @trackinggroup) = 1
	-- and	weo.OrganizationId IS NULL
	AND pv.PlaylistID = @iplaylistid
	AND v.IsEnabled = 1
	AND v.IsDeleted = 0
	ORDER BY pv.SortOrder ASC
GO
