USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ads_BlockingPhrase_list] 
	@id int = null,
	@includeInactive bit = 1
as 
BEGIN
	select PhrasesToFilterAds TriggerPhraseID, replace(Phrase, '%', '') TriggerPhrase, KeywordAddPhrase AdBlockingPhrase, filteractive IsActive, AddUpdateUser LastUpdatedBy, ISNULL(UpdatedDate, FilterAddedDate) LastUpdated
	from	ads_phrasesToBlockAds
	where	PhrasesToFilterAds = @id 
	or		(@id is null and (@includeInactive = 1 
			or		
			filteractive = 1))
	order by phrase
END
GO
