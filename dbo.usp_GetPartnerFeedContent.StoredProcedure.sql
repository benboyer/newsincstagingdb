USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetPartnerFeedContent]  
-- declare  
--@feedtoken uniqueidentifier,  
@feedtoken varchar(255),  
@playlistid int = null,  
@numbertoreturn int = 100,  
@datefrom datetime = null,  
@dateto datetime = null,  
@Updates bit = 0  
  
AS  
BEGIN  

 
--  declare @feedtoken varchar(255),  
-- @playlistid int = null,  
--    @numbertoreturn int = 100,  
--    @datefrom datetime = null,  
--    @dateto datetime = null,  
--    @Updates bit = 0  
    
--set @feedtoken = '40B3DF43-C7C1-41A1-A1F1-AAAD732F88B8'
--set @numbertoreturn = 50
--set @Updates = 0
  
 SET FMTONLY OFF  
    SET NOCOUNT ON;  
 exec SprocTrackingUpdate 'usp_GetPartnerFeedContent'  
  
 if @numbertoreturn = 0  
  set @numbertoreturn = 50  
 if @numbertoreturn > 500  
  set @numbertoreturn = 500  
 if @feedtoken is null  
     set @feedtoken = '00000000-0000-0000-0000-000000000000'  
  
    DECLARE @accesspartnerid int, @landingurl varchar(120), @itrackinggroup int, @now datetime  
  
 if exists (select 1 from Playlist where RSSToken = @feedtoken)  
 begin  
   select @accesspartnerid = PartnerID,  
     @landingurl = LandingURL,  
     @itrackinggroup = trackinggroup  
   -- from Partner where FeedGUID = @feedtoken  
   from Partner where PartnerID = (select partnerid from Playlist where CONVERT(varchar(255), rsstoken) = @feedtoken)  
 end  
  
 if @playlistid is null or exists (select 1 from Partner where FeedGUID = @feedtoken)  
 begin  
   select @accesspartnerid = PartnerID,  
     @landingurl = LandingURL,  
     @itrackinggroup = trackinggroup  
   from Partner where FeedGUID = @feedtoken  
   -- from Partner where PartnerID = (select partnerid from Playlist where CONVERT(varchar(255), rsstoken) = @feedtoken)  
 end  
  select @now = GETDATE()  
  
 if @accesspartnerid is null  
  set @accesspartnerid = 0  
 -- select @accesspartnerid  
  
    SELECT  distinct  
   TOP(@numbertoreturn) c.ContentID, c.ContentTypeID, c.PartnerID, c.ContentSourceID, c.Name, c.Description, c.Category, c.Keyword, c.EffectiveDate, c.ExpirationDate, c.CreatedDate, c.CreatedUserID, c.UpdatedDate, c.UpdatedUserID, c.Active,  
   c.FeedID, partners.PartnerID, partners.ParentPartnerID, partners.Name, partners.ContactID, partners.Website, partners.StatusID, partners.CreatedDate, partners.CreatedDate, partners.CreatedUserID, Partners.UpdatedDate, partners.UpdateUserID, 
   partners.TrackingGroup, partners.ShortName, @landingurl LandingURL, partners.isContentPrivate, partners.ZoneID, partners.FeedGUID, partners.isFeedGUIDActive, c.IsFileArchived,  
   c.ContentID, c.ContentID, 0 NumberOfReviews, a.Duration, 0 as ClipPopularity, 0 as IsAdvertisement, null as CandidateOfInterest, 0 as NumberOfPlays, 1 as SentToFreeWheel, null as FileExtension, null as IngestionStatusMessage, a.IsDeleted,  
   c.PartnerID, null as ContentItemId, null as Phone, pt.PartnerTypeID as OrganizationType, Partners.ShortName, null as CandidateOfficePlaceholder, case when pt.PartnerTypeID IN (2,3) then 1 else 0 end as AllowUploadingVideo, Partners.LogoUrl, 
   case when pt.PartnerTypeID IN (2,3) then 1 else 0 end as IsMediaSource, partners.isContentPrivate,  
   0 as AllowContentID,  
   c.partnerid as  ContentProviderPartnerID,  
   pd.Partnerid as DistributorPartnerID, c.CreatedDate  
   , aa.FilePath + '/' + aa.Filename as TranscriptPath --, pc.PlaylistID  
  
 --select top 100 *  
 FROM Content c (nolock)  
    JOIN Partner AS Partners (nolock)  
    ON  c.PartnerID = Partners.PartnerID  
 left join Playlist_Content pc  
 on  c.contentid = pc.ContentID  
    join Content_Asset ca (nolock)  
    on  c.ContentID = ca.contentid  
    and  1 = ca.TargetPlatformID  
    and  1 = ca.AssetTypeID  
    join Asset a (nolock)  
    on  ca.assetid = a.AssetID  
    left join Asset aa (nolock)  
    on  ca.AssetID = aa.AssetID  
    and  17 = ca.AssetTypeID  
    left join (select max(partnertypeid) PartnerTypeID, partnerid from Partner_PartnerType (nolock) group by partnerid) ppt  
    on  Partners.partnerid = ppt.PartnerID  
    left join PartnerType pt (nolock)  
    on  ppt.PartnerTypeID = pt.PartnerTypeID  
 join partner pd  
 on  c.PartnerID = pd.PartnerID  
 join dbo.allowedcontentbypartnerid(@accesspartnerid) acbp
 on acbp.ContentID = c.ContentID
 WHERE 
 ca.active = 1 and a.active = 1 -- and ca.TargetPlatformID = 1  
 and  
 (pc.PlaylistID = @playlistid or @playlistid is null)  
 and  c.PartnerID = @accesspartnerid  
 and  (@playlistid is null or pc.PlaylistID = @playlistid)  
 and (  
        (@datefrom IS NULL OR case when @updates = 1 then c.UpdatedDate else c.CreatedDate end >= @datefrom)  
        AND (@dateto IS NULL OR case when @updates = 1 then c.updateddate else c.CreatedDate end <= @dateto)  
  )  
  
    ORDER BY c.CreatedDate DESC, c.contentid desc  
 set nocount off  
  
END
GO
