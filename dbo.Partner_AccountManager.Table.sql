USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Partner_AccountManager](
	[PartnerID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
 CONSTRAINT [PK_Partner_AccountManager] PRIMARY KEY NONCLUSTERED 
(
	[PartnerID] ASC,
	[UserID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Partner_AccountManager]  WITH CHECK ADD  CONSTRAINT [FK_Partner_AccountManager_Partner] FOREIGN KEY([PartnerID])
REFERENCES [dbo].[Partner] ([PartnerID])
GO
ALTER TABLE [dbo].[Partner_AccountManager] CHECK CONSTRAINT [FK_Partner_AccountManager_Partner]
GO
ALTER TABLE [dbo].[Partner_AccountManager]  WITH CHECK ADD  CONSTRAINT [FK_Partner_AccountManager_User] FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Partner_AccountManager] CHECK CONSTRAINT [FK_Partner_AccountManager_User]
GO
