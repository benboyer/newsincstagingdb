USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetFeedContentPL_WID]
	-- declare
	@feedtoken uniqueidentifier = null,
    @providerid int = 0,
    @datefrom datetime = null,
    @dateto datetime = null,
    @numbertoreturn int = 100,
    @Updates bit = 0,
    @playlistid int = null,
    @launcherid int = null
as
BEGIN

--	set @launcherid = 2
--	set @feedtoken = '54B6CAE7-E19B-4FF7-AED1-BFAB914EC47E'
-- set @feedtoken = '7B9EF57B-601E-4A98-B171-29E8773C1D90'
--	set @numbertoreturn = 6
--	set @datefrom = '2013-03-15 09:00:00'
--	set @dateto = '2013-03-15 11:00:00'
--	set @updates = 1


	SET FMTONLY OFF
    SET NOCOUNT ON;


	if @datefrom is null and @dateto is not null
		set @datefrom = DATEADD(dd, -30, @dateto)

	-- select @datefrom, @dateto

	exec SprocTrackingUpdate 'usp_GetContentFeedContent'
	if @providerid is null
		set @providerid = 0
	if @numbertoreturn = 0 or @numbertoreturn is null
		set @numbertoreturn = 100
	if @numbertoreturn > 500
		set @numbertoreturn = 500
	if @feedtoken is null
	    set @feedtoken = '00000000-0000-0000-0000-000000000000'

    DECLARE @accesspartnerid int, @landingurl varchar(120), @itrackinggroup int, @now datetime
		select	@accesspartnerid = PartnerID,
				@landingurl = LandingURL,
				@itrackinggroup = trackinggroup
		from Partner where FeedGUID = @feedtoken
	if @accesspartnerid is null
		set @accesspartnerid = 0

	if @playlistid is null and @launcherid is null
		set @playlistid = (select convert(int,value) from vw_sysDefaultSettings where Entity = 'playlist' and Setting = 'Number')

	declare @numbertodouble int = (select @numbertoreturn * 2)
--	select @playlistid, @numbertoreturn, @numbertodouble, @itrackinggroup

	create table #tmpContent(contentid bigint not null)



--	if @Updates = 1
--	begin
		if @playlistid is not null
		begin
			--declare @playlistid int = 507, @numbertodouble int = 12, @itrackinggroup int = 90051
			insert into #tmpContent(contentid)
			select	top (@numbertodouble) pc.ContentID
			from	(select * from playlist_content (nolock) where PlaylistID = @playlistid) pc
			join	Content (nolock) c
			on		pc.ContentID = c.ContentID
			where	dbo.AllowedContentID(c.ContentID, @itrackinggroup, 1) = 1
			order by pc.[Order]
		end

		if @playlistid is null and @launcherid is not null
		begin
			--declare @launcherid int = 2, @playlistid int, @numbertodouble int = 200, @itrackinggroup int = 90051
			if exists (select top 1 * from Launcher_Playlist (nolock) lp join Playlist_Content (nolock) pc on lp.PlaylistID = pc.PlaylistID where lp.LauncherID = @launcherid /*and dbo.AllowedContentID(pc.ContentID, @itrackinggroup, 1) = 1*/)
			begin
				insert into #tmpContent(contentid)
				--declare @launcherid int = 2, @playlistid int, @numbertodouble int = 200, @itrackinggroup int = 90051
				select	top (@numbertodouble) pc.ContentID
				from	(select * from Launcher_Playlist (nolock) where launcherid = @launcherid) lp
				join	playlist_content (nolock) pc
				join	Content (nolock) c
				on		pc.ContentID = c.ContentID
				on		lp.PlaylistID = pc.PlaylistID
				where	dbo.AllowedContentID(pc.ContentID, @itrackinggroup, 1) = 1
				order by lp.[Order], pc.[Order]
			end
			else
			begin
				if @Updates = 0
				begin
					insert into #tmpContent(contentid)
					--declare @launcherid int = 3233, @playlistid int, @numbertoreturn int = 100, @itrackinggroup int = 10557, @datefrom datetime, @dateto datetime
					select	top (@numbertoreturn) pc.ContentID
					from	(select distinct top (@numbertodouble) lp.[Order] LPorder, p.*
							from  (select top (@numbertodouble) * from Launcher_Playlist (nolock) where PlaylistID = (select convert(int,value) from vw_sysDefaultSettings where Entity = 'playlist' and Setting = 'Number')) lp
							join	playlist_content (nolock) p
							on		lp.PlaylistID = p.PlaylistID
							order by lp.[Order], p.[Order]) pc
					join	Content (nolock) c
					on		pc.ContentID = c.ContentID
					where	dbo.AllowedContentID(pc.ContentID, @itrackinggroup, 1) = 1
					and		c.CreatedDate >= isnull(@datefrom, c.CreatedDate)
					and		c.CreatedDate <= ISNULL(@dateto, c.CreatedDate)
					order by pc.LPorder, pc.[Order]
				end
				if @Updates = 1
				begin
					insert into #tmpContent(contentid)
					select	top (@numbertoreturn) pc.ContentID
					from	(select distinct top (@numbertodouble) lp.[Order] LPorder, p.*
							from  (select top (@numbertodouble) * from Launcher_Playlist (nolock) where PlaylistID = (select convert(int,value) from vw_sysDefaultSettings where Entity = 'playlist' and Setting = 'Number')) lp
							join	playlist_content (nolock) p
							on		lp.PlaylistID = p.PlaylistID
							order by lp.[Order], p.[Order]) pc
					join	Content (nolock) c
					on		pc.ContentID = c.ContentID
					where	dbo.AllowedContentID(pc.ContentID, @itrackinggroup, 1) = 1
					and		c.UpdatedDate >= isnull(@datefrom, c.UpdatedDate)
					and		c.UpdatedDate <= ISNULL(@dateto, c.UpdatedDate)
					order by pc.LPorder, pc.[Order]
				end
			end
		end

		select top (@numbertoreturn) -- c.CreatedDate, c.UpdatedDate, *
		c.ContentID, c.ContentTypeID, c.PartnerID, c.ContentSourceID, c.Name, c.Description, c.Category, c.Keyword, c.EffectiveDate, c.ExpirationDate, c.CreatedDate, c.CreatedUserID, c.UpdatedDate, c.UpdatedUserID, c.Active,
				c.FeedID, partners.PartnerID, partners.ParentPartnerID, partners.Name, partners.ContactID, partners.Website, partners.StatusID, partners.CreatedDate, partners.CreatedDate, partners.CreatedUserID, Partners.UpdatedDate, partners.UpdateUserID, partners.TrackingGroup, partners.ShortName, @landingurl LandingURL, partners.isContentPrivate, partners.ZoneID, partners.FeedGUID, partners.isFeedGUIDActive, c.IsFileArchived,
				c.ContentID, c.ContentID, 0 NumberOfReviews, a.Duration, 0 as ClipPopularity, 0 as IsAdvertisement, null as CandidateOfInterest, 0 as NumberOfPlays, 1 as SentToFreeWheel, null as FileExtension, null as IngestionStatusMessage, a.IsDeleted,
				c.PartnerID, null as ContentItemId, null as Phone, pt.PartnerTypeID as OrganizationType, Partners.ShortName, null as CandidateOfficePlaceholder, case when pt.PartnerTypeID IN (2,3) then 1 else 0 end as AllowUploadingVideo, Partners.LogoUrl, case when pt.PartnerTypeID IN (2,3) then 1 else 0 end as IsMediaSource, partners.isContentPrivate,
				-- max(ac.AllowContentID),
				0 as AllowContentID,
				c.partnerid as  ContentProviderPartnerID,
				pd.Partnerid as DistributorPartnerID, c.CreatedDate
				, aa.FilePath + '/' + aa.Filename as TranscriptPath
		from #tmpContent ta
		join Content c
		on	ta.contentid = c.ContentID
		JOIN	Partner AS Partners (nolock)
		ON		c.PartnerID = Partners.PartnerID
		join	Content_Asset ca (nolock)
		on		c.ContentID = ca.contentid
		and		1 = ca.TargetPlatformID
		and		1 = ca.AssetTypeID -- (1 = ca.AssetTypeID or 17 = ca.assettypeid)
		join	Asset a (nolock)
		on		ca.assetid = a.AssetID
		left join Asset aa (nolock)
		on		ca.AssetID = aa.AssetID
		and		ca.AssetTypeID = 17
		left join (select max(partnertypeid) PartnerTypeID, partnerid from Partner_PartnerType (nolock) group by partnerid) ppt
		on		Partners.partnerid = ppt.PartnerID
		left join PartnerType pt (nolock)
		on		ppt.PartnerTypeID = pt.PartnerTypeID
		left	join mTrackingGroup pd -- select top 1 * from AllowContent
		on		@itrackinggroup = pd.TrackingGroup
		ORDER BY ta.contentid

		drop table #tmpContent
		set nocount off

END
GO
