USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[defemb_GetEmbedCodes]
		-- declare
	@partnerid int = null,
	@userid int = null,
	@launcherid int = null,
	@videoid int = null,
	@trackinggroup int = null,
	@playlistid int = null,
	@sitesection varchar(60) = null,
	@height int = null,
	@width int = null,
	@autoplay int = 1,
	@sharing int = 1
	
AS
BEGIN
-- set @partnerid = 446
	-- select * from partner p join playlist pl on p.partnerid = pl.partnerid join playlist_content pc on pl.playlistid = pc.playlistid where p.feedguid = '54B6CAE7-E19B-4FF7-AED1-BFAB914EC47E'
/*
set @partnerid = 446
set @videoid = (select MAX(contentid) from Content (nolock) where Active = 1)
set @playlistid = 10203
set @trackinggroup = 90121
*/
	-- set @feedtoken = '40B3DF43-C7C1-41A1-A1F1-AAAD732F88B8'
	-- set @playlistid = 9491-- 9653  -- 9491 -- 10623
	-- Optimized code, tried and rolled back on 2012-05-29
	--	fixed (exclusions), deployed on 2012-06-11, then rolled back
	-- double-check on exclusions 2012-06-12, ready for QA

	-- set @providerid = 1

	SET FMTONLY OFF
    SET NOCOUNT ON;
	exec SprocTrackingUpdate 'defemb_GetEmbedCodes'

	declare @jsTID int, @ifTID int
	
	if @partnerid = null and @launcherid = null and @userid is null and @videoid = null and @trackinggroup = null and @playlistid = null
	begin
		--select convert(varchar(60), 'You must provide input parameters') as Messageback
		SELECT convert(int, null) as Autoplay, convert(int, null) as Height, convert(int, null) as Width, convert(int, null) as Sharing, convert(varchar(400), null) as IFrame2Embed, convert(varchar(400), null) as jscriptEmbed
		 return
	end
	
	declare @h varchar(10) = '', @w varchar(10) = '', @ssid varchar(120) = ''

	if @partnerid is null and @userid > 0
		set @partnerid = (select partnerid from [user] (nolock) where userid = @userid)	

	if @partnerid is null and @trackinggroup > 0
		set @partnerid = (select partnerid from mTrackingGroup (nolock) where TrackingGroup = @trackinggroup)

	if @partnerid is null and @launcherid > 0
		set @partnerid = (select partnerid from launcher (nolock) where LauncherID = @launcherid)

	if @partnerid is null and @playlistid > 0
		set @partnerid = (select partnerid from playlist (nolock) where PlaylistID = @playlistid)

	if @partnerid is null and @videoid > 0
		set @partnerid = (select partnerid from content (nolock) where ContentID = @videoid)
		
	if @launcherid is null
	set @launcherid = (select isnull((select value from vw_partnerDefaultSettings (nolock) where PartnerID = @PartnerID and entity = 'DefaultSingleEmbed' and Setting = 'LauncherID'), (select value  from vw_sysDefaultSettings where entity = 'DefaultSingleEmbed' and Setting = 'LauncherID')))

	if @playlistid is null
	set @playlistid = (select isnull((select value from vw_partnerDefaultSettings (nolock) where PartnerID = @partnerid and entity = 'DefaultSingleEmbed' and Setting = 'PlaylistID'), (select value  from vw_sysDefaultSettings where entity = 'DefaultSingleEmbed' and Setting = 'PlaylistID')))

	if isnull(@trackinggroup, 0) = 0 or not exists (select 1 from partner (nolock) where TrackingGroup = @trackinggroup) 
	set @trackinggroup = (select trackinggroup from Partner (nolock) where PartnerID = @partnerid)
	
	if isnull(@trackinggroup, 0) = 0 or not exists (select 1 from partner (nolock) where TrackingGroup = @trackinggroup) 
	set @trackinggroup = (select isnull((select value from vw_partnerDefaultSettings (nolock) where PartnerID = @partnerid and entity = 'TrackingGroup' and Setting = 'Number'), (select value  from vw_sysDefaultSettings where entity = 'TrackingGroup' and Setting = 'Number')))

	if @sitesection is null
		set @sitesection = (select isnull((select value from vw_partnerDefaultSettings (nolock) where PartnerID = @partnerid and entity = 'DefaultSingleEmbed' and Setting = 'SiteSection'), (select value  from vw_sysDefaultSettings where entity = 'DefaultSingleEmbed' and Setting = 'SiteSection')))
	

	if @height is not null
		set @h = convert(varchar(20), @height)
	if isnull(@h, '') = '' -- is null
		set @h = (select convert(varchar(20), height) from launcher (nolock) where launcherid = @launcherid)
	if isnull(@h, '') = '' -- is null
		set @h = (select isnull((select value from vw_partnerDefaultSettings (nolock) where PartnerID = @partnerid and entity = 'DefaultSingleEmbed' and Setting = 'Height'), (select value  from vw_sysDefaultSettings where entity = 'DefaultSingleEmbed' and Setting = 'Height')))

	if @width is not null
		set @w = convert(varchar(20), @width)
	if isnull(@w, '') = '' -- is null
		set @w = (select convert(varchar(20), width) from launcher (nolock) where launcherid = @launcherid)
	if isnull(@w, '') = '' -- is null
		set @w = (select isnull((select value from vw_partnerDefaultSettings (nolock) where PartnerID = @partnerid and entity = 'DefaultSingleEmbed' and Setting = 'Width'), (select value  from vw_sysDefaultSettings where entity = 'DefaultSingleEmbed' and Setting = 'Width')))


	--	select @partnerid, @w, @h, @sharing, @videoid

	if @autoplay is null
		set @autoplay = (select isnull((select value from vw_partnerDefaultSettings (nolock) where PartnerID = @partnerid and entity = 'DefaultSingleEmbed' and Setting = 'Autoplay'), (select value  from vw_sysDefaultSettings where entity = 'DefaultSingleEmbed' and Setting = 'Autoplay')))
	if @autoplay is null
		set @autoplay = (select -1 * DisableAutoPlay from Launcher (nolock) where LauncherID = @launcherid)

	if @sharing is null
		set @sharing = (select isnull((select value from vw_partnerDefaultSettings (nolock) where PartnerID = @partnerid and entity = 'DefaultSingleEmbed' and Setting = 'Sharing'), (select value  from vw_sysDefaultSettings where entity = 'DefaultSingleEmbed' and Setting = 'Sharing')))
	if @sharing is null
		set @sharing = (select -1 * DisableShareButton from Launcher (nolock) where LauncherID = @launcherid)
		
	--	select @partnerid, @launcherid, @videoid, @trackinggroup, @playlistid, @sitesection
	if @videoid is null
		set @videoid = (select top 1 contentid from Playlist_Content (nolock) where PlaylistID = @playlistid order by [Order])

	if @partnerid is null or not exists (select 1 from Content (nolock) where ContentID = @videoid)
	begin
		select convert(varchar(400), null) as IFrame2Embed, convert(varchar(400),  null) as jscriptEmbed -- convert(varchar(60), 'Invalid input parameters supplied') as Messageback
		 return
	end
	--	 select @partnerid, @w, @h, @sharing, @videoid

	set @jsTID = (select value from vw_partnerdefaultsettings (nolock) where partnerid = @partnerid and entity = 'DefaultSingleEmbed' and Setting = 'EmbedJscriptTemplateID')
	if  @jsTID is null
		set @jsTID = (select value from vw_sysdefaultsettings (nolock) where entity = 'DefaultSingleEmbed' and Setting = 'EmbedJscriptTemplateID')	
	
	set @ifTID = (select value from vw_partnerdefaultsettings (nolock) where partnerid = @partnerid and entity = 'DefaultSingleEmbed' and Setting = 'EmbediframeTemplateID')
	if  @ifTID is null
		set @ifTID = (select value from vw_sysdefaultsettings (nolock) where entity = 'DefaultSingleEmbed' and Setting = 'EmbediframeTemplateID')	
	
	-- select @partnerid, @launcherid, @playlistid, @trackinggroup, @sitesection, @h, @w, @videoid


    SELECT convert(varchar(400), (select replace(replace(replace(replace(replace(replace(replace(urlline, '{ssid}', @sitesection), '{w}', @w), '{h}', @h), '{tg}', @trackinggroup), '{vid}', @videoid), '{cid}', @playlistid), '{wid}', @launcherid) from templates where TemplatesID = @ifTID)) as IFrame2Embed
			, convert(varchar(400), (select replace(replace(replace(replace(replace(replace(replace(urlline, '{ssid}', @sitesection), '{w}', @w), '{h}', @h), '{tg}', @trackinggroup), '{vid}', @videoid), '{cid}', @playlistid), '{wid}', @launcherid) from templates where TemplatesID = @jsTID)) as jscriptEmbed

  END
GO
