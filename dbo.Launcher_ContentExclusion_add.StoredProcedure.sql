USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Launcher_ContentExclusion_add]
	@PartnerID int,
	@LauncherID int
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'Launcher_ContentExclusion_add'

	IF @LauncherID = 0 or @PartnerID = 0
	BEGIN
		select 'Bad Parameters' as MessageBack
		return
	END

	insert into Launcher_ContentExclusion(PartnerID, LauncherID)
	select @PartnerID, @LauncherID
	where not exists
		(select 1
		from Launcher_ContentExclusion lce
		where lce.PartnerID = @PartnerID and lce.LauncherID = @LauncherID)

	set nocount off
END
GO
