USE [NewsincStaging]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[logo_UpdateLogo]
	@PartnerID int,
	@LogoTypeID int,
	@LogoURL varchar(200),
	@Default bit = 1,
	@Active bit = 1,
	@LogoID int
as
BEGIN
	set fmtonly off
	set nocount on
	exec SprocTrackingUpdate 'logo_UpdateLogo'

	Update Logo
		set LogoURL = @LogoURL,
			LogoTypeID = @LogoTypeID,
			Active = @Active,
			PartnerDefault = @Default
	where	LogoID = @LogoID

	If @Active = 1
	begin
		Update b
		set	b.Active = 0
		from	(select pl.PartnerID, lo.LogoID
				from Partner_Logo pl
				join	Logo lo
				on		pl.LogoID = lo.LogoID
				where	pl.PartnerID = @PartnerID
				and		lo.LogoTypeID = (select LogoTypeID from Logo where LogoID = @LogoID)) a
		join	Logo b
		on		a.LogoID = b.LogoID
		where	b.LogoID <> @LogoID
	end

	-- handle legacy tables
	if @Active = 1 and  @LogoTypeID = 5
	begin
		--update Partner and other tables
		update Partner set LogoURL = @LogoURL where PartnerID = @PartnerID
		update organizationslegacy set LogoURL = right(@LogoURL, charindex('/', reverse(@LogoURL))-1) where PartnerID = @Partnerid
	end

	set nocount off
END
GO
